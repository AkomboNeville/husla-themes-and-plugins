<?php

namespace dm;

use stdClass;


if (!class_exists('Dm_Database_Helper')) {
    class Dm_Database_Helper
    {
        /**
         * @var string
         */
        private $table_name;
        /**
         * @var string
         */
        private $create_sql;
        /**
         * @var string
         */
        private $charset_collate;

        /**
         * Database_Helper constructor.
         * @param $table_name
         * @param $create_sql
         */
        public function __construct($table_name, $create_sql)
        {
            global $wpdb;

            $this->charset_collate = $wpdb->get_charset_collate();
            $this->table_name = $table_name;
            $this->create_sql = $create_sql;

            $this->create_database_table();
        }

        /**
         * Creates a new database if it does not exists
         */
        private function create_database_table()
        {
            global $wpdb;

            //Initializes database
            if ($wpdb->get_var("SHOW TABLES LIKE '$this->table_name'") != $this->table_name) {
                /*$this->create_sql = "CREATE TABLE " . $this->table_name . "  (
                      session_id BIGINT(20) NOT NULL AUTO_INCREMENT,
                      session_key varchar(128) NOT NULL,
                      session_value LONGTEXT NOT NULL,
                      session_expiry BIGINT(20),
                      PRIMARY KEY  (session_id)
                    ) $this->charset_collate;";*/

                echo '<br/>' . $this->create_sql . '<br/>';
                require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
                dbDelta($this->create_sql . " $this->charset_collate;");
                //dbDelta($sql);
            }
        }

        /**
         * @param $values array : associative array containing the values to be inserted
         * @return int : the id of the item inserted
         */
        public function insert($values)
        {
            global $wpdb;

            $wpdb->insert($this->table_name, $values);

            return $wpdb->insert_id;
        }

        public function update($values, $where)
        {
            global $wpdb;

            /*$values = array(
                'column1' => 1,	// string
                'column2' => 'value2'	// integer (number)
            );

            $where = array( 'ID' => 1 );*/

            /*array(
                '%s',	// value1
                '%d'	// value2
            )*/

            //return $this->get_format_array($where);


            return $wpdb->update(
                $this->table_name,
                $values,
                $where,
                $this->get_format_array($values),
                $this->get_format_array($where)
            );
        }

        /**
         * @param array $items_to_select
         * @param $after_general_select string : this is the text that will appear after
         * `SELECT FROM table_name`.
         * You can put a space before or ignore it if you want
         * @return array
         */
        public function getMany($items_to_select = array(), $after_general_select = '')
        {
            global $wpdb;

            $select_string = '*';

            if(count($items_to_select) > 0) {
                $select_string = '';
                foreach ($items_to_select as $item) {
                    $select_string .= $item . ', ';
                }
                $select_string = substr($select_string, 0, count($items_to_select)-1);
            }

            $results = $wpdb->get_results("SELECT " . $select_string . " FROM  $this->table_name $after_general_select");
            $results = ($results != null) ? $results : array();

            return $results;
        }

        /**
         * @param array $items_to_select
         * @param $after_general_select string : this is the text that will appear after
         * `SELECT FROM table_name`.
         * You can put a space before or ignore it if you want
         * @return object
         */
        public function get($items_to_select = array(), $after_general_select = '')
        {
            global $wpdb;

            $select_string = '*';

            if(count($items_to_select) > 0) {
                $select_string = '';
                foreach ($items_to_select as $item) {
                    $select_string .= $item . ', ';
                }
                $select_string = substr($select_string, 0, (strlen($select_string) - 2));
            }

            $after_general_select .= ' LIMIT 1';

            $results = $wpdb->get_results("SELECT " . $select_string . " FROM  $this->table_name $after_general_select");

            if ($results != null && gettype($results) == 'array' && count($results) > 0) {
                return $results[0];
            } else {
                return new stdClass();
            }
        }

        public function raw_query($items_to_select = array(), $after_general_select = '')
        {
            global $wpdb;

            $select_string = '*';
            /*
                    if(count($items_to_select) > 0) {
                        $select_string = '';
                        foreach ($items_to_select as $item) {
                            $select_string .= $item . ', ';
                        }
                        $select_string = substr($select_string, 0, (strlen($select_string) - 2));
                    }

                    $after_general_select .= ' LIMIT 1';

                    $results = $wpdb->get_results("SELECT " . $select_string . " FROM  $this->table_name $after_general_select");

                    if ($results != null && gettype($results) == 'array' && count($results) > 0) {
                        return $results[0];
                    } else {
                        return new stdClass();
                    }*/
        }

        private function get_format_array($values) {
            $format_array = array();

            foreach ($values as $value) {
                switch (gettype($value)) {
                    case 'boolean':
                    case 'integer':
                    case 'double':
                        array_push($format_array, '%d');
                        break;
                    case 'NULL':
                    case 'string':
                        array_push($format_array, '%s');
                        break;
                }
            }

            return $format_array;
        }
    }
}
