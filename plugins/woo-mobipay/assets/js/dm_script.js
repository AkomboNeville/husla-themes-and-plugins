/*
 * Created by Diyen Momjang (http://diyenmomjang.info)
 */
jQuery(document).ready(function ($) {

    $(document).on('click', '#place_order', function () {
        $('html, body').animate({
            scrollTop: $("#content").offset().top
        }, 1000);
    });
});