<?php

class WC_Gateway_Zingerpay extends WC_Payment_Gateway
{

    // Setup our Gateway's id, description and other values
    function __construct()
    {

        // The global ID for this Payment method
        $this->id = "woomozingerpay";

        // The Title shown on the top of the Payment Gateways Page next to all the other Payment Gateways
        $this->method_title = __("ZingerPay", 'woo-mobipay');

        // The description for this Payment Gateway, shown on the actual Payment options page on the backend
        $this->method_description = __("ZingerPay Payment Gateway Plug-in for WooCommerce", 'woo-mobipay');

        // The title to be used for the vertical tabs that can be ordered top to bottom
        $this->title = __("ZingerPay", 'woo-mobipay');

        $this->order_button_text = __("Proceed to ZingerPay", 'woo-mobipay');

        // If you want to show an image next to the gateway's name on the frontend, enter a URL to an image.
        $this->icon = null;

        // Bool. Can be set to true if you want payment fields to show on the checkout 
        // if doing a direct integration, which we are doing in this case
        $this->has_fields = false;

        // Supports the default credit card form
        // $this->supports = array('default_credit_card_form');

        // This basically defines your settings which are then loaded with init_settings()
        $this->init_form_fields();

        // After init_settings() is called, you can get the settings and load them into variables, e.g:
        // $this->title = $this->get_option( 'title' );
        $this->init_settings();

        // Turn these settings into variables we can use
        foreach ($this->settings as $setting_key => $value) {
            $this->$setting_key = $value;
        }

        // Lets check for SSL
        add_action('admin_notices', array($this, 'do_ssl_check'));

        // Save settings
        if (is_admin()) {
            // Versions over 2.0
            // Save our administration options. Since we are not going to be doing anything special
            // we have not defined 'process_admin_options' in this class so the method in the parent
            // class will be used instead
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        }
    }

    // End __construct()
    // Check if we are forcing SSL on checkout pages
    // Custom function not required by the Gateway
    public function do_ssl_check()
    {
        if ($this->enabled == "yes") {
            if (get_option('woocommerce_force_ssl_checkout') == "no") {
                echo "<div class=\"error\"><p>" . sprintf(__("<strong>%s</strong> is enabled and WooCommerce is not forcing the SSL certificate on your checkout page. Please ensure that you have a valid SSL certificate and that you are <a href=\"%s\">forcing the checkout pages to be secured.</a>"), $this->method_title, admin_url('admin.php?page=wc-settings&tab=checkout')) . "</p></div>";
            }
        }
    }

    // Build the administration fields for this specific Gateway
    public function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Enable / Disable', 'woo-mobipay'),
                'label' => __('Enable this payment gateway', 'woo-mobipay'),
                'type' => 'checkbox',
                'default' => 'no',
            ),
            'title' => array(
                'title' => __('Title', 'woo-mobipay'),
                'type' => 'text',
                'desc_tip' => __('Payment title the customer will see during the checkout process.', 'woo-mobipay'),
                'default' => __('ZingerPay', 'woo-mobipay'),
            ),
            'description' => array(
                'title' => __('Description', 'woo-mobipay'),
                'type' => 'textarea',
                'desc_tip' => __('Payment description the customer will see during the checkout process.', 'woo-mobipay'),
                'default' => __('Pay securely using your express card.', 'woo-mobipay'),
                'css' => 'max-width:350px;'
            ),
            'secureHashSecret' => array(
                'title' => __('Secure Hash Secret', 'woo-mobipay'),
                'type' => 'text',
                'desc_tip' => __('Secure Hash Secret', 'woo-mobipay'),
            ),
            'accessCode' => array(
                'title' => __('Access Code', 'woo-mobipay'),
                'type' => 'text',
                'desc_tip' => __('Access Code', 'woo-mobipay'),
            ),
            'merchantId' => array(
                'title' => __('Merchant ID', 'woo-mobipay'),
                'type' => 'text',
                'desc_tip' => __('Merchant ID', 'woo-mobipay'),
            ),
            'vpcUrl' => array(
                'title' => __('VPC URL', 'woo-mobipay'),
                'type' => 'text',
                'desc_tip' => __('VPC URL', 'woo-mobipay'),
            ),
            'returnUrl' => array(
                'title' => __('Return url', 'woo-mobipay'),
                'type' => 'text',
                'desc_tip' => __('Return url', 'woo-mobipay'),
            ),
            'errorColor' => array(
                'title' => __('Error color', 'woo-mobipay'),
                'type' => 'text',
                'desc_tip' => __('This is the color you want the error message to carry. For example: red, #f00, '
                    . '#ff0000, rgb(255, 0, 0), rgba(255, 0, 0, 0.3). The default is red.', 'woo-mobipay'),
            ),
            'api_details' => array(
                'title'       => __( 'API credentials', 'woocommerce' ),
                'type'        => 'title',
                'description' => sprintf( __( 'Enter your WooMobiPay API credentials to process refunds via master and visa cards. Learn how to access your <a href="%s">WooMobiPay API Credentials</a>.', 'woo-mobipay' ), 'https://developer.paypal.com/webapps/developer/docs/classic/api/apiCredentials/#creating-an-api-signature' ),
            ),
            'publicKey' => array(
                'title' => __('Public key', 'woo-mobipay'),
                'type' => 'text',
                'desc_tip' => __('A public/private key pair is required to make transactions', 'woo-mobipay'),
            ),
            'privateKey' => array(
                'title' => __('Private key', 'woo-mobipay'),
                'type' => 'text',
                'desc_tip' => __('A public/private key pair is required to make transactions', 'woo-mobipay'),
            ),
        );
    }

    //Processes payments
    public function process_payment($order_id)
    {
        $amount = WC()->cart->cart_contents_total;

        $paraFinale = array(
            'order_id' => $order_id,
            'amount' => $amount,
            'access_code' => $this->get_option('accessCode', true),
            'merchant_id' => $this->get_option('merchantId', true),
            'base_url' => $this->get_option('vpcUrl', true),
            'return_url' => $this->get_option('returnUrl', true),
            'secret_hash' => $this->get_option('secureHashSecret', true),
            'public_key' => $this->get_option('publicKey', true),
            'private_key' => $this->get_option('privateKey', true)
        );

        $request_url = 'http://localhost:8000/zinger-pay' . '?' . http_build_query($paraFinale);

        $response = file_get_contents($request_url);

        $response = json_decode($response);

        if ($response->code == 200) {
            $transaction_link = $response->message;

            return array(
                'result' => 'success',
                'redirect' => $transaction_link
            );
        } else {
            wc_add_notice(__('Payment error: ', 'woo-mobipay') . $response->message, 'error');
            return;
        }
    }
}