<?php

use dm\Dm_Database_Helper;

/**
 * OM Notif URL
 *
 * @param $request
 * @return array
 */
function notif_url_callback($request)
{
    //wp_strip_all_tags
    $parameters = $request->get_params();

    $status = $parameters["status"];
    $notif_token = $parameters["notif_token"];
    $txnid = $parameters["txnid"];

//    mail('diyenmomjang@gmail.com', 'PARAMS', json_encode($parameters));

    switch ($status) {
        case 'SUCCESS':

            $order_id = $_GET['order_id'];

            global $wpdb;

            $table_name = $wpdb->prefix . 'orange_money_transaction';

            // TODO see if this can come from one place
            // TODO verify if database has to be cleaned after transaction
            $create_sql = "CREATE TABLE " . $table_name . "  (
                  transaction_id BIGINT(20) NOT NULL AUTO_INCREMENT,
                  order_id BIGINT(20) NOT NULL,
                  pay_token varchar(252) NOT NULL,
                  notif_token varchar(252) NOT NULL,
                  PRIMARY KEY  (transaction_id)
                )";

            $db = new Dm_Database_Helper($table_name, $create_sql);

            $after_general_select = 'WHERE order_id="' . $order_id . '"';
            $result = $db->getMany(array(), $after_general_select);

            if (count($result) > 0 && $result[0]->notif_token == $notif_token) {

                global $woocommerce;

                $order = new WC_Order($order_id);

                // Mark as on-hold (we're awaiting the cheque)
                // $order->update_status('on-hold', __('Awaiting orange money payment', 'woocommerce'));

                //Complete payment
                 $order->payment_complete();

                // Reduce stock levels
                $order->reduce_order_stock();

                // Remove cart
                $woocommerce->cart->empty_cart();

                // Return thankyou redirect
                wp_send_json_success([
                    'message' => 'Success'
                ]);
            } else {
                wp_send_json_error([
                    'message' => 'There was an error notifying the server',
                ]);

            }
            break;
        default:
            wp_send_json_error([
                'message' => 'There was an error notifying the server',
            ]);
            break;
    }
}