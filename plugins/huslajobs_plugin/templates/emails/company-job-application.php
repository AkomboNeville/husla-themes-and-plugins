<?php
if (!defined('ABSPATH')) exit;  // if direct access

ob_start();
?>
    <div style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;width:100%!important;height:100%;margin:0;padding:40px 0;background:#1cb6c5">
        <div style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-radius:3px;display:block!important;max-width:450px!important;clear:both!important;margin:0 auto;margin-top:20px;margin-bottom:20px;background: #ffffff">
            <div class="header" style="padding: 20px 0; text-align: center;background: #ff9922">

                <strong style="font-size: 20px;">{site_name}</strong>
            </div>
            <div style="padding:15px 25px 10px 25px;">
                <div class="content" style="padding: 10px 0 40px;">
                    <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('A job application sent by {applicant_name}, from {site_name}.', 'huslajobs'); ?></p>

                    <h4 style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;">
                        <em><span style="color: #333333; font-family: Arial, sans-serif;"><?php echo __('Application details', 'huslajobs'); ?></span></em>
                    </h4>

                    <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Applicant name:{applicant_name}', 'huslajobs'); ?></p>
                    <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Applicant email:', 'huslajobs'); ?>
                        <a class="btn"
                           style="color: #51b3ff; padding: 10px 20px; font-size: 14px; font-family: Arial, sans-serif;"
                           href="mailto:<?php echo __('{applicant_email}', 'user-verification'); ?>"><?php echo __('{applicant_email}', 'user-verification'); ?></a>
                    </p>
                    <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Applicant phone number:{applicant_phone}', 'huslajobs'); ?></p>
                    <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Applicant country:{applicant_country}', 'huslajobs'); ?></p>
                    <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Applicant city:{applicant_city}', 'huslajobs'); ?></p>
                    <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Applicant motivation', 'huslajobs'); ?></p>
                    <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('{applicant_motivation}', 'huslajobs'); ?></p>
                    <p><?php echo __('Attachment:', 'huslajobs'); ?></p>
                    <p><a href="{attachments}"><?php echo __('{applicant_name} CV', 'huslajobs'); ?></a></p>
                </div>

                <div class="footer" style="padding: 20px 0; clear: both; text-align: center;"><small
                            style="font-size: 11px;">{site_name} - {site_description}</small></div>
            </div>
        </div>
    </div>
<?php


$templates_data_html['company_job_application'] = ob_get_clean();