<?php
if (!defined('ABSPATH')) exit;  // if direct access

ob_start();
?>
<div style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;width:100%!important;height:100%;margin:0;padding:40px 0;background:#1cb6c5">
    <div style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;font-size:100%;line-height:1.6;border-radius:3px;display:block!important;max-width:450px!important;clear:both!important;margin:0 auto;margin-top:20px;margin-bottom:20px;background: #ffffff">
        <div class="header" style="padding: 20px 0; text-align: center;background: #ff9922">
            <strong style="font-size: 20px;">{site_name}</strong>
        </div>
        <div style="padding:15px 25px 10px 25px;">
            <div class="content" style="padding: 10px 0 40px;">
                <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Hello {applicant_name},', 'huslajobs'); ?></p>
                <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Thank you for using {site_name},to apply for a job.', 'huslajobs'); ?></p>

                <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><span
                            style="color: #333333; font-family: Arial, sans-serif;"><?php echo __('Application details', 'huslajobs'); ?></span>
                </p>

                <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Name:{applicant_name}', 'huslajobs'); ?></p>
                <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Email:', 'user-verification'); ?>
                    <a class="btn"
                       style="color: #51b3ff; padding: 10px 20px; font-size: 14px; font-family: Arial, sans-serif;"
                       href="mailto:<?php echo __('{applicant_email}', 'huslajobs'); ?>"><?php echo __('{applicant_email}', 'huslajobs'); ?></a>
                </p>
                <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Phone number:{applicant_phone}', 'huslajobs'); ?></p>
                <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Country:{applicant_country}', 'huslajobs'); ?></p>
                <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('City:{applicant_city}', 'huslajobs'); ?></p>
                <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('Motivation', 'huslajobs'); ?></p>
                <p style="font-size: 14px; line-height: 20px; color: #333; font-family: Arial, sans-serif;"><?php echo __('{applicant_motivation}', 'huslajobs'); ?></p>
                <p><?php echo __('Attachment:{attachments}', 'huslajobs'); ?></p>

            </div>

            <div class="footer" style="padding: 20px 0; clear: both; text-align: center;"><small
                        style="font-size: 11px;">{site_name} - {site_description}</small></div>
        </div>
    </div>
</div>
<?php


$templates_data_html['job_seeker_application_email'] = ob_get_clean();