# Husla Jobs WordPress Plugin

## Installation
1. Download Plugin
2. Extract and copy plugin folder to the wp-content/plugins directory of your WordPress installation
3. Activate the plugin

## Contributing
Please look at the 
[contributing guide](contributing.txt)