<?php

namespace huslajobs;

use Faker\Generator;

new HuslaSeeder( function ( Generator $faker ) {
	$seeder              = new Category();
	$seeder->name        = $faker->jobTitle;
	$seeder->description = $faker->text( 30 );
	$seeder->save();
}, 30 );


new HuslaSeeder( function ( Generator $faker ) {
	$seeder              = new JobType();
	$seeder->name        = $faker->jobTitle;
	$seeder->description = $faker->text( 30 );
	$seeder->save();
}, 30 );


new HuslaSeeder( function ( Generator $faker ) {
	$seeder         = new Currency();
	$currency       = $faker->currencyCode;
	$seeder->name   = $currency;
	$seeder->symbol = $currency;
	$seeder->code   = $currency;
	$seeder->save();
}, 10 );


new HuslaSeeder( function ( Generator $faker ) {
	global $wpdb;

	$total_query             = "SELECT MAX(id) as total FROM wp_users";
	$total                   = intval( $wpdb->get_var( $total_query ) );
	$total                   = $total > 30 ? 30 : $total;
	$seeder                  = new Account();
	$seeder->wp_user_id      = $faker->numberBetween( 1, $total );
	$seeder->website = $faker->url;
	$seeder->email   = $faker->email;
	$seeder->account_type    = $faker->randomElement( [
		'individual_job_seeker',
		'company_job_seeker',
		'company_recruiter',
		'individual_recruiter'
	] );
	$seeder->name    = $faker->company;
	$seeder->fax             = $faker->phoneNumber;
	$seeder->save();

}, 10 );

new HuslaSeeder( function ( Generator $faker ) {
	$last_account_id      = Account::orderBy( 'id', 'desc' )->first()->id;
	$profile              = new Profile();
	$profile->name        = $faker->jobTitle;
	$profile->cv          = $faker->imageUrl();
	$profile->description = $faker->text( 50 );
	$profile->account_id  = $faker->numberBetween( 1, $last_account_id );
	$profile->save();
}, 10 );

new HuslaSeeder( function ( Generator $faker ) {
	$last_profile_id  = Profile::orderBy( 'id', 'desc' )->first()->id;
	$last_category_id = Category::orderBy( 'id', 'desc' )->first()->id;

	$profile_category              = new ProfileCategory();
	$profile_category->profile_id  = $faker->numberBetween( 1, $last_profile_id );
	$profile_category->category_id = $faker->numberBetween( 1, $last_category_id );
	$profile_category->save();
}, 10 );

new HuslaSeeder( function ( Generator $faker ) {
	$last_currency_id = Currency::orderBy( 'id', 'desc' )->first()->id;
	$wp_users         = WpUser::get( [ 'id' ] );
	$ids              = array_map( function ( $e ) {
		return $e->id;
	}, $wp_users );
	array_push( $ids, 0 );


	$seeder              = new Package();
	$seeder->wp_user_id  = $faker->randomElement( $ids );
	$seeder->name        = $faker->text( 6 );
	$seeder->description = $faker->text( 30 );
	$seeder->price       = $faker->numberBetween( 1000, 50000 );
	$seeder->duration    = $faker->numberBetween( 1, 40 );
	$seeder->currency_id = $faker->numberBetween( 1, $last_currency_id );
	$seeder->profiles    = $faker->numberBetween( 1, 20 );
//	$seeder->account_id  = $faker->numberBetween( 1, $last_account_id );
	$seeder->save();
}, 30 );

new HuslaSeeder( function ( Generator $faker ) {
	$last_package_id     = Package::orderBy( 'id', 'desc' )->first()->id;
	$last_job_type_id    = JobType::orderBy( 'id', 'desc' )->first()->id;
	$seeder              = new PackageLimit();
	$seeder->package_id  = $faker->numberBetween( 1, $last_package_id );
	$seeder->job_type_id = $faker->numberBetween( 1, $last_job_type_id );
	$seeder->limit       = $faker->numberBetween( 1, 20 );
	$seeder->save();
}, 30 );

new HuslaSeeder( function ( Generator $faker ) {
	$last_package_id = Package::orderBy( 'id', 'desc' )->first()->id;
	$wp_users        = WpUser::get( [ 'id' ] );
	$ids             = array_map( function ( $e ) {
		return $e->id;
	}, $wp_users );

	$seeder             = new Subscription();
	$seeder->wp_user_id = $faker->randomElement( $ids );
	$seeder->package_id = $faker->numberBetween( 1, $last_package_id );
	$seeder->start_date = $faker->date();
	$seeder->end_date   = $faker->date();
	$seeder->status     = $faker->numberBetween( 0, 1 );
	$seeder->save();
}, 30 );

new HuslaSeeder( function ( Generator $faker ) {
	$last_job_type_id = JobType::orderBy( 'id', 'desc' )->first()->id;
	$last_account_id  = Account::orderBy( 'id', 'desc' )->first()->id;
	$last_category_id = Category::orderBy( 'id', 'desc' )->first()->id;


	$seeder                    = new Job();
	$seeder->job_type_id       = $faker->numberBetween( 1, $last_job_type_id );
	$seeder->salary            = $faker->randomNumber( 5 );
	$seeder->name              = $faker->name;
	$seeder->application_email = $faker->email;
	$seeder->work              = $faker->randomElement( [
		'onsite',
		'remote',
	] );
	$seeder->category_id       = $faker->numberBetween( 1, $last_category_id );
	$seeder->description       = $faker->text( 100 );
	$seeder->location          = $faker->address;
	$seeder->experience        = $faker->numberBetween( 1, 10 );
	$seeder->account_id        = $faker->numberBetween( 1, $last_account_id );
	$seeder->currency_id       = $faker->text( 10 );


	$seeder->save();
}, 30 );

new HuslaSeeder( function ( Generator $faker ) {
	$last_job_id        = Job::orderBy( 'id', 'desc' )->first()->id;
	$last_account_id    = Account::orderBy( 'id', 'desc' )->first()->id;
	$seeder             = new JobApplication();
	$seeder->account_id = $faker->numberBetween( 1, $last_account_id );
	$seeder->job_id     = $faker->numberBetween( 1, $last_job_id );
	$seeder->save();
}, 30 );

new HuslaSeeder( function ( Generator $faker ) {
	$last_job_id         = Job::orderBy( 'id', 'desc' )->first()->id;
	$last_category_id    = Category::orderBy( 'id', 'desc' )->first()->id;
	$seeder              = new JobCategory();
	$seeder->category_id = $faker->numberBetween( 1, $last_category_id );
	$seeder->job_id      = $faker->numberBetween( 1, $last_job_id );
	$seeder->save();
}, 30 );
