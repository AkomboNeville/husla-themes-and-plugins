<?php

add_filter( 'husla_jobs_includes_filter', function ( $includes ) {
	$files = [
		HUSLA_JOBS_MIGRATIONS_DIR . '/migrations.php', //
		HUSLA_JOBS_MIGRATIONS_DIR . '/demo_seeders.php', //
	];

	$includes = array_merge( $includes, $files );

	return $includes;
} );