
import {JobSearch} from "../../../dashboard/resources/components/job-search/job-search.js";
import {AnaVueSelect} from "../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
const FrontPage = {
    data(){
        return{
            loading:false,
            frontPageStrings:VueUiStrings.clientFrontEnd.frontPage,
            loggingOut:false,
        }
    },
    components:{
        JobSearch,
        AnaVueSelect,
        VLoader,
    },

    mixins:[ReusableFunctions],
    methods:{
        updateLoading(value){
            this.loading = value;
            if (!this.loading){
                this.closeLoader();
            }
        }
    },
    mounted(){

        if ( parseInt(user_id)){
            this.logOutUser();
        }
    },
    template:`
            <div>
                <section class="hs-section">
                    <div class="row">
                       <div class="col-md-1"></div>
                        <div class="col-md-10 text-center">
                            <div class="hs-page-header">
                                <h1 class="page-title mt-0 pb-0">{{frontPageStrings.heading.toString()}}</h1>
                <!--                <p class="m-0 hs-subtitle">{{frontPageStrings.heading.toString()}}</p>-->
                            </div>
                        </div>
                           <div class="col-md-1"></div>
                    </div>
                </section>
                <section class="hs-section">
                    <div class="row">
                    <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <job-search @update-loading="updateLoading" current-page="frontpage"></job-search>
                        </div>
                           <div class="col-md-1"></div>
                    </div>
                </section>
            </div>

    `,
}

export  {FrontPage}