<?php
// get_header();
//
// global $wp_query;
// echo 'JOB ID IS : ' . $wp_query->query_vars['job'];
// echo '<br />';
// get_footer();
global $user_ID;
if ($user_ID){
    $user = get_user_by('id', $user_ID);
//    redirect admin to admin area
    if (in_array('administrator', (array)$user->roles)) {
        $redirect_to = home_url('/wp-admin');
        ?>
        <script type="text/javascript">
            window.location.href = "<?php echo home_url("/wp-admin") ?>"
        </script>
        <?php
    }
}

    global $wpdb, $user_ID;
    get_header();
    ?>

<div id="job-application-content" class="content-area primary vue-component"></div>
<script type="text/javascript">
    const job_slug = "<?php echo $wp_query->query_vars['job'] ?>"
</script>
<div id="login-nonce">
    <?php wp_nonce_field( 'huslajobs-login', 'huslajobs-login-nonce' ); ?>
</div>
<script src="<?php echo HUSLA_JOBS_CLIENT_MODULE_URL ?>/jobs/resources/index.js" type="module"></script>

<?php get_footer(); ?>
