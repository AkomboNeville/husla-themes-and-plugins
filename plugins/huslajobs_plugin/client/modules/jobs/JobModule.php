<?php

namespace huslajobs_client;

use huslajobs\HuslaModule;
use KMSubMenuPage;

class JobModule extends HuslaModule {
	public function __construct() {
		$this->parent_module = 'client';
		$this->module        = 'jobs';
		$this->addActions();
		$this->addFilters();
	}

	public function addActions() {

		add_action( 'init', function () {
            add_rewrite_rule(
                '^jobs/([^/]*)/apply/?',
                'index.php?job=$matches[1]&apply=apply',
                'top'
            );

		} );

				add_action( 'template_include', function ( $template ) {
        			if ( get_query_var( 'apply' ) == false || get_query_var( 'apply' ) == '' ) {
        				return $template;
        			}
        			return HUSLA_JOBS_CLIENT_MODULE_DIR . '/jobs/templates/apply.php';
        		} );

		add_action( 'init', function () {
			add_rewrite_rule( 'jobs/([a-z0-9-]+)[/]?$', 'index.php?job=$matches[1]', 'top' );
		} );

		add_action( 'template_include', function ( $template ) {
			if ( (get_query_var( 'job' ) == false || get_query_var( 'job' ) == '')  || (get_query_var( 'apply' ) != false || get_query_var( 'apply' ) != '')) {
				return $template;
			}
			return HUSLA_JOBS_CLIENT_MODULE_DIR . '/jobs/templates/single.php';
		} );

		add_action( 'init', function () {

			add_rewrite_rule( 'jobs$', 'index.php?jobs=$matches[0]', 'top' );

		} );

		add_action( 'template_include', function ( $template ) {
			if ( get_query_var( 'jobs' ) == false || get_query_var( 'jobs' ) == '' ) {
				return $template;
			}
			return HUSLA_JOBS_CLIENT_MODULE_DIR . '/jobs/templates/index.php';
		} );

	}

	public function addFilters() {

		add_filter( 'query_vars', function ( $query_vars ) {
			$query_vars[] = 'jobs';

			return $query_vars;
		} );
		add_filter( 'query_vars', function ( $query_vars ) {
			$query_vars[] = 'job';

			return $query_vars;
		} );
				add_filter( 'query_vars', function ( $query_vars ) {
        			$query_vars[] = 'apply';
        			return $query_vars;
        		} );
	}
}