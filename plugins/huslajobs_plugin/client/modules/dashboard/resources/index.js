import {Dashboard} from "./components/Dashboard.js";
import {Layout} from "./components/Layout.js";
import {EditUserAccount} from "./components/accounts/edit-user-account.js";
import {AddJob} from "./components/job/add-job.js";
import {AddProfile} from "./components/profiles/create.js";
import {EditProfile} from "./components/profiles/edit.js";
import {EditAccount} from "./components/accounts/edit.js";
import {CreateCompany} from "./components/accounts/create-company.js";
import {UpgradeAccount} from "./components/packages/index.js";
import {EditAccountJob} from "./components/job/edit-job.js";


const routes = [
    {path: "/", component: Dashboard},
    {path: "/jobs/:slug/edit", component: EditAccountJob},
    {path: "/add-job", component: AddJob},
    {path: "/add-profile", component: AddProfile},
    {path: "/edit-account/:id", component: EditAccount},
    {path: "/edit-profile/:id", component: EditProfile},
    {path: "/edit-user-profile/:id", component: EditUserAccount},
    {path: "/upgrade-account", component:UpgradeAccount},
    {path:"/create-company",component:CreateCompany}

];

// 3. Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's

const router = VueRouter.createRouter({
    // 4. Provide the history implementation to use. We are using the hash history so that it will not interfere with WordPress routes.
    history: VueRouter.createWebHashHistory(),
    // history: VueRouter.createWebHistory(),
    routes, // short for `routes: routes`
    scrollBehavior(to, from, savedPosition) {
        if (to.hash) {
            return {
                el: to.hash,
                behavior: 'smooth',
            }
        }
            // if (savedPosition) {
            //   return savedPosition
            // } else {
            //   return { top: 0, behavior: 'smooth' }
        // }
        else {
            return { top: 0, behavior: 'smooth' }
        }
    }
});

// VUE 3 initialization
const app = Vue.createApp({
    render() {
        return Vue.h(Layout, {});
    },
});

app.use(router);
app.mount("#dashboard-content");
