import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {world_countries as WorldCountries} from "../../../../../../js/countries_data.js";
import {CountriesData} from "../../../../../../js/countries_and_codes.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {VLoader} from "../../../../../../inc/global_components/loader/v-loader.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
import {AnaVuePhoneNumber} from "../../../../../../inc/global_components/phone-number/ana_vue_phone_number.js";

const EditAccount = {
    data() {
        return {
            id: 0,
            form: {},
            loading: false,
            submitting: false,
            error:false,
            formErrors: {},
            countries: WorldCountries,
            states: [],
            maxUpload:maximum_upload,
            placeHolders:{},
            accountEditStrings: VueUiStrings.clientFrontEnd.dashboard.accounts.edit,
            jsUrl: THEME_JS_URL,
        }
    },
    mixins:[ReusableFunctions],
    watch:{
        'form.name': function (currentVal, oldVal) {
            this.validateInput("name", currentVal, {required: true}, this.accountEditStrings.formFields.name.label);
        },
        'form.email': function (currentVal, oldVal) {
            this.validateInput("email", currentVal, {required: true}, this.accountEditStrings.formFields.email.label);
        },
        'form.country':function (currentVal, oldVal) {
            this.validateInput("country", currentVal, {required: true}, this.accountEditStrings.formFields.country.label);
            this.states = [];
            if(currentVal){
                const countriesData = CountriesData;
                let states = countriesData.filter((country)=>country.code.toLowerCase() === currentVal.toLowerCase())[0].states;
                if (states.length){
                    this.states = states.map((state)=>{
                        return{
                            label:state,
                            value:state
                        }
                    });
                }
            }else{
                // this.form.country =
                this.states = [];
                this.form.state = undefined
            }

        },
        'form.city':function (currentVal, oldVal) {
            this.validateInput("city", currentVal, {required: true}, this.accountEditStrings.formFields.city.label);
        },
        'form.state': function (currentVal, oldVal) {
            this.validateInput("state", currentVal, {required: true}, this.accountEditStrings.formFields.state.label);
        },
        'form.bio': function (currentVal, oldVal) {
            this.validateInput("bio", currentVal, {required: true}, this.accountEditStrings.formFields.bio.label);
        },
        'form.fileUpload': function (currentVal, oldVal) {
            if (this.form.account_type.includes('seeker')){
                this.validateInput("fileUpload", currentVal, {required: true}, this.placeHolders?.uploadImage);
            }
        },
    },
    components: {
        AnaVueSelect,
        LoadingError,
        LoadingBar,
        VLoader,
        AnaVuePhoneNumber
    },
    methods: {

        phoneNumberError(err) {
            if (!err && this.formErrors){
                this.formErrors["phone"] = "Phone number is incorrect";
            }
            else if (err && (this.formErrors && this.formErrors["phone"] ) ){
                delete this.formErrors["phone"]
            }
        },
        /**
         * function gets categories,job types, currencies, accounts
         */
        getAccount() {
            this.startLoader();
            const data = new FormData();
            data.append('account_id', this.id);
            data.append("action", "get_account");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response) {

                        const formobj = response.data;
                        //to prevent the watch function to throw errors we
                        //set all null values to undefined
                        //before assigning it to form object
                        that.form = formobj;
                        Object.keys(formobj).map(function(key, index) {
                            formobj[key] = !formobj[key] ? undefined : formobj[key];

                        });

                        if (that.form.account_type.includes('company')){
                            that.placeHolders ={
                                bio:that.accountEditStrings.formFields.bio.placeholders.company,
                                uploadImage:that.accountEditStrings.formFields.profileImage.buttons.company.uploadImage,
                                changeImage:that.accountEditStrings.formFields.profileImage.buttons.company.changeImage
                            }
                        }else{
                            that.placeHolders ={
                                bio:that.accountEditStrings.formFields.bio.placeholders.individual,
                                uploadImage:that.accountEditStrings.formFields.profileImage.buttons.individual.uploadImage,
                                changeImage:that.accountEditStrings.formFields.profileImage.buttons.individual.changeImage
                            }
                        }
                        //set fileUpload
                        that.form.fileUpload = that.form.profile_image ? {url:that.form.profile_image} : that.form.profile_image;

                        that.loading = false;
                        that.closeLoader();
                        console.log("Object.keys(that.form).length ");
                        console.log(Object.keys(that.form).length );
                        setTimeout(()=>{
                            that.setUpPhoneNumber();
                        },100)

                    }
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.accountEditStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.closeLoader();
                });
        },

        submitForm() {
            this.submitting = true
            //validate inputs

            this.validateInput("name", this.form.name, {required: true}, this.accountEditStrings.formFields.name.label);
            this.validateInput("email", this.form.email, {required: true}, this.accountEditStrings.formFields.email.label);
            this.validateInput("bio", this.form.bio, {required: true}, this.accountEditStrings.formFields.bio.label);
            this.validateInput("city", this.form.city, {required: true}, this.accountEditStrings.formFields.city.label);
            this.validateInput("country", this.form.country, {required: true}, this.accountEditStrings.formFields.country.label);
            this.validateInput("state", this.form.state, {required: true}, "State");
            if (this.form.account_type.includes('seeker')){
                this.validateInput("fileUpload", this.form.fileUpload, {required: true}, this.placeHolders?.uploadImage);
            }

            if (Object.keys(this.formErrors).length === 0) {

                const data = new FormData();
                const that = this;

                data.append("name", this.form.name);
                data.append("email", this.form.email);
                data.append("website", this.form.website ?? '');
                data.append("country", this.form.country);
                data.append("profile_image", this.form.fileUpload?.file ? this.form.fileUpload?.file: this.form.fileUpload?.url ?? '');
                data.append("state", this.form.state);
                data.append("city", this.form.city);
                data.append("phone_number", this.form.phone_number ??'');
                data.append("fax", this.form.fax ?? '');
                data.append("bio", this.form.bio);
                data.append("account_id", this.id);
                data.append('my_account_nonce', document.getElementById("huslajobs-my-account-nonce").value);
                data.append("action", "update_account");

                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data);
                            that.successMessage = response.data
                            localStorage.removeItem('user') // force getting new data from database
                            window.location.href = response.data.redirect_url;

                        }
                        that.submitting = false;

                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.accountEditStrings.errors.errorOccurred);
                        }
                        that.submitting = false;

                    });

            } else {
                this.submitting = false
            }
        },
    },
    mounted() {
        this.id = this.$route.params.id;
        this.getAccount();
    },
    // language=HTML
    template:
        `<VLoader :active="loading" :translucent="true">
        <div v-if="error">
            <loading-error @update="getAccount"></loading-error>
        </div>
            <div v-else-if="Object.keys(form).length >  0" class="my-account-profile">
                <section class="hs-section">
                    <div class="row mb-4 text-center">
                        <div class="col-md-12">
                            <h1 class="page-title pt-0 mb-2">
                                {{accountEditStrings.header}}
                            </h1>
                            <router-link to="/" class="hs-primary-text-color-light">{{accountEditStrings.goBack}}</router-link>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <form ref="submitForm" id="wp_signup_form" method="post"
                                  @submit.prevent="submitForm">

                                <div class="d-flex justify-content-between flex-column">
                                    <div class="row">
                                        <div class="mb-4 col-md-12">
                                            <input type="text" class="form-control" :placeholder="accountEditStrings.formFields.name.placeholder"
                                                   autocomplete="off" id="name" name="name"
                                                   v-model="form.name"
                                                   >
                                            <span class="text-danger" v-if="formErrors.name">{{formErrors.name}}</span>
                                        </div>
                                        <div class="mb-4  col-sm-12">
                                            <input type="email" class="form-control" id="email"
                                                   :placeholder="accountEditStrings.formFields.email.label" name="email"
                                                   v-model="form.email"
                                                   >
                                            <span class="text-danger d-block" v-if="formErrors.email">{{formErrors.email}}</span>
                                        </div>
                                        <div v-if="form.account_type.includes('company')" class="mb-4  col-sm-12">
                                            <input type="text" class="form-control" id="website"
                                                   :placeholder="accountEditStrings.formFields.website.placeholder" name="website"
                                                   v-model="form.website">
                                        </div>
                                        <div class="col-md-6 mb-4">
<!--                                            {{countries}}-->
                                            <AnaVueSelect
                                                    v-model="form.country" :options="countries"
                                                    :placeholder="accountEditStrings.formFields.country.placeholder"></AnaVueSelect>
                                            <span class="text-danger"
                                                  v-if="formErrors.country">{{formErrors.country}}</span>

                                        </div>
                                        <div class="col-md-6 mb-4">
                                            <AnaVueSelect v-model="form.state"
                                                          :options="states"
                                                          :placeholder="accountEditStrings.formFields.state.placeholder"
                                                          :fieldName="accountEditStrings.formFields.state.label"
                                                          :disabled="!form.country"></AnaVueSelect>
                                            <small>{{accountEditStrings.formFields.state.tip}}</small>
                                            <span class="text-danger d-block" v-if="formErrors.state">{{formErrors.state}}</span>
                                        </div>

                                        <div class="col-md-12 mb-4">
                                            <input type="text" name="city" id="city" v-model="form.city"
                                                   :placeholder="accountEditStrings.formFields.city.placeholder"
                                                   class="form-control">
                                            <span class="text-danger" v-if="formErrors.city">{{formErrors.city}}</span>
                                        </div>
                                        <div class="col-md-12 mb-4">
                                            <ana-vue-phone-number v-model="form.phone_number" :country-code="form.country" @phoneError="phoneNumberError"></ana-vue-phone-number>
                                            <span class="text-danger validation-error" v-if="formErrors.phone">{{formErrors.phone}}</span>
                                        </div>
                                        <div class="col-md-12 mb-4">
                                            <textarea name="description" id="description" v-model="form.bio" rows="4"
                                                      :placeholder="placeHolders?.bio" class="form-control">
                                            </textarea>
                                            <span class="text-danger" v-if="formErrors.bio">{{formErrors.bio}}</span>
                                        </div>
                                        <div v-if="form.account_type.includes('company')" class="col-md-12 mb-4">
                                            <input type="text" name="fax_number" v-model="form.fax"
                                                   id="fax-number" class="form-control" :placeholder="accountEditStrings.formFields.faxNumber.label">
                                        </div>
                                        <div class="col-md-12 mb-5">
                                            <div class="d-flex bg-light flex-wrap align-items-end py-5 px-3">

                                                <div class="profile-image-preview mr-5">
                                                    <img v-if="form.fileUpload" ref="profile-image"
                                                         :src="form.fileUpload.url"
                                                         :alt="form.fileUpload.name">
                                                    <div>
                                                        <p class="m-0">{{accountEditStrings.formFields.profileImage.tips.imageTypes}}: png,jpeg,jpg</p>
                                                        <p class="m-0">{{accountEditStrings.formFields.profileImage.tips.maxSize}}:{{maxUpload}}M</p>
                                                    </div>

                                                </div>
                                                <div class="profile-image-btn">
                                                    <label for="profile-image"
                                                           class="hs-btn hs-btn-gray cursor-pointer">
                                                        <input type="file" name="profile-image"
                                                               accept="image/png,image/jpeg,image/jpg"
                                                               id="profile-image" class="d-none"
                                                               @input="getFile">
                                                        <span v-if="form.fileUpload">{{placeHolders.changeImage}}</span>
                                                        <span v-else>{{placeHolders.uploadImage}}</span>
                                                    </label>

                                                </div>
                                            </div>
                                            <span class="text-danger" v-if="formErrors.fileUpload">{{formErrors.fileUpload}}</span>
                                        </div>
                                        <div class="mt-4 col-md-12">
                                            <div class="d-flex align-items-center flex-wrap">
                                                <button type="submit"
                                                        class="hs-btn hs-btn-signup hs-btn-primary"
                                                        :class="{'not-allowed':submitting || Object.keys(formErrors).length > 0}"
                                                        :disabled="submitting || Object.keys(formErrors).length > 0">
                                                    {{accountEditStrings.formFields.button}}
                                                    <i
                                                        v-if="submitting"
                                                        class="fas fa-spinner fa-pulse"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </section>

            </div>
            </VLoader>
        `
}

export {EditAccount}