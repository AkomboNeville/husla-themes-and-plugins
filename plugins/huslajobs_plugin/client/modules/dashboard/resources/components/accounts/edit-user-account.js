import {SignUpForm} from "../../../../signup/resources/components/signup-form.js";
import {VLoader} from "../../../../../../inc/global_components/loader/v-loader.js";

const EditUserAccount ={
    data(){
        return{
            imageUrl:THEME_URL,
            homeUrl:home_url,
            loading:false,
            editUserStrings: VueUiStrings.clientFrontEnd.dashboard.accounts.editUser
        }
    },
    components:{
        SignUpForm,
        VLoader
    },
    methods:{
        updateSuccessMessage(message){
            localStorage.removeItem('user');
            const that = this;
            setTimeout(function () {
                that.$router.push({
                    path: `/`,
                })
            },3000)
        },
        updateLoading(value){
            this.loading = value
            if (!this.loading){
                if( document.querySelector('#loading-screen')){
                    document.querySelector('#loading-screen').classList.add("d-none")
                }
            }
        }
    },


    // language=HTML
    template:
        `
            <VLoader :active="loading" :translucent="true">
            <section id="edit-myaccount" class="hs-section">
                <div class="row mb-4 text-center">
                    <div class="col-md-12">
                        <h1 class="page-title pt-0 mb-2">
                            {{editUserStrings.header}}
                        </h1>
                        <router-link to="/" class="hs-primary-text-color-light">{{editUserStrings.goBack}}</router-link>
                    </div>
                </div>

                      <div class="row">
                          <div class="col-md-2"></div>
                          <div class="col-md-8">
                              <sign-up-form  @success-message="updateSuccessMessage" @update-loading="updateLoading"></sign-up-form>
                          </div>
                          <div class="col-md-2"></div>
                      </div>
            </section>
            </VLoader>   
   

        `
}

export  {EditUserAccount}
