import {Pagination} from "../../../../../../admin/modules/dashboard/resources/components/Pagination.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {AnaToolTip} from "../../../../../../inc/global_components/tool-tip/ana-tool-tip.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const Accounts = {
    data() {
        return {
            accounts: [],
            active_account: {},
            user: {},
            loading: false,
            error: false,
            tooltip:undefined,
            afterDelete:false,
            accountIndexStrings: VueUiStrings.clientFrontEnd.dashboard.accounts.index
        };
    },
    components: {
        LoadingBar,
        LoadingError,
        Pagination,
        AnaToolTip
    },
    mixins:[ReusableFunctions],
    mounted() {
        //todo get all
        this.getAccounts();
    },

    watch: {
        searchText: function (current) {
            if (current === '') {
                this.search();
            }
        },
    },
    methods: {
        getAccounts() {
            let user = localStorage.getItem('user');
            user = JSON.parse(user);
            let active_account = localStorage.getItem('active_account');
            if (!active_account){
                active_account = JSON.parse(active_account);
                this.active_account =  accounts.find((account) => account.default_account == 1);
            }else{
                active_account = JSON.parse(active_account);
                this.active_account = active_account;
            }

            this.accounts = user.accounts;
            this.user = user;

            if (this.afterDelete){
                this.closeLoader();
            }
        },
        switchAccount(account) {
            const active_account = JSON.stringify(account)
            localStorage.setItem('active_account', active_account)
            location.reload();
        },
        deleteAccount(account) {
            const confirm_delete = confirm(this.accountIndexStrings.confirm + account.name + "?");
            if (confirm_delete) {
                this.startLoader();
                const data = new FormData();
                data.append("action", "delete_account");
                data.append("account_id", account.id);
                const that = this;
                let active_account = localStorage.getItem('active_account');
                active_account = JSON.parse(active_account);
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {

                        let user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : undefined
                        if (user){
                            user.accounts = user.accounts.filter((acc)=>acc.id !== account.id);
                            localStorage.setItem('user',JSON.stringify(user))
                        }if (active_account.id == account.id){
                            localStorage.removeItem('active_account');
                        }
                        console.log("got here")
                        toastr.success(account.name + " deleted");
                        that.loading = false;
                        that.afterDelete = true;
                        that.getAccounts();

                    })
                    .catch(function (error) {
                        console.log(error);
                        that.loading = false;

                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.accountIndexStrings.errors.errorOccurred);
                        }
                        that.closeLoader();
                    });
            }
        },
        goToPage(page) {
            this.page = page
            this.getAllJobs();
        },
        accountType(account_type){
            let split_acc = account_type.split('_');
            let acc_type = '';
            if (split_acc){
                for (const acc_type_element of split_acc) {
                    acc_type = acc_type+acc_type_element+' ';
                }
            }
            return acc_type;
        }
    },
    template: `<div class="module-content-wrapper">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <table class="table table-striped">
                        <tr>
                            <th>{{accountIndexStrings.tableHeadings.account}}</th>
                            <th>{{accountIndexStrings.tableHeadings.actions}}</th>
                        </tr>
                        <tr v-for="(account,index) of accounts" :key="index">
                            <td>{{account.name ? account.name : user.fname + ' ' + user.lname}}
                                <span class="badge badge-primary mr-1">{{accountType(account.account_type)}}</span>

                                <span v-if="account.id == active_account.id"
                                      class="badge badge-info mr-1">{{accountIndexStrings.current}}</span>

                                <span v-if="account.default_account == '1'"
                                      class="badge badge-success">{{accountIndexStrings.default}}</span>

                            </td>

                            <td>
                                <i class="fa-solid fa-arrow-right-arrow-left"></i>
                                <button class="btn btn-primary btn-sm mr-1 mb-2 position-relative" @click="switchAccount(account)"

                                        @mouseover="tooltip='switch account'+index"
                                        @mouseleave="tooltip=undefined"
                                ><i
                                        class="fas fa-exchange-alt fa-2x"></i>
                                    <ana-tool-tip v-if="showToolTip(tooltip,'switch account'+index)"
                                                  message="Switch account"></ana-tool-tip>
                                </button>

                                <button v-if="account.default_account !== '1'"
                                        class="btn btn-danger btn-sm mr-1 mb-2 position-relative"
                                        @click="deleteAccount(account)"

                                        @mouseover="tooltip='delete'+index"
                                        @mouseleave="tooltip=undefined"
                                ><i
                                        class="fa fa-trash fa-2x"></i>
                                    <ana-tool-tip v-if="showToolTip(tooltip,'delete'+index)"
                                                  message="Delete"></ana-tool-tip>
                                </button>

                                <router-link :to="'/edit-account/'+account.id"
                                             class="btn btn-info btn-sm position-relative"
                                             @mouseover="tooltip='edit'+index"
                                             @mouseleave="tooltip=undefined"
                                >
                                    <i
                                            class="fa fa-pencil-alt fa-2x">

                                    </i>
                                    <ana-tool-tip v-if="showToolTip(tooltip,'edit'+index)"
                                                  message="Edit"></ana-tool-tip>
                                </router-link>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
`,
};
export {Accounts};
