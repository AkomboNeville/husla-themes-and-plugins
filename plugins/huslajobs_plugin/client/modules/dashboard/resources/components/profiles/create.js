import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
import {VLoader} from "../../../../../../inc/global_components/loader/v-loader.js";

const AddProfile = {
    data() {
        return {
            id: 0,
            categories: [],
            router: {},
            formData: {
                profileName: undefined,
                profileDescription: undefined,
                fileUpload: undefined,
                profileCategories: undefined
            },
            loading: false,
            submitting:false,
            error:false,
            maxUpload:maximum_upload,
            formErrors: {},
            profileCreateStrings:VueUiStrings.clientFrontEnd.dashboard.profiles.create
        }
    },
    watch: {
        'formData.profileName': function (currentVal, oldVal) {
            this.validateInput("profileName", currentVal, {required: true}, this.profileCreateStrings.formFields.name.label);
        },
        'formData.profileDescription': function (currentVal, oldVal) {
            this.validateInput("profileDescription", currentVal, {required: true,min:150 ,max:500}, this.profileCreateStrings.formFields.description.label);
        }
        ,
        'formData.fileUpload': function (currentVal, oldVal) {
            this.validateInput("fileUpload", currentVal, {required: true}, this.profileCreateStrings.formFields.cv.label);
        },
        'formData.profileCategories': function (currentVal, oldVal) {
            this.validateInput("profileCategories", currentVal, {required: true}, this.profileCreateStrings.formFields.categories.label);
        },

    },
    components: {
        AnaVueSelect,
        LoadingBar,
        LoadingError,
        VLoader
    },
    methods: {
        getActiveAccount() {
            let active_account = localStorage.getItem('active_account')
            active_account = JSON.parse(active_account)
            this.active_account = active_account
        },

        /**
         * function gets categories,job types, currencies, accounts
         */
        getCategories() {
            this.startLoader();
            const data = new FormData();
            data.append("action", "get_categories");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response) {

                        that.categories = response.data.map(function (e) {
                            return {'label': e.name, 'value': e.id}
                        })
                        setTimeout(()=>{
                            that.loading = false;
                            that.closeLoader();
                        },100)
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.profileCreateStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.closeLoader();
                });
        },
        // async getFile(e) {
        //     const files = e.target.files || e.dataTransfer.files
        //     if (!files.length) return
        //     const file = files[0];
        //     const file_size = (file.size / 1024 / 1024).toFixed(2) //in megabytes
        //     if (parseInt(file_size) <= maximum_upload) {
        //         // file.name = file.name.replace(/[^a-zA-Z0-9.-]/g, '-')
        //         const imageFile = {
        //             url: '',
        //             file
        //         }
        //         imageFile.url = (await this.getDataURL(file))
        //         this.formData.fileUpload = imageFile;
        //     } else {
        //         toastr.error(`${files[key].name}`);
        //         e.target.value = ''
        //     }
        // },
        // /**
        //  * get file Url
        //  * @param file:array
        //  * @returns file base64url
        //  */
        // async getDataURL(file) {
        //     return new Promise((resolve, reject) => {
        //         const reader = new FileReader()
        //         reader.onload = () => {
        //             resolve(reader.result)
        //         }
        //         reader.onerror = reject
        //
        //         reader.readAsDataURL(file)
        //     })
        // },
        submitForm() {
            this.submitting = true
            //validate inputs

            this.validateInput("profileName", this.formData.profileName, {required: true},this.profileCreateStrings.formFields.name.label);
            this.validateInput("profileDescription", this.formData.profileDescription, {required: true}, this.profileCreateStrings.formFields.description.label);
            this.validateInput("fileUpload", this.formData.fileUpload, {required: true}, this.profileCreateStrings.formFields.cv.label);
            this.validateInput("profileCategories", this.formData.profileCategories, {required: true}, this.profileCreateStrings.formFields.categories.label);


            if (Object.keys(this.formErrors).length === 0) {

                const data = new FormData();
                const that = this;

                data.append("profile_name", this.formData.profileName);
                data.append("profile_description", this.formData.profileDescription);
                data.append("profile_cv", this.formData.fileUpload.file);
                data.append("profile_categories", this.formData.profileCategories);
                data.append("account_id", this.active_account.id);
                data.append('my_account_nonce', document.getElementById("huslajobs-my-account-nonce").value);
                data.append("action", "add_profile");

                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data);
                            that.successMessage = response.data
                            localStorage.removeItem('user') // force getting new data from database
                            that.router.push('/')

                        }
                        that.submitting = false;

                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.profileCreateStrings.errors.errorOccurred);
                        }
                        that.submitting = false;

                    });

            } else {
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }
        },
    },
    mixins:[ReusableFunctions],
    mounted() {
        this.router = VueRouter.useRouter();
        this.getActiveAccount();
        this.getCategories();
    },
    // language=HTML
    template:
        `<VLoader :active="loading" :translucent="true">
    <div v-if="error">
        <loading-error @update="getCategories"></loading-error>
    </div>
            
        <section v-else class="my-account-profile hs-section" >
            <div class="row mb-4 text-center">
                <div class="col-md-12">
                    <h1 class="page-title pt-0 mb-2">
                        {{profileCreateStrings.headings.create}}
                    </h1>
                    <router-link to="/" class="hs-primary-text-color-light">{{profileCreateStrings.goBack}}</router-link>
                </div>
            </div>
            <div class="row mb-1">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form class="husla-form mt-4" @submit.prevent="submitForm">
                        <!--     Show this part when user is posting for the first time or is using a job seeker account                             -->
                        <div class="mb-4">
                            <input type="text" class="form-control" :placeholder="profileCreateStrings.formFields.name.label"
                                   v-model="formData.profileName"/>
                            <span class="text-danger validation-error"
                                  v-if="formErrors.profileName">{{formErrors.profileName}}</span>
                        </div>
                        <div class="mb-3">
                                <textarea class="form-control" :placeholder="profileCreateStrings.formFields.description.placeholder"
                                          v-model="formData.profileDescription" rows="5"></textarea>
                            <span class="text-danger validation-error"
                                  v-if="formErrors.profileDescription">{{formErrors.profileDescription}}</span>
                        </div>
                        <div class="mb-4">
                            <ana-vue-select v-model="formData.profileCategories"
                                            :options="categories" :placeholder="profileCreateStrings.formFields.categories.label"
                                            mode="multiple" :searchable="true"></ana-vue-select>

                            <span class="text-danger validation-error"
                                  v-if="formErrors.profileCategories">{{formErrors.profileCategories}}</span>
                        </div>
                        <div class="mb-5">
                            <div class="d-flex bg-light flex-wrap align-items-end py-5 px-3">

                                <div class="profile-image-preview mr-5">
                                  
                                    <a v-if="formData.fileUpload?.url" :href="formData.fileUpload?.url" target="_blank">
                                        <img :src="cvUrl(formData.fileUpload?.file.name)" alt="microsoft">
                                    </a>
                                    <div>
                                        <p class="m-0">{{profileCreateStrings.formFields.cv.tips.fileTypes}}: pdf,word</p>
                                        <p class="m-0">{{profileCreateStrings.formFields.cv.tips.maxSize}}:{{maxUpload}}M</p>
                                    </div>

                                </div>
                                <div class="profile-image-btn">
                                    <label for="cv"
                                           class="hs-btn hs-btn-gray cursor-pointer">
                                        <input type="file" name="cv"
                                               accept="application/pdf"
                                               id="cv" class="d-none" @input="getFile">
                                        <span v-if="formData.fileUpload">{{profileCreateStrings.formFields.cv.buttons.changeCv}}</span>
                                        <span v-else>{{profileCreateStrings.formFields.cv.buttons.uploadCv}}</span>
                                    </label>

                                </div>


                            </div>
                            <span class="text-danger validation-error"
                                  v-if="formErrors.fileUpload">{{formErrors.fileUpload}}</span>
                        </div>

                        <div class="mt-5 d-flex justify-content-between flex-wrap">

                            <router-link to="/" class="hs-btn hs-btn-signup hs-btn-gray-outline mb-4">
                                {{profileCreateStrings.formFields.buttons.cancel}}
                            </router-link>
                            <button type="submit" class="hs-btn hs-btn-signup hs-btn-primary mb-4" :class="{'not-allowed':submitting || Object.keys(formErrors).length > 0 }"
                                    :disabled="submitting || Object.keys(formErrors).length > 0 ">
                               {{profileCreateStrings.formFields.buttons.addProfile}}
                                <i
                                        v-if="submitting" class="fas fa-spinner fa-pulse"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-2"></div>
            </div>

        </section>



</VLoader>
            `
}

export {AddProfile}