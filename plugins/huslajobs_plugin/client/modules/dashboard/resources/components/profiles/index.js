import {Pagination} from "../../../../../../admin/modules/dashboard/resources/components/Pagination.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const Profiles = {
    emits:['delete_profile'],
    data() {
        return {
            imageUrl: THEME_URL,
            profiles: [],
            active_account: {},
            user: {},
            loading: false,
            error: false,
            profileIndexStrings:VueUiStrings.clientFrontEnd.dashboard.profiles.index
        };
    },
    components: {
        LoadingBar,
        LoadingError,
        Pagination,
    },
    mounted() {
        //todo get all
        this.getProfiles();
    },
    mixins:[ReusableFunctions],

    watch: {
        searchText: function (current) {
            if (current === '') {
                this.search();
            }
        },
    },

    methods: {
        getProfiles() {
            let user = localStorage.getItem('user')
            let active_account = localStorage.getItem('active_account')
            user = JSON.parse(user)
            active_account = JSON.parse(active_account)
            this.profiles = user.profiles.filter(e => e.account_id == active_account.id)
            this.user = user;
            this.active_account = active_account
        },
        deleteProfile(profile) {
            const confirm_delete = confirm(this.profileIndexStrings.confirm + profile.name + "?");
            if (confirm_delete) {
                this.startLoader();
                const data = new FormData();
                data.append("action", "delete_profile");
                data.append("profile_id", profile.id);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        // that.getAllJobs()
                        toastr.success(profile.name + " deleted");
                        that.profiles = that.profiles.filter(e => e.id != profile.id);
                        const user = JSON.parse(JSON.stringify(that.user));
                        user.profiles = that.profiles;
                        localStorage.setItem('user', JSON.stringify(user));
                        that.$emit('delete_profile',that.profiles.length);
                        
                        that.closeLoader();
                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.profileIndexStrings.errors.errorOccurred);
                        }
                        that.closeLoader();
                    });
            }
        },
        goToPage(page) {
            this.page = page
            this.getAllJobs();
        }
    },
    template: `<div class="module-content-wrapper">
<loading-bar v-if="loading"></loading-bar>
                <div v-else>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <router-link to="/add-profile" class="hs-btn hs-btn-primary">+ {{profileIndexStrings.addProfile}}</router-link>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">

                    <table class="table table-striped">
                        <tr>
                            <th>{{profileIndexStrings.profiles}}</th>
                            <th>{{profileIndexStrings.actions}}</th>
                        </tr>
                        <tr v-for="profile of profiles" :key="profile.id">
                            <td>
                                <div class="row">
                                    <div class="col-md-1">
                                        <a :href="profile.cv" target="_blank">
                                            <img :src="imageUrl+'/document-type/pdf-logo.png'" alt="cv">
                                        </a>

                                    </div>
                                    <div class="col-md-11">
                                        <h5 class="m-0 userdata-body-title">{{decodeHtml(profile.name)}}</h5>
                                        <p>
                                            <i class="fas fa-eye"></i>
                                            <span class="ml-2">{{profileIndexStrings.public}}</span>
                                        </p>

                                    </div>
                                </div>
                            </td>

                            <td>
                                <i class="fa-solid fa-arrow-right-arrow-left"></i>
                                <router-link :to="'/edit-profile/'+profile.id"
                                             class="btn btn-info btn-sm mr-2 mb-2"><i
                                        class="fa fa-pencil-alt fa-2x"></i></router-link>
                                
                                <button class="btn btn-danger btn-sm mr-1" @click="deleteProfile(profile)"><i
                                        class="fa fa-trash fa-2x"></i></button>


                            </td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
`,
};
export {Profiles};
