import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {world_countries as WorldCountries} from "../../../../../../js/countries_data.js";
import {CountriesData} from "../../../../../../js/countries_and_codes.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const JobSeekerSearchForm = {
    props: {
        currentPage: {
            required: true,
            default: 'jobseekers'
        },
        clearSearch: {
            required: true
        },
        pageNumber: {
            required: true
        }
    },
    mixins:[ReusableFunctions],
    emits: ['search-result', 'get-category', 'is-searching'],
    data() {
        return {
            // imageUrl: THEME_URL,
            form: {
                searchText: undefined,
                state: undefined,
                country: undefined,
                category: undefined
            },
            jobCategories: undefined,
            show_results: false,
            countries: WorldCountries,
            states: [],
            page: 1,
            perPage: jobs_per_page,
            homeUrl: home_url,
            pages: 0,
            sortField: 'profile_image',
            sort: 'desc',
            submitting: false,
            searchFormStrings: VueUiStrings.clientFrontEnd.dashboard.jobSeekerSearch,
            searchParams: undefined
        }
    },
    components: {
        AnaVueSelect
    },
    mounted() {
        this.getSearchOptions();
    },
    watch: {
        'form.country': function (currentVal, oldVal) {

            this.states = [];
            if (currentVal) {
                const countriesData = CountriesData;
                let states = countriesData.filter((country) => country.code.toLowerCase() === currentVal.toLowerCase())[0].states;
                if (states.length) {
                    this.states = states.map((state) => {
                        return {
                            label: state,
                            value: state
                        }
                    });
                }
            } else {
                // this.form.country =
                this.states = [];
                this.form.state = undefined

            }

        },
        pageNumber: function (currentVal, oldVal) {
            this.page = currentVal;
            this.jobSeekerSearch()
        },
        clearSearch: function (currentVal, oldVal) {
            this.form = {
                searchText: undefined,
                state: undefined,
                country: undefined,
                category: undefined
            }
        },
    },
    methods: {
        jobSeekerSearch() {
            if (this.form.country || this.form.searchText || this.form.category || this.form.state) {
                this.submitting = true;
                if (this.currentPage !== 'jobseekers') {
                    let url = new URL(this.homeUrl + '/job-seekers');
                    const searchParams = {
                        "searchText": this.form.searchText,
                        "state": this.form.state,
                        "country": this.form.country,
                        "category": this.form.category
                    }
                    url.searchParams.set("search", true);
                    url.searchParams.set('searchParams', window.btoa(JSON.stringify(searchParams)))
                    window.location.href = url;
                } else {
                    this.$emit('is-searching', true);
                    const data = new FormData();
                    data.append("action", "search_job_seekers");
                    data.append("page", this.page);
                    data.append("perPage", this.perPage);
                    data.append("sortBy", this.sortField);
                    data.append("order", this.sort);
                    data.append("searchText", this.form.searchText ?? '');
                    data.append("country", this.form.country ?? '');
                    data.append("state", this.form.state ?? '');
                    data.append("category", this.form.category ?? '');
                    const that = this;
                    axios({
                        method: "post",
                        url: ajaxurl,
                        data: data,
                        headers: {"Content-Type": "multipart/form-data"},
                    })
                        .then(function (response) {
                            if (that.form.category) {
                                that.$emit('get-category', that.selectedCategory(that.form.category))
                            }
                            that.$emit('search-result', response.data)
                            that.submitting = false;
                        })
                        .catch(function (error) {
                            console.log(error)
                            if (error.response) {
                                toastr.error(error.response.data.data);
                            } else {
                                toastr.error(that.searchFormStrings.errors.errorOccurred);
                            }
                            that.error = true;
                            // this.$emit('is-searching', false);
                            that.submitting = false;
                        });
                }

            } else {
                toastr.error(this.searchFormStrings.errors.noFilter);
            }
        },
        getSearchOptions() {
            this.startLoader();
            const data = new FormData();
            const url = new URL(window.location.href);
            const search = url.searchParams.get("search")
            let searchParams = url.searchParams.get("searchParams")

            if (search && searchParams) {
                searchParams = JSON.parse(window.atob(searchParams));
                data.append("searchText", searchParams.searchText ?? '');
                data.append("country", searchParams.country ?? '');
                data.append("state", searchParams.state ?? '');
                data.append("category", searchParams.category ?? '');
                data.append("searchSeekers", true);
                this.searchParams = searchParams;
            }
            data.append("page", 1);
            data.append("perPage", this.perPage);
            data.append("action", "job_seeker_search_options");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response.data.categories.data.length) {
                        that.jobCategories = response.data.categories.data.map((category) => {
                            return {
                                label: category.name,
                                value: category.id,
                            }
                        });

                    } else {
                        that.jobCategories = response.data.categories.data
                    }
                    if (search) {
                        that.$emit('search-result', response.data.accounts);
                        that.setFormFields();
                    }

                    that.loading = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.searchFormStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                });
        },
        selectedCategory(id) {
            const categories = this.jobCategories;
            return categories.filter((category) => parseInt(category.value) === parseInt(id))[0].label;
        },
        setFormFields() {
            if (this.searchParams) {

                if (this.searchParams.category) {
                    this.form.category = this.searchParams.category
                }
                if (this.searchParams.searchText) {
                    this.form.searchText = this.searchParams.searchText
                }
                if (this.searchParams.country) {
                    this.form.country = this.searchParams.country
                }
                if (this.searchParams.state) {
                    this.form.state = this.searchParams.state
                }

            }
        }
    },
    template:
        `<form action="" id="search-form" @submit.prevent="jobSeekerSearch">
        <div class="hs-desktop-only">
        <div class="hs-border-1 mb-4 bg-white p-2">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-group icon-left position-relative">
                        <i class="fas fa-search icon"></i>
                        <input type="text" class="br-1 form-control" v-model="form.searchText"
                               :placeholder="searchFormStrings.formFields.searchText.placeholder">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="d-flex justify-content-between align-items-center">
                        <AnaVueSelect id="job-seeker-search"  :options="jobCategories"
                                      :placeholder="searchFormStrings.formFields.category.placeholder" v-model="form.category" class="flex-grow-1"></AnaVueSelect>
                        <button type="button" class="hs-btn hs-btn-primary hs-desktop-only"
                                 @click="jobSeekerSearch">{{searchFormStrings.formFields.button}}
                                  <i v-if="submitting" class="fas fa-spinner fa-pulse"></i>
                                 </button>
                    </div>
                </div>
    
            </div>
        </div>
        <div class="row">
            <div class="col-md-11" style="margin: 0 auto">
                <div class="d-flex form-filters justify-content-center flex-wrap">
                    <AnaVueSelect v-model="form.country" class="mr-3 mb-2 filter-item"
                                  :placeholder="searchFormStrings.formFields.country.placeholder" :options="countries"></AnaVueSelect>
                    <AnaVueSelect v-model="form.state" class="mr-3 mb-2 filter-item"
                                  :placeholder="searchFormStrings.formFields.state.placeholder" :options="states" :disabled="!form.country"></AnaVueSelect>
   
                </div>
            </div>
        </div>
       
        </div>
        
<!--        mobile-->

        <div class="hs-mobile-only">
        <div class="hs-border-1 mb-4 p-2">
            <div class="row">
                <div class="col-md-6 mb-2">
                    <div class="input-group icon-left position-relative">
                        <i class="fas fa-search icon"></i>
                        <input type="text" class="form-control" v-model="form.searchText"
                               placeholder="Search text ...">
                    </div>
                </div>
                <div class="col-md-6 mb-2">
                    <div class="d-flex justify-content-between">
                        <AnaVueSelect id="job-seeker-search" :options="jobCategories"
                                      placeholder="Select category" v-model="form.category" class="w-100"></AnaVueSelect>
                    </div>
                </div>
    
            </div>
            <div class="row form-filters">
    
                <div class="col-md-12 mb-2">
                    <AnaVueSelect v-model="form.country" class="filter-item"
                                  placeholder="Country" :options="countries"></AnaVueSelect>
                </div>
                <div class="col-md-12 mb-2">
                    <AnaVueSelect v-model="form.state" class="filter-item"
                                  placeholder="State" :options="states" :disabled="!form.country"></AnaVueSelect>
    
                </div>
                
                                <div class="col-md-12">

                    <button type="submit" class="hs-btn hs-btn-primary w-100">Find Jobs
                        Seekers
                    </button>
    
                </div>
    
            </div>
    
        </div>
        
        </div>
</form>

        `
}

export {JobSeekerSearchForm}