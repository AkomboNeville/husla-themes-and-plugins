import {PaginationComponent} from "../pagination/pagination.js";
import {JobSeekerCard} from "../../../../job-seekers/resources/componennts/job-seeker-card.js";
import {Pagination} from "../../../../../../admin/modules/dashboard/resources/components/Pagination.js";

const JobSeekerSearchResults = {
    emits:['clear-search','on-update-page'],
    props: {
        options: {
            required: true
        },
        totalItems: {
            required: true
        },
        job_category:{
            type: String,
            required: false,
            default : ""
        },
        totalPages: {
            required: true
        }
    },
    data() {
        return {
            page:1,
            searchResultsStrings:VueUiStrings.clientFrontEnd.dashboard.jobSeekerSearch
        }
    },
    components: {
        JobSeekerCard,
        Pagination
    },
    methods:{
        clearSearch(){
            this.$emit('clear-search',true);
        },
        goToPage(page) {
            this.page = page;
            this.$emit('on-update-page',this.page);
        },
    },
    template:`<div class="search-results mt-5">
    <!--         search results                   -->
    <div class="d-flex justify-content-end">
        <span class="cancel-search" @click="clearSearch" :title="searchResultsStrings.tip"><i
                class="fas fa-times"></i></span>
    </div>

    <div v-if="totalItems > 0">
        <div class="row mb-1">
            <div class="col-md-12">
            <p class="hs-title">{{totalItems}} 
                <span v-if="totalItems > 1"> {{searchResultsStrings.jobSeekersFound}}</span>
                <span v-else> {{searchResultsStrings.jobSeekerFound}}</span>
            </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <!--          jobseekers card           -->
                    <div v-for="(job_seeker,index) in options" class="col-md-4 hs-seeker-card-wrapper" :key="index" :job_category="job_category">
                        <job-seeker-card :job_seeker_data="[job_seeker]" :job_category="job_category"></job-seeker-card>
                    </div>
                </div>
                                <pagination v-if="totalPages>1" :page="page" :pages="totalPages"
                                            @update="goToPage">
                                </pagination>
            </div>
        </div>
    </div>
    <section v-else class="hs-section">
        <div class="text-center">
            <p class="hs-subtitle">{{searchResultsStrings.noJobSeekers}}</p>
        </div>
    </section>


</div>

        `
}

export {JobSeekerSearchResults}