import {JobSearchForm} from "./job-search/job-search-form.js";
import {JobSearchResults} from "./job-search/job-search-results.js";
import {JobSeekerSearchForm} from "./job-seeker-search/job-seeker-search-form.js";
import {AccountJobs} from "./job/index.js";
import {Jobs} from "../../../../../admin/modules/dashboard/resources/components/jobs/index.js";
import {Accounts} from "./accounts/index.js";
import {Profiles} from "./profiles/index.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {JobSearch} from "./job-search/job-search.js";
import {DefaultAccount} from "./DefaultAcount.js";
import {AccJobApplications} from './job-applications/index.js'
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";
import {JobSeekerSearch} from "./job-seeker-search/job-seeker-search.js";
import {AnaToolTip} from "../../../../../inc/global_components/tool-tip/ana-tool-tip.js";
import {ReusableFunctions} from "../../../../../js/functions.js";

const Dashboard = {
    data() {
        return {
            imageUrl: THEME_URL,
            showResults: false,
            userId: user_id,
            user: undefined,
            homeUrl: home_url,
            active_account: undefined,
            accountJobs: 0,
            loading: false,
            error: false,
            loggingOut:false,
            activation_status:user_activation_status,
            userDataHeaders: [ {key: 'sub_accounts', value: VueUiStrings.clientFrontEnd.dashboard.dashboard.dataHeaders.subAccounts, total: 0}],
            defaultHeader: 'sub_accounts',
            membership_plan:undefined,
            payment_complete,
            tooltip : 'upload resume',
            dashboardStrings:VueUiStrings.clientFrontEnd.dashboard.dashboard
        }

    },
    components: {
        JobSearch,
        JobSeekerSearchForm,
        Jobs,
        Accounts,
        Profiles,
        LoadingBar,
        LoadingError,
        DefaultAccount,
        AccJobApplications,
        VLoader,
        JobSeekerSearch,
        AnaToolTip

    },
    mounted() {
        this.logOutUser();
        if (!this.loggingOut){
            this.getUser();
        }


    },
    mixins:[ReusableFunctions],
    destroy(){
        this.payment_complete = undefined;
    },
    methods: {
        getSearchResult(searchResult) {
            this.showResults = searchResult;
        },
        updateProfileCount(total){
            this.userDataHeaders =this.userDataHeaders.map((header)=>{
                if (header.key === 'profiles'){
                    header.total = total
                }
                return header
            })
        },
        setUserData() {
            if (this.active_account) {
                if (this.active_account.account_type === 'individual_recruiter' || this.active_account.account_type === 'company_recruiter') {
                    this.userDataHeaders.push({
                        key: 'jobs',
                        value: this.dashboardStrings.dataHeaders.jobs,
                        total: 0
                    });

                } else {
                    const profiles = this.user.profiles.filter(e => e.account_id == this.active_account.id)
                    this.userDataHeaders.push({
                        key: 'job_applications',
                        value: this.dashboardStrings.dataHeaders.jobApplications,
                        total: 0
                    }, {
                        key: 'profiles',
                        value: this.dashboardStrings.dataHeaders.profiles,
                        total: profiles.length
                    });
                }
            }
            this.userDataHeaders = this.userDataHeaders.map((header) => {
                if (header.key === 'sub_accounts') {
                    header.total = this.user.accounts.length;
                }
                return header
            })

            if (!parseInt(this.activation_status)){
                toastr.warning(this.dashboardStrings.errors.emailUnverified)
            }
            if (this.payment_complete !== undefined){
                toastr.success(this.dashboardStrings.successMessage.paymentSuccessful);
                this.payment_complete = undefined;
            }
            if (this.user.subscriptions.length){
                for (const subscription of this.user.subscriptions) {
                    const start_date = Date.parse( new Date(subscription.start_date) ) ;
                    if(parseInt(subscription.status) === 1){
                        this.membership_plan = subscription
                    }
                }
            }
        },
        getUser() {
            // if we already have an active account in local storage, no need to fetch the database again
            let active_account = localStorage.getItem('active_account')
            let user = localStorage.getItem('user')
            this.startLoader()
            if (active_account == null || user == null) {

                const data = new FormData();
                const that = this;
                data.append("action", "get_user_with_accounts");
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            that.user = response.data;
                            const accounts = that.user?.accounts;
                            that.active_account = accounts.filter((account) => account.active_account == 1)[0];
                            if (that.active_account == undefined) {
                                const default_account = accounts.filter((account) => account.default_account == 1)[0]
                                if (default_account != undefined) {
                                    that.active_account = default_account
                                    localStorage.setItem('active_account', JSON.stringify(that.active_account))
                                    localStorage.setItem('user', JSON.stringify(that.user))
                                }
                            }

                            that.setUserData();

                        }
                        setTimeout(()=>{
                            that.closeLoader();
                            if( document.querySelector('#loading-screen')){
                                document.querySelector('#loading-screen').classList.add("d-none")
                            }
                        },200)
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.dashboardStrings.errors.errorOccurred);
                        }
                        that.loading = false;
                        that.closeLoader();
                    });
            } else {
                this.active_account = JSON.parse(active_account)
                this.user = JSON.parse(user)
                this.setUserData();
                setTimeout(()=>{
                    this.loading = false
                    this.closeLoader();
                },400)
            }

        },
        getAccountJobs(total) {
            this.userDataHeaders = this.userDataHeaders.map((header) => {
                if (header.key === 'jobs') {
                    header.total = total;
                }
                return header
            })
        },
        getApplications(total) {
            this.userDataHeaders = this.userDataHeaders.map((header) => {
                if (header.key === 'job_applications') {
                    header.total = total;
                }
                return header
            })
        },

        updateDefaultHeader(currentHeader) {
            this.defaultHeader = currentHeader;
        },
        updateLoading(value){
            this.loading = value
        },
    },
    template:
        `<VLoader :active="loading" :translucent="true">
    <div v-if="error">
        <loading-error @update="getUser"></loading-error>
    </div>
    <div v-else-if="active_account">

        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <job-seeker-search
                        @update-loading="updateLoading"
                        v-if="active_account && (active_account.account_type === 'individual_recruiter' || active_account.account_type === 'company_recruiter')" current-page="my-account"></job-seeker-search>
                <job-search @searchResult="getSearchResult"
                            v-if=" active_account &&(active_account.account_type === 'individual_job_seeker' || active_account.account_type === 'company_job_seeker')" current-page="my-account"></job-search>
            </div>
            <div class="col-md-1"></div>
        </div>
        <section id="user-profile" class="hs-section">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3 profile-image-wrapper">
                            <img v-if="0" :src="user?.avatar_id" alt="" class="profile-image">
                            <div v-else
                                 class="round hs-bg-primary d-flex profile-image justify-content-center align-items-center">
                                <h2 class="m-0 hs-gray-text-color font-weight-800" style="font-size: 3.5rem">
                                    {{decodeHtml(user?.fname[0])}}{{decodeHtml(user?.lname[0])}}</h2>
                            </div>
                        </div>
                        <div class="col-md-9 profile-description-wrapper">
                            <h3 class="mb-0 mt-5 font-weight-800">{{decodeHtml(user?.fname)}} {{decodeHtml(user?.lname)}}</h3>
                            <p class="p-0 mt-0 mb-2">
                                <span class="mr-2">{{user?.email}}</span> <span
                                    v-if="!parseInt(activation_status)" class="text-danger"><a
                                    :href="homeUrl+'/resend-verification-code'"
                                    class="text-danger">{{dashboardStrings.links.verifyEmail}}</a> 
</span>
                            </p>
                            <p v-if="membership_plan" class="hs-secondary-text-color">
                                <span>{{membership_plan.package_name}} account  <router-link
                                        v-if="membership_plan.package_name.toLowerCase() !== 'premium' "
                                        to="/upgrade-account" class="hs-primary-text-color-light">{{dashboardStrings.links.upgradeAccount}}</router-link>  </span>

                            </p>
                            <div class="d-flex flex-wrap">

                                <router-link to="/create-company"
                                             class="hs-btn hs-btn-primary hs-btn-sm  d-inline-block mr-3 mb-3 position-relative"
                                                                                          @mouseover="tooltip='create company'"
                                             @mouseleave="tooltip=undefined"
                                             >
                                    {{dashboardStrings.links.createCompany}}
                                                                        <ana-tool-tip v-if="showToolTip(tooltip,'create company')"
                                                  message="Create company"></ana-tool-tip>
                                </router-link>
                                <router-link :to="'/edit-user-profile/'+ user?.id"
                                             class="hs-btn hs-btn-gray-outline hs-btn-sm d-inline-block mr-3 mb-3 position-relative"
                                             @mouseover="tooltip='edit profile'"
                                             @mouseleave="tooltip=undefined"
                                >
                                    {{dashboardStrings.links.editProfile}}
                                    <ana-tool-tip v-if="showToolTip(tooltip,'edit profile')"
                                                  message="Edit profile"></ana-tool-tip>
                                </router-link>
                                <a :href="homeUrl+'/new-password'"
                                   class="hs-btn hs-btn-gray-outline hs-btn-sm d-inline-block mr-3 mb-3 position-relative"
                                   @mouseover="tooltip='change password'"
                                   @mouseleave="tooltip=undefined"
                                >{{dashboardStrings.links.changePassword}}
                                    <ana-tool-tip v-if="showToolTip(tooltip,'change password')"
                                                  message="Change password"></ana-tool-tip>
                                </a>
                                <router-link to="/add-profile"
                                             class="hs-btn hs-btn-gray hs-btn-sm  d-inline-block mr-3 mb-3 position-relative"
                                             @mouseover="tooltip='upload resume'"
                                             @mouseleave="tooltip=undefined"
                                             v-if=" active_account && (active_account.account_type === 'individual_job_seeker' || active_account.account_type === 'company_job_seeker')"
                                >
                                    {{dashboardStrings.links.uploadResume}}
                                    <ana-tool-tip v-if="showToolTip(tooltip,'upload resume')"
                                                  message="Upload resume"></ana-tool-tip>
                                </router-link>

                                <a :href="homeUrl+'/post-job'"
                                   class="hs-btn hs-btn-primary hs-btn-sm  d-inline-block mr-3 mb-3 position-relative"
                                   @mouseover="tooltip='post job'"
                                   @mouseleave="tooltip=undefined"
                                >
                                    {{dashboardStrings.links.postJob}}
                                    <ana-tool-tip v-if="showToolTip(tooltip,'post job')"
                                                  message="Post job"></ana-tool-tip>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div id="user-data" class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3 pr-lg-0">
                                    <div class="userdata-menu ">
                                        <div v-for="(header,index) in userDataHeaders" :key="index"
                                             @click="updateDefaultHeader(header.key)"
                                             class="userdata-menu-item cursor-pointer"
                                             :class="{'active':header.key === defaultHeader}">
                                            <span class="mr-1">{{header.value}}</span>
                                            <span class="total"
                                                  v-if="header.total===0 || header.total>0">{{header.total}}</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 pl-lg-0">
                                    <div class="userdata-body">
                                        <!--        jobs                    -->
                                        <div :class="{'d-block active':defaultHeader==='jobs' ,'d-none':defaultHeader!=='jobs'}"
                                             class="userdata-body-item">
                                            <jobs v-if="active_account" @account_jobs="getAccountJobs"
                                                  :active_account_id="active_account?.id"></jobs>
                                        </div>
                                        <!--    resume            -->
                                        <div :class="{'d-block active':defaultHeader==='profiles' ,'d-none':defaultHeader!=='profiles'}"
                                             class="userdata-body-item">
                                            <profiles v-if="!loading" @delete_profile="updateProfileCount"></profiles>
                                        </div>


                                        <!--                  job applications                  -->
                                        <div class="userdata-body-item"
                                             :class=" {'d-block active':defaultHeader==='job_applications' ,'d-none':defaultHeader!=='job_applications'} ">
                                            <acc-job-applications v-if="active_account"
                                                                  @job_applications="getApplications"
                                                                  :active_account_id="active_account?.id"></acc-job-applications>
                                        </div>
                                        <!--                  job applications                  -->
                                        <div class="userdata-body-item"
                                             :class=" {'d-block active':defaultHeader==='sub_accounts' ,'d-none':defaultHeader!=='sub_accounts'}">
                                            <accounts v-if="!loading"></accounts>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </section>
    </div>

</VLoader>


        `
}

export {Dashboard}