import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {world_countries as WorldCountries} from "../../../../../../js/countries_data.js";
import {CountriesStates} from "../../../../../../js/states.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
import {CountriesData} from "../../../../../../js/countries_and_codes.js";
import {VLoader} from "../../../../../../inc/global_components/loader/v-loader.js";

const EditAccountJob = {
    data(){
        return{
            imageUrl:THEME_URL,
            countries: WorldCountries,
            states:[],
            formData:{},
            loading:false,
            submitting:false,
            jobCategories:[],
            jobTypes:[],
            currencies:[],
            categories:[],
            formErrors: {},
            works:[],
            temporalWorks:[{label:'On site',value:'On site'},{label: 'Remote',value:'Remote'}],
            page: 1,
            perPage: 15,
            pages: 0,
            sortField: 'id',
            sort: 'desc',
            error:false,
            editJobStrings: VueUiStrings.clientFrontEnd.dashboard.job.edit

        }
    },
    mixins:[ReusableFunctions],
    watch: {
        'formData.name': function (currentVal, oldVal) {
            this.validateInput("name", currentVal, {required: true}, this.editJobStrings.formFields.jobTitle.label);
        },
        'formData.job_type_id': function (currentVal, oldVal) {
            this.validateInput("job_type_id", currentVal, {required: true},this.editJobStrings.formFields.jobType.label);
        },
        'formData.category_id': function (currentVal, oldVal) {
            this.validateInput("category_id", currentVal, {required: true}, this.editJobStrings.formFields.jobCategory.label);
        },
        'formData.location': function (currentVal, oldVal) {
            this.validateInput("location", currentVal, {required: true}, this.editJobStrings.formFields.location.label);
        },
        'formData.description': function (currentVal, oldVal) {
            this.validateInput("description", currentVal, {required: true}, this.editJobStrings.formFields.description.label);
        },
        'formData.work': function (currentVal, oldVal) {
            this.validateInput("work", currentVal, {required: true}, this.editJobStrings.formFields.work.label);
        },
        'formData.country': function (currentVal, oldVal) {
            this.validateInput("country", currentVal, {required: true}, this.editJobStrings.formFields.country.label);

            this.jobStates = [];
            if (currentVal) {
                const countriesData = CountriesData;
                let states = countriesData.filter((country) => country.code.toLowerCase() === currentVal.toLowerCase())[0].states;
                if (states.length) {
                    this.jobStates = states.map((state) => {
                        return {
                            label: state,
                            value: state
                        }
                    });
                }
            } else {
                // this.form.country =
                this.jobStates = [];
                this.formData.jobState = undefined

            }

        },
        'formData.city':function (currentVal, oldVal) {
            this.validateInput("city", currentVal, {required: true}, this.editJobStrings.formFields.city.label);
        },
        'formData.state': function (currentVal, oldVal) {
            this.validateInput("state", currentVal, {required: true}, this.editJobStrings.formFields.state.label);
        },
        'formData.currency_id': function (currentVal, oldVal) {
            if (this.formData.salary){
                this.validateInput("currency", currentVal, {required: true}, this.editJobStrings.formFields.currency.label);
            }

        },
    },
    components:{
        AnaVueSelect,
        LoadingBar,
        LoadingError,
        VLoader
    },
    methods:{
        getWorks(searchText=undefined){
            if (searchText) {
                const works = this.temporalWorks;
                this.works = works.filter((option) => option.label.toLowerCase().includes(searchText.toLowerCase()));
            } else {
                this.works = this.temporalWorks;
            }
        },
        /**
         * function gets categories,job types, currencies, accounts
         */

        getCurrencies(searchText='') {
                const data = new FormData();
                data.append("searchText", searchText);
                data.append("searchFields", "code");
                data.append("action", "get_currencies");
                data.append("page", this.page);
                data.append("perPage", this.perPage);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if(response.data.data.length){
                            that.currencies = response.data.data.map((currency)=>{
                                return{
                                    label:currency.code,
                                    value:currency
                                }
                            })
                        }else{
                            that.currencies = response.data.data;
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.editJobStrings.errors.errorOccurred);
                        }

                    });


        },
        getAllJobTypes(searchText= '') {

                const data = new FormData();
                data.append("action", "get_job_types");
                data.append("page", this.page);
                data.append("perPage", this.perPage);
                data.append("sortBy", this.sortField);
                data.append("order", this.sort);
                data.append("searchText", searchText);
                data.append("searchFields", "name");

                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},

                })
                    .then(function (response) {
                        if(response.data.data.length){
                            that.jobTypes =   response.data.data.map((jobType)=>{
                                return{
                                    label:jobType.name,
                                    value:jobType
                                }
                            });
                        }else {
                            that.jobTypes = response.data.data;
                        }

                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.editJobStrings.errors.errorOccurred);
                        }

                    });

        },
        submitForm() {
            this.submitting = true
            //validate inputs
            this.validateInput("name", this.formData?.name, {required: true}, this.editJobStrings.formFields.jobTitle.label);
            this.validateInput("job_type_id", this.formData?.job_type_id, {required: true}, this.editJobStrings.formFields.jobType.label);
            this.validateInput("category_id", this.formData?.category_id, {required: true}, this.editJobStrings.formFields.jobCategory.label);
            this.validateInput("description", this.formData?.description, {required: true},this.editJobStrings.formFields.description.label);
            this.validateInput("work", this.formData?.work, {required: true}, this.editJobStrings.formFields.work.label);
            this.validateInput("country", this.formData?.country, {required: true}, this.editJobStrings.formFields.country.label);
            this.validateInput("city", this.formData?.city, {required: true}, this.editJobStrings.formFields.city.label);
            this.validateInput("state", this.formData?.state, {required: true}, this.editJobStrings.formFields.state.label);

            if (Object.keys(this.formErrors).length === 0) {

                const data = new FormData();
                const that = this;
                data.append("name", this.formData?.name);
                data.append("job_type_id", this.formData?.job_type_id);
                data.append("category_id", this.formData?.category_id);
                data.append("description", this.formData?.description);
                data.append("experience", this.formData.experience);
                data.append("salary", this.formData.salary);
                data.append("currency_id", this.formData.currency_id);
                data.append("work", this.formData.work);
                data.append("job_id", this.formData.id);
                data.append("account_id", this.formData.account_id);
                data.append("cv_required", this.formData.cv_required);
                data.append('jobCountry', this.formData.country ?? '');
                data.append('jobCity', this.formData.city ?? '');
                data.append('jobState', this.formData.state ?? '');
                data.append('motivation_required', this.formData.motivation_required);

                data.append("action", "update_job");

                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data.message);
                            that.successMessage = response.data
                            // window.location.href= '/my-account';
                            that.$router.push({
                                path: "/",
                            });

                        }

                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.editJobStrings.errors.errorOccurred);
                        }
                        that.submitting = false;

                    });

            }else{
                this.submitting = false
            }
        },
    },
    mounted(){
        this.startLoader();
        this.works = this.temporalWorks;
        this.getJobDependencies();

        VLoader

    },
    // language=HTML
    template:
        `<VLoader :active="loading" :translucent="true">
         <div class="user-profile">
            <section class="hs-section">
                <div v-if="error">
                    <loading-error @update="getJob"></loading-error>
                </div>
                      <div v-else-if="Object.keys(this.formData).length > 0">
                          <div class="row mb-4 text-center">
                              <div class="col-md-12">
                                  <h1 class="page-title pt-0 mb-2">
                                      {{editJobStrings.header}}
                                  </h1>
                                  <router-link to="/" class="hs-primary-text-color-light">{{editJobStrings.goBack}}</router-link>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-2"></div>
                              <div class="col-md-8">
                                  <form class="husla-form mt-4" @submit.prevent="submitForm">

                                      <div class="mb-4">
                                          <input type="text" class="form-control" id="name" :placeholder="editJobStrings.formFields.jobTitle.placeholder" v-model="formData.name">
                                          <span class="text-danger" v-if="formErrors.name">{{formErrors.name}}</span>

                                      </div>
                                      <div class="mb-4">
                                          <div class="row">
                                              <div class="col-md-6">
                                                  <AnaVueSelect @search-change="getAllJobTypes" :options="jobTypes" :placeholder="editJobStrings.formFields.jobType.placeholder" v-model="formData.job_type_id"></AnaVueSelect>
                                                  <span class="text-danger" v-if="formErrors.job_type_id">{{formErrors.job_type_id}}</span>
                                              </div>
                                              <div class="col-md-6">
                                                  <AnaVueSelect @search-change="getCategories" :options="jobCategories" :placeholder="editJobStrings.formFields.jobCategory.placeholder" v-model="formData.category_id"></AnaVueSelect>
                                                  <span class="text-danger" v-if="formErrors.category_id">{{formErrors.category_id}}</span>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="mb-4">
                                          <textarea id="description" class="form-control"  rows="5"  :placeholder="editJobStrings.formFields.description.placeholder" v-model="formData.description"></textarea>
                                          <span class="text-danger" v-if="formErrors.description">{{formErrors.description}}</span>
                                      </div>
                                      <div class="row">

                                          <div class="col-md-6 mb-4">
                                              <AnaVueSelect v-model="formData.country" :options="countries"
                                                            :placeholder="editJobStrings.formFields.country.placeholder"></AnaVueSelect>
                                              <span class="text-danger validation-error" v-if="formErrors.country">{{formErrors.country}}</span>

                                          </div>
                                          <div class="col-md-6 mb-4">
                                              <AnaVueSelect v-model="formData.state" :options="jobStates"
                                                            :placeholder="editJobStrings.formFields.state.placeholder"
                                                            :fieldName="editJobStrings.formFields.state.label"
                                                            :disabled="!formData.country"></AnaVueSelect>
                                              <small>{{editJobStrings.formFields.state.tip}}</small>
                                              <span class="text-danger d-block validation-error" v-if="formErrors.state">{{formErrors.state}}</span>
                                          </div>
                                      </div>


                                      <div class="mb-4">
                                          <input type="text" name="city" id="city" v-model="formData.city"
                                                 :placeholder="editJobStrings.formFields.city.placeholder"
                                                 class="form-control"
                                                 >
                                          <span class="text-danger validation-error"
                                                v-if="formErrors.city">{{formErrors.city}}</span>
                                      </div>

                                      <div class="mb-4">
                                          <input type="number" class="form-control" id="experience"  :placeholder="editJobStrings.formFields.experience.placeholder" v-model="formData.experience"/>
                                      </div>
                                      <div class="mb-4">
                                          <div class="row">
                                              <div class="col-md-8">
                                                  <input type="number" class="form-control" id="salary"  :placeholder="editJobStrings.formFields.salary.placeholder" v-model="formData.salary" />

                                              </div>
                                              <div class="col-md-4">
                                                  <AnaVueSelect @search-change="getCurrencies" :options="currencies" :placeholder="editJobStrings.formFields.currency.placeholder" v-model="formData.currency_id" ></AnaVueSelect>
                                                  <span class="text-danger validation-error"
                                                        v-if="formErrors.currency">{{formErrors.currency}}</span>
                                              </div>

                                          </div>
                                      </div>
                                      <div class="mb-4">
                                          <AnaVueSelect @search-change="getWorks" :options="works" :placeholder="editJobStrings.formFields.work.placeholder" v-model="formData.work" ></AnaVueSelect>
                                          <span class="text-danger" v-if="formErrors.work">{{formErrors.work}}</span>
                                      </div>
                                      <div class="mb-4">
                                          <label for="cv">
                                              <span class="ml-2">{{editJobStrings.formFields.cv.placeholder}}</span>
                                          </label>
                                          <div class="d-flex flex-wrap">
                                              <div class="mr-5">
                                                  <input type="radio" class="mr-2" id="yes" name="create_account"
                                                         v-model="formData.cv_required" value="1"/>
                                                  <label for="yes"
                                                         class="form-label cursor-pointer">{{editJobStrings.yes}}</label>
                                              </div>
                                              <div>
                                                  <input type="radio" class="mr-2" id="no" name="create_account"
                                                         v-model="formData.cv_required" value="0"/>
                                                  <label cursor-pointer for="no"
                                                         class="form-label cursor-pointer">{{editJobStrings.no}}</label>
                                              </div>

                                          </div>


                                      </div>
                                      <div class="mb-4">
                                          <label for="cv_required" class="form-label">{{editJobStrings.formFields.motivation.placeholder}}</label>
                                          <div class="d-flex flex-wrap">
                                              <div class="mr-5">
                                                  <input type="radio" class="mr-2" id="motivation-yes" name="motivation"
                                                         v-model="formData.motivation_required" value="1"/>
                                                  <label for="motivation-yes" class="form-label cursor-pointer">{{editJobStrings.yes}}</label>
                                              </div>
                                              <div >
                                                  <input type="radio" class="mr-2" id="motivation-no" name="motivation"
                                                         v-model="formData.motivation_required" value="0"/>
                                                  <label cursor-pointer for="motivation-no"
                                                         class="form-label cursor-pointer">{{editJobStrings.no}}</label>
                                              </div>

                                          </div>
                                      </div>
                                      <div class="my-5"><hr></div>
                                      <div class="mt-5 d-flex justify-content-between">

                                          <router-link to="/" class="hs-btn hs-btn-signup hs-btn-gray-outline" >{{editJobStrings.formFields.buttons.cancel}}</router-link>
                                          <button type="submit" class="hs-btn hs-btn-signup hs-btn-primary" >{{editJobStrings.formFields.buttons.updateJob}}<i v-if="submitting" class="fas fa-spinner fa-pulse ml-1"></i></button>
                                      </div>

                                  </form>
                              </div>
                              <div class="col-md-2"></div>                              
                          </div>

                      </div>
          
            </section>
                       
         </div>
        </VLoader>
        `
}

export  {EditAccountJob}