import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {VLoader} from "../../../../../../inc/global_components/loader/v-loader.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
const UpgradeAccount = {
    data() {
        return {
            id: 0,
            categories: [],
            router: {},
            formData: {
                profileName: undefined,
                profileDescription: undefined,
                profileCV: undefined,
                profileCategories: undefined
            },
            packages: undefined,
            selectedPackage: undefined,
            loading: false,
            formErrors: {},
            error: false,
            submitting: false,
            // discountRate,
            woocommerce_currency,
            duration: 1,
            yearly_discount,
            biannual_discount,
            quarter_discount,
            packagePeriod:undefined,
            membership_plan:undefined,
            packagesStrings: VueUiStrings.clientFrontEnd.dashboard.packages
        }
    },
    components: {
        AnaVueSelect,
        LoadingBar,
        LoadingError,
        VLoader
    },
    methods: {
        sortPackages(a, b) {
            if (parseInt(a.price) < parseInt(b.price)) {
                return -1;
            }
            if (parseInt(a.price) > parseInt(b.price)) {
                return 1;
            }
            return 0;
        },
        getUserPlan(){
            let user = localStorage.getItem('user');
            user = JSON.parse(user)
            if (user){
                for (const subscription of user.subscriptions) {
                    const start_date = Date.parse( new Date(subscription.start_date) ) ;
                    if(parseInt(subscription.status) === 1){
                        this.membership_plan = subscription
                    }
                }
            }
        },
        getAdminPackages() {
            this.startLoader();
            const data = new FormData();
            data.append('action', 'get_admin_packages');
            data.append("page", 1);
            data.append("perPage", -1);
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response.data) {
                        that.packages = response.data.data.map((data) => {
                            if (parseInt(data.price) > 0 && data.name.toLowerCase() !== 'premium') {
                                that.selectedPackage = data;
                            }
                            return data
                        }).sort(that.sortPackages);
                        that.getUserPlan();
                    }
                    setTimeout(() => {
                        that.loading = false;
                        that.closeLoader();
                    }, 200)

                })
                .catch(function (error) {
                    console.log(error)
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.packagesStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.submitting = false;
                    that.closeLoader();

                });

        },
        getStarted(userPackage = undefined) {

            this.submitting = true;
            /**
             * todo
             * check package if free redirect to my account page
             * if premium redirect to payment page.
             */
            if (userPackage) {
                this.selectedPackage = userPackage;
            }

            let selectedUserPackage = userPackage || this.selectedPackage;

            if(parseInt(this.selectedPackage.price) == 0){
                document.location.href = '/my-account';
            }else if(parseInt(this.selectedPackage.price) > 0 && (this.membership_plan && this.selectedPackage.name == this.membership_plan.package_name)){
                toastr.success(this.packagesStrings.subscribed, 'Success');
                this.submitting = false;
            }else{
                /**
                 * get package price
                 * make call to the server and add our product to cart
                 */
                const data = new FormData();
                const that = this;
                data.append("package_name", this.selectedPackage.name);
                data.append('package_price',this.packagePrice(this.selectedPackage.price,this.duration) );
                data.append("package_id", this.selectedPackage.id);
                data.append("package_duration",this.duration);
                data.append('application_type', 'package_subscription');
                data.append("action", "hs_add_to_cart");

                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        // return;
                        if (response.data) {
                            // console.log('response.data',response.data)
                            setTimeout(() => {
                                window.location.href = response.data.checkout_url
                            }, 200)
                        }


                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.packagesStrings.errors.errorOccurred);
                        }
                        that.submitting = false;

                    });
            }

        }
        ,

        packageBenefits(benefits) {
            return benefits.split(',');
        },
        packagePrice(price,duration){
            // let price= 0;
            if(parseInt(duration) === 12) {
                this.packagePeriod = this.packagesStrings.packagePeriod.year
                price = (parseInt(price) * parseInt(duration)) * ((100 - parseInt(this.yearly_discount)) / 100)
            }else if(parseInt(duration) === 6) {
                this.packagePeriod = this.packagesStrings.packagePeriod.sixMonths
                price = (parseInt(price) * parseInt(duration)) * ( (100 - parseInt(this.biannual_discount)) / 100)
            }
            else if(parseInt(duration) === 3) {
                this.packagePeriod = this.packagesStrings.packagePeriod.threeMonths
                price = (parseInt(price)*parseInt(duration)) * ( (100 - parseInt(this.quarter_discount) ) / 100)
            }else{
                this.packagePeriod = this.packagesStrings.packagePeriod.month
                price = parseInt(price)*parseInt(duration)
            }
            return Math.ceil(price);
        }
    },
    mixins:[ReusableFunctions],
    mounted() {
        this.getAdminPackages();

    },
    // language=HTML
    template:
        `
            <VLoader :active="loading || submitting" :translucent="true">
                <div v-if="error">
                    <loading-error @update="getAdminPackages"></loading-error>
                </div>
                <div v-else-if="packages" class="my-account-packages">
                    <section class="hs-section">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="hs-page-header">
                                    <h1 class="page-title mt-0 pb-0 ">{{packagesStrings.heading}}</h1>
                                    <p class="hs-subtitle mb-4">{{packagesStrings.subHeading}}</p>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <label for="one-month" class="mr-5 cursor-pointer">
                                        <input type="radio" name="one-month" id="one-month" v-model="duration"
                                               value="1">
                                        <span class="ml-2">/month</span>
                                    </label>
                                    <label for="three-month" class="mr-5 cursor-pointer">
                                        <input type="radio" name="three-month" id="three-month" v-model="duration"
                                               value="3">
                                        <span class="ml-2">/3 months</span>

                                    </label>
                                    <label for="six-month" class="mr-5 cursor-pointer">
                                        <input type="radio" name="one-month" id="six-month" v-model="duration"
                                               value="6">
                                        <span class="ml-2">/6 months</span>
                                    </label>
                                    <label for="twelve-month" class="mr-5 cursor-pointer">
                                        <input type="radio" name="twelve-month" id="twelve-month" v-model="duration"
                                               value="12">
                                        <span class="ml-2">1 year</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="hs-section">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div v-for="(package,index) in packages"
                                         class="col-md-4 mb-4"
                                         :key="index">
                                        <div class="rounded package-card cursor-pointer pt-5 px-5 pb-4"
                                             :class="{'active':selectedPackage.name === package.name}"
                                             @click="getStarted(package)">
                                            <div class="text-center">
                                                <h5 class="package-name font-weight-800">{{decodeHtml(package.name)}}</h5>
                                                <p class="package-description">{{package.description}}</p>
                                                <h2 class="package-amount font-weight-800">
                                                    {{packagePrice(package.price,duration)}} <span>{{woocommerce_currency}}</span></h2>
                                                <p class="package-paid-per">{{packagePeriod}}</p>

                                                <button class="hs-btn hs-btn-primary mb-3"
                                                >{{packagesStrings.button}}
                                                </button>
                                            </div>
                                            <p v-for="(benefit,index) in packageBenefits( package.benefits)"
                                               class="d-flex mb-0">
                                                <span class="mr-2"><i class="fas fa-check-circle"></i></span>
                                                <span>{{decodeHtml(benefit)}}</span>
                                            </p>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </section>

                </div>

            </VLoader>
        `
}

export {UpgradeAccount}