import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
import {world_countries as WorldCountries} from "../../../../../../js/countries_data.js";

const JobSearchForm = {
    props: {
        currentPage: {
            required: true
        },
        jobLimit:{
            required: true
        },
        clearSearch:{
            required: true
        },
        pages:{
            required:true
        },
        pageNumber:{
            required: true
        }
    },
    emits:['search-result','is-loading','is-searching'],
    mixins:[ReusableFunctions],
    data() {
        return {
            // imageUrl: THEME_URL,
            form:{
                searchText:undefined,
                searchLocation:undefined,
                jobType:undefined,
                experienceLevel:undefined,
                // recruiter:undefined,
                workType:undefined,
                category:undefined,
                country:undefined,
            },
            temporalExperienceOptions:[
                {label:'O',value:0}, {label:'1yr+',value:1}, {label:'2yrs+',value:2}, {label:'3yrs+',value:3}, {label:'4yrs+',value:4},
                {label:'5yrs+',value:5}, {label:'6yrs+',value:6}, {label:'7yrs+',value:7}, {label:'8yrs+',value:8}, {label:'9yrs+',value:9},
                {label:'10yrs+',value:10},
            ],
            jobTypes:[],
            // recruiters:[],
            experienceOptions : [],
            jobCategories:[],
            workTypes:[],
            temporalWorks:[{label:'On site',value:'On site'},{label: 'Remote',value:'Remote'}],
            page: 1,
            perPage: jobs_per_page,
            // pages: 0,
            sortField: 'id',
            sort: 'desc',
            showResults:false,
            submitting:false,
            searchJobs:undefined,
            jobSearchFormStrings:VueUiStrings.clientFrontEnd.dashboard.jobSearch,
            countries: WorldCountries,
            homeUrl:home_url,
            jobParams:undefined
        }
    },
    watch: {
        pageNumber:function(currentVal, oldVal){
            this.page = currentVal;
            let limitPerPage = this.perPage;
            if (this.jobLimit && this.jobLimit !== -1){
                if (this.page == this.pages){
                    limitPerPage = this.jobLimit - ((this.page-1)*this.perPage);
                }
            }
            this.jobSearch(limitPerPage)
        },
        clearSearch:function(currentVal, oldVal){
                this.form ={
                    searchText:undefined,
                    searchLocation:undefined,
                    jobType:undefined,
                    experienceLevel:undefined,
                    // recruiter:undefined,
                    workType:undefined,
                    category:undefined,
                    country:undefined,
                }
        },
    },
    methods:{
        jobSearch(limitPerPage=0){
            if(this.form.searchLocation || this.form.searchText || this.form.jobType || this.form.country || this.form.workType || this.form.experienceLevel || this.form.category) {
                this.submitting = true;

                if (this.currentPage !== 'jobs'){
                    let category = this.form.category ? JSON.stringify(this.form.category):undefined;
                let jobType = this.form.jobType? JSON.stringify(this.form.jobType) :undefined;
                let url = new URL(this.homeUrl+'/jobs');
                const jobParams = {
                    "searchText":this.form.searchText,
                    "searchLocation":this.form.searchLocation,
                    "jobType":this.form.jobType,
                    "experienceLevel":this.form.experienceLevel,
                    "country":this.form.country,
                    "category":this.form.category,
                    "workType":this.form.workType
                }
                url.searchParams.set("jobSearch",true);
                url.searchParams.set('jobParams',window.btoa(JSON.stringify(jobParams)))
                window.location.href = url;
            }else {
                    this.$emit('is-searching', true);
                    //set country name
                    let country = this.countries.find((country) => country.value === this.form.country);
                    const data = new FormData();
                    data.append("action", "search_jobs");
                    data.append("page", this.page);
                    data.append("perPage", this.perPage);
                    data.append("sortBy", this.sortField);
                    data.append("order", this.sort);
                    data.append("limitPerPage", limitPerPage);
                    data.append("searchText", this.form.searchText ?? '');
                    data.append("searchLocation", this.form.searchLocation ?? '');
                    data.append("jobType", this.form.jobType?.value ?? 0);
                    data.append("experienceLevel", this.form.experienceLevel ?? '');
                    data.append("country", this.form.country ?? '');
                    data.append("category", this.form.category?.value ?? 0);
                    data.append("workType", this.form.workType ?? 0);
                    data.append("countryName", country?.label ?? '');
                    data.append("categoryName", this.form.category?.label ?? '');
                    data.append("jobTypeName", this.form.jobType?.label ?? '');
                    const that = this;
                    axios({
                        method: "post",
                        url: ajaxurl,
                        data: data,
                        headers: {"Content-Type": "multipart/form-data"},
                    })
                        .then(function (response) {
                            that.$emit('search-result', response.data);
                            that.submitting = false;
                            window.scrollTo({
                                top: 0,
                                left: 0,
                                behavior: 'smooth'
                            });
                        })
                        .catch(function (error) {
                            console.log(error)
                            if (error.response) {
                                toastr.error(error.response.data.message);
                            } else {
                                toastr.error(that.jobSearchFormStrings.errors.errorOccurred);
                            }
                            that.error = true;
                            // that.$emit('is-searching', false);
                            that.submitting = false;
                        });
                    this.showResults = !this.showResults;
                }

            }else{
                toastr.error(this.jobSearchFormStrings.errors.noFilter);
            }
        },
        getJobSearchOptions(){
            this.startLoader();
            this.$emit('is-loading', true);
            const data = new FormData();
            data.append("page", 1);
            data.append("perPage", this.perPage);
            const jobsUrl = new URL(window.location.href);
            const jobSearch = jobsUrl.searchParams.get("jobSearch")
            let jobParams = jobsUrl.searchParams.get("jobParams")

            if (jobSearch && jobParams){
                jobParams =  JSON.parse(window.atob(jobParams))
                let country = this.countries.find((country) => country.value === jobParams.country);
                data.append("searchText", jobParams.searchText ?? '');
                data.append("searchLocation", jobParams.searchLocation ?? '');
                data.append("jobType", jobParams.jobType?.value ?? 0);
                data.append("experienceLevel", jobParams.experienceLevel ?? '');
                data.append("country", jobParams.country ?? '');
                data.append("category", jobParams.category?.value ?? 0);
                data.append("workType", jobParams.workType ?? 0);
                data.append("countryName", country?.label ?? '');
                data.append("categoryName", jobParams.category?.label ?? '');
                data.append("jobTypeName", jobParams.jobType?.label ?? '');
                data.append("searchJobs", true);
                this.jobParams = jobParams;
            }


            data.append("action", "get_job_search_options");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {

                    that.loading = false;

                    if (response.data) {
                        if (response.data?.job_types?.data?.length){
                            that.jobTypes = response.data.job_types.data.map((jobType)=>{

                                return  {
                                    label:jobType.name,
                                    value:{
                                        label:jobType.name,
                                        value:jobType.id
                                    }
                                }
                            });
                        }else{
                            that.jobTypes = [];
                        }
                        if (response.data?.categories?.data?.length){
                            that.jobCategories = response.data.categories.data.map((category)=>{
                                return  {
                                    label:category.name,
                                    value:{
                                        label:category.name,
                                        value:category.id
                                    }
                                }
                            });
                        }else{
                            that.jobCategories = []
                        }
                    }

                    that.setFormFields();
                    if (response.data.search_results){
                        that.$emit('search-result', response.data.search_results);
                    }
                    that.$emit('is-loading', false);

                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.jobSearchFormStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.$emit('is-loading', false);
                });
        },
        setFormFields(){
            if (this.jobParams){
                if (this.jobParams.jobType){
                    this.form.jobType = this.jobParams.jobType
                }
                if (this.jobParams.category){
                    this.form.category = this.jobParams.category
                }
                if (this.jobParams.workType){
                    this.form.workType = this.jobParams.workType
                }
                if (this.jobParams.experienceLevel){
                    this.form.experienceLevel = this.jobParams.experienceLevel
                }
                if (this.jobParams.searchLocation){
                    this.form.searchLocation = this.jobParams.searchLocation
                }
                if (this.jobParams.searchText){
                    this.form.searchText = this.jobParams.searchText
                }
                if (this.jobParams.country){
                    this.form.country = this.jobParams.country
                }

            }
        }
    },
    components:{
        AnaVueSelect
    },
    mounted(){
        this.getJobSearchOptions();
        this.experienceOptions = this.temporalExperienceOptions;
        this.workTypes = this.temporalWorks;
    },
    template:
        `<form action="" id="search-form" @submit.prevent="jobSearch()">
    <div class="hs-desktop-only">
        <div class="hs-border-1 mb-4 p-2 bg-white ">
            <div class="d-flex flex-wrap">
                <div class="col-md-6 p-0">
                    <div class="input-group icon-left position-relative">
                        <i class="fas fa-search icon"></i>
                        <input type="text" v-model="form.searchText" class="br-1 form-control"
                               :placeholder="jobSearchFormStrings.formFields.searchText.placeholder">
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="d-flex justify-content-between">
                        <div class="flex-grow-1">
                            <div class="input-group icon-left position-relative">
                                <input type="text" v-model="form.searchLocation" class="bl-1 form-control"
                                       :placeholder="jobSearchFormStrings.formFields.location.placeholder">
                                <i class="fas fa-map-marker-alt icon"></i>
                            </div>
                        </div>

                        <button type="submit" class="hs-btn hs-btn-primary" :class="{'not-allowed':submitting}"
                                :disable="submitting">{{jobSearchFormStrings.formFields.button}}
                            <i v-if="submitting" class="fas fa-spinner fa-pulse"></i>
                        </button>

                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="d-flex justify-content-center flex-wrap form-filters">
                    <AnaVueSelect v-model="form.jobType" class="mr-3 mb-2 filter-item"
                                  :placeholder="jobSearchFormStrings.formFields.jobType.placeholder"
                                  :options="jobTypes"></AnaVueSelect>
                    <AnaVueSelect v-model="form.workType" class="mr-3 mb-2 filter-item"
                                  :placeholder="jobSearchFormStrings.formFields.work.placeholder"
                                  :options="workTypes"></AnaVueSelect>
                    <AnaVueSelect v-model="form.experienceLevel" class="mr-3 mb-2 filter-item"
                                  :placeholder="jobSearchFormStrings.formFields.experience.placeholder"
                                  :options="experienceOptions"></AnaVueSelect>
                    <!--                    <AnaVueSelect @search-change="getAccounts" v-model="form.recruiter" class="mr-3 mb-2 filter-item" :ajaxSearch="true" :placeholder="jobSearchFormStrings.formFields.recruiter.placeholder"-->
                    <!--                                  :options="recruiters"></AnaVueSelect>-->

                    <AnaVueSelect v-model="form.category" class="mr-3 mb-2 filter-item"
                                  :placeholder="jobSearchFormStrings.formFields.category.placeholder"
                                  :options="jobCategories"></AnaVueSelect>
                    <AnaVueSelect v-model="form.country" class="mr-3 mb-2 filter-item"
                                  :placeholder="jobSearchFormStrings.formFields.country.placeholder"
                                  :options="countries"></AnaVueSelect>
                </div>
            </div>
        </div>

    </div>
    <!--    mobile-->


    <div class="hs-mobile-only">
        <div class="hs-border-1 mb-4 p-2">
            <div class="row">
                <div class="col-md-6 mb-2">
                    <div class="input-group icon-left position-relative">
                        <i class="fas fa-search icon"></i>
                        <input type="text" v-model="form.searchText" class="br-1 form-control"
                               :placeholder="jobSearchFormStrings.formFields.searchText.placeholder">
                    </div>
                </div>
                <div class="col-md-6 mb-2">
                    <div class="d-flex justify-content-between">
                        <div class="input-group icon-left position-relative">
                            <input type="text" v-model="form.searchLocation" class="bl-1 form-control"
                                   :placeholder="jobSearchFormStrings.formFields.location.placeholder">
                            <i class="fas fa-map-marker-alt icon"></i>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row form-filters">
                <div class="col-md-12 mb-2">

                    <AnaVueSelect v-model="form.jobType" class="filter-item"
                                  :placeholder="jobSearchFormStrings.formFields.jobType.placeholder"
                                  :options="jobTypes"></AnaVueSelect>
                </div>
                <div class="col-md-12 mb-2">


                    <AnaVueSelect v-model="form.workType" class="filter-item"
                                  :placeholder="jobSearchFormStrings.formFields.work.placeholder"
                                  :options="workTypes"></AnaVueSelect>


                </div>
                <div class="col-md-12 mb-2">

                    <AnaVueSelect v-model="form.experienceLevel" class="filter-item"
                                  :placeholder="jobSearchFormStrings.formFields.experience.placeholder"
                                  :options="experienceOptions"></AnaVueSelect>


                </div>
                <!--                <div class="col-md-12 mb-2">-->

                <!--                    <AnaVueSelect v-model="form.recruiter" class="filter-item" :placeholder="jobSearchFormStrings.formFields.recruiter.placeholder"-->
                <!--                                  :options="recruiters"></AnaVueSelect>-->


                <!--                </div>-->
                <div class="col-md-12 mb-2">

                    <AnaVueSelect v-model="form.category" class="filter-item"
                                  :placeholder="jobSearchFormStrings.formFields.category.placeholder"
                                  :options="jobCategories"></AnaVueSelect>


                </div>
                <div class="col-md-12 mb-2">

                    <AnaVueSelect v-model="form.country" class="mr-3 mb-2 filter-item"
                                  :placeholder="jobSearchFormStrings.formFields.country.placeholder"
                                  :options="countries"></AnaVueSelect>


                </div>

                <div class="col-md-12">

                    <button type="submit" class="hs-btn hs-btn-primary w-100" :class="{'not-allowed':submitting}"
                            :disable="submitting">{{jobSearchFormStrings.formFields.button}}<i
                            v-if="submitting" class="fas fa-spinner fa-pulse"></i></button>


                </div>
            </div>

        </div>
    </div>
</form>

        `
}

export {JobSearchForm}