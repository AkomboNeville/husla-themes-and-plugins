import {world_countries, world_countries as WorldCountries} from "../../../../../js/countries_data.js";
// import {AnaVueSelect} from "../../../../../inc/global_components/ana_select/ana_vue_select.js";
// import {CountriesStates} from "../../../../../js/states.js";
import {AnaVueSelect} from "../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {AnaVueLanguageSelect} from "../../../../../inc/global_components/ana_select/ana-vue-language-select.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {CountriesData} from "../../../../../js/countries_and_codes.js";


// window.intlTelInput(phone, {});

const DefaultAccount = {
    data() {
        return {
            form: {
                profileImage: undefined,
                bio: undefined,
                phone: undefined,
                country: undefined,
                state: undefined,
                dob: false
            },
            registered: false,
            showPassword: false,
            countries: WorldCountries,
            states: [],
            successMessage: undefined,
            progressSteps: 4,
            imgUrl: THEME_URL,
            userAccountType: 'individual_job_seeker',
            accountTypes: [{
                label: 'I am a professional/freelancer looking for jobs',
                value: 'individual_job_seeker'
            }, {label: 'I am an employer looking to hire employees or post jobs', value: 'individual_recruiter'}],
            progressBarStyle: "width: 0%; margin-left: 5px",
            formErrors: {},
            submitting: false,
            currentStep: 1,
            selectedPackage: undefined,
            packages: [],
            jsUrl: THEME_JS_URL,
            homeUrl: home_url,
            loading: false,
            maxUpload:maximum_upload

        };
    },

    watch: {
        'form.bio': function (currentVal, oldVal) {
            this.validateInput("bio", currentVal, {required: true}, "Bio");
        },
        'form.city': function (currentVal, oldVal) {
            this.validateInput("city", currentVal, {required: true}, "City/Town");
        },
        'form.country': function (currentVal, oldVal) {
            this.validateInput("country", currentVal, {required: true}, "Country");

            this.states = [];
            if(currentVal){
                const countriesData =CountriesData;
                let states = countriesData.filter((country)=>country.code.toLowerCase() === currentVal.toLowerCase())[0].states;
                if (states.length){
                    this.states = states.map((state)=>{
                        return{
                            label:state,
                            value:state
                        }
                    });
                }
            }else{
                // this.form.country =
                this.states = [];
                this.form.state = undefined

            }

        },
        'form.state': function (currentVal, oldVal) {
            this.validateInput("state", currentVal, {required: true}, "State");
        }

    },

    components: {
        AnaVueSelect,
        AnaVuePhoneNumber: AnaVueLanguageSelect,
        LoadingBar
        // 'vue-tel-input': VueTelInput.VueTelInput
    },
    mounted() {
        // this.startLoader()
        const phone = document.querySelector("#phone-number");
        window.intlTelInput(phone, {
            utilsScript: this.jsUrl + "/intl-tel-utils.js",
            customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                return "e.g. " + selectedCountryPlaceholder;
            },
            initialCountry: "auto",
            geoIpLookup: this.getIp,
            // any initialisation options go here

        });
        this.logOut();
    },

    destroyed() {
        this.form = {
            name: undefined,
            email: undefined,
            password: undefined,
            country: undefined,
            state: undefined,
            terms: false
        }
    },
    methods: {
        getIp(callback) {
            const that = this;
            fetch('https://ipinfo.io/json?token=d8da314e209c2d', {headers: {'Accept': 'application/json'}})
                .then((resp) => resp.json())
                .catch(() => {
                    return {
                        country: 'us',
                    };
                })
                .then((resp) => {
                    callback(resp.country);

                });
        },
        getCountries(searchText) {
            if (searchText) {
                const countries = WorldCountries
                this.countries = countries.filter((option) => option.label.toLowerCase().includes(searchText.toLowerCase()));
            } else {
                this.countries = WorldCountries;
            }
        },
        validateInput(fieldKey, fieldValue, validation = {
            required: false,
            isEmail: false,
            min: 0,
            terms: true,
            isPassword: false,
            isUsername: false
        }, fieldName) {

            if (validation.required && fieldValue === undefined) {
                this.formErrors[fieldKey] = `${fieldName} field is required`;
            } else if (validation.required && fieldValue.toString().replace(/^\s+|\s+$/gm, '') === "") {
                this.formErrors[fieldKey] = `${fieldName} field is required`;
            } else if (validation.min && fieldValue.replace(/^\s+|\s+$/gm, '').length < parseInt(validation.min)) {
                this.formErrors[
                    fieldKey
                    ] = `${fieldName} field must be at least  ${validation.min} characters`;
            } else if (validation.isEmail && !fieldValue.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g)) {
                this.formErrors[
                    fieldKey
                    ] = `${fieldName} field must be a valid email e.g husla@gmail.com`;
            } else if (validation.isPassword && fieldValue !== this.form.password) {
                this.formErrors[
                    fieldKey
                    ] = `Password do not match`;
            } else if (validation.isUsername && !fieldValue.match(/^\S*$/)) {
                this.formErrors[
                    fieldKey
                    ] = `${fieldName} must not contain space`;
            } else {
                delete this.formErrors[fieldKey];
            }
        },

        setAccount() {
            //validate function
            if (this.userAccountType.includes("seeker")){
                this.validateInput("bio", this.form.bio, {required: true}, "Bio");
                this.validateInput("city", this.form.city, {required: true}, "City/Town");
                this.validateInput("country", this.form.country, {required: true}, "Country");
                this.validateInput("state", this.form.state, {required: true}, "State");
                this.validateInput("profileImage", this.form.profileImage, {required: true}, "Profile image");

            }
            if (Object.keys(this.formErrors).length === 0) {
                this.submitting = true;
                const data = new FormData();
                const that = this;
                data.append("account_type", this.userAccountType);
                data.append("action", "user_account_type");
                data.append("description", this.form.bio??'');
                data.append("city", this.form.city?? '');
                data.append("country", this.form.country?? '');
                data.append("state", this.form.state?? '');
                data.append("dob", this.form.dob?? '');
                data.append("phone", this.form.phone?? '');
                data.append("profile_image", this.form.profileImage?.file ?? '');


                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        that.submitting = false;
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error("An error occurred");
                        }
                        that.submitting = false;

                    });
            }else{
                this.submitting = false
            }
        },

        updateAccountType(accountType) {
            if (accountType.value !== this.userAccountType) {
                this.userAccountType = accountType.value;
            }
        },
        getStates(searchText) {
            const countriesData =CountriesData;
            let states = countriesData.filter((country)=>country.code.toLowerCase() === currentVal.toLowerCase())[0].states;

                this.states = states.map((state)=>{
                    return{
                        label:state,
                        value:state
                    }
                });

            if (searchText) {
                this.states = states.filter((option) => option.label.toLowerCase().includes(searchText.toLowerCase()));
            } else {
                this.states = states;
            }
        },
        async getFile(e) {
            const files = e.target.files || e.dataTransfer.files
            if (!files.length) return
            const file = files[0];
            const file_size = (file.size / 1024 / 1024).toFixed(2) //in megabytes
            if (parseInt(file_size) <= maximum_upload) {
                // file.name = file.name.replace(/[^a-zA-Z0-9.-]/g, '-')
                const imageFile = {
                    url: '',
                    file
                }
                imageFile.url = (await this.getDataURL(file))
                this.form.profileImage = imageFile;
            }else {
                toastr.error(`${files[key].name}`);
                e.target.value = ''
            }
        },
        /**
         * get file Url
         * @param file:array
         * @returns file base64url
         */
        async getDataURL(file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader()
                reader.onload = () => {
                    resolve(reader.result)
                }
                reader.onerror = reject

                reader.readAsDataURL(file)
            })
        },

    },

    template: `
            <div id="card" class="card">
              
                <div class="card-body p-0">
                            <div class="row mb-5" >
                                <div class="col-md-12">
                                    <h2 class="section-title mb-1">
                                        <span>Your Activity</span>
                                    </h2>
                                    <p>You can do any activity on the site but select one to begin with</p>
                                </div>
                            </div>  
                            <div class="row">
                                <div  class="col-md-7 ">  
                                   <form  id="hs_account_form"  method="post"  @submit.prevent="setAccount" >
                                        <div class="row">
                                            <div class="col-md-12 d-flex justify-content-between flex-column" style="margin: 0 auto"> 
                                                <div></div>
                                                <div class="d-flex justify-content-between mb-5">
                                                    <div v-for="(accountType,index) in accountTypes" class="rounded activity-card cursor-pointer text-center p-4" :key="index" :class="{'active':userAccountType === accountType.value}" @click="updateAccountType(accountType)">
                                                            <p class="pt-4 font-weight-800">{{accountType.label.toString()}}</p>
                                                            <span><i class="fas fa-check-circle"></i></span>
                                                    </div>
                                                    
                                                </div>
                                               
                                            </div>
                                        </div> 
                                        
                                        <div v-if="userAccountType==='individual_job_seeker'" class="row">
                                                                   
                                                    <div class="col-md-12 mb-4">
                                                        <label for="country">Country <span class="text-danger">*</span> </label>
                                                        <AnaVueSelect  @search-change="getCountries" v-model="form.country" :options="countries" placeholder="Select country"></AnaVueSelect>
                                                        <span class="text-danger" v-if="formErrors.country">{{formErrors.country.toString()}}</span>

                                                    </div>
                                          
                                                    <div class="col-md-12 mb-4">
                                                    <label for="country">State/Region <span class="text-danger">*</span></label>
                                                        <AnaVueSelect  @search-change="getStates" v-model="form.state" :options="states" placeholder="Select state" fieldName="State/Region"  :disabled="!form.country"></AnaVueSelect>
                                                        <small>Select country to select state</small>
                                                         <span class="text-danger d-block" v-if="formErrors.state">{{formErrors.state.toString()}}</span>

                                                    </div>
                                  
                                                    <div class="col-md-12 mb-4">
                                                        <label for="phone-number">Phone number</label>
                                                        <input type="tel" name="phone_number" v-model="form.phone" id="phone-number"  class="form-control" >
                                                      
                                                    </div>
                                                    <div class="col-md-12 mb-4">
                                                        <label for="phone-number">City/Town <span class="text-danger">*</span></label>
                                                        <input type="text" name="city" id="city" v-model="form.city" placeholder="City/Town" class="form-control">
                                                        <span class="text-danger" v-if="formErrors.city">{{formErrors.city.toString()}}</span>

                                                    </div>
                             
                                                    <div class="col-md-12 mb-4">
                                                        <label for="dob">Date of birth</label>
                                                        <input type="date" name="dob" v-model="form.dob" id="dob" placeholder="Date of birth" class="form-control" >
                                                    </div>                                   
                                                    <div class="col-md-12 mb-5">
                                                        <label for="description">Bio <span class="text-danger">*</span></label>
                                                        <textarea name="description" id="description" v-model="form.bio"  rows="5" placeholder="Tell us about yourself" class="form-control"></textarea>
                                                        <span class="text-danger" v-if="formErrors.bio">{{formErrors.bio.toString()}}</span>

                                                    </div>
                                                    
                                                    <div class="col-md-12 mb-5">
                                                    
                                                        <div class="d-flex flex-wrap align-items-end">
                                                            <div class="profile-image-preview mr-5" >
                                                                <img v-if="form.profileImage" ref="profile-image" :src="form.profileImage.url" :alt="form.profileImage.name">
                                                                <div>
                                                                    
                                                                    <p class="m-0">Image types: png,jpeg,jpg</p>
                                                                     <p class="m-0">Max size:{{maxUpload}}M</p>
                                                                </div>
                                                               
                                                            </div>
                                                            <div class="profile-image-btn">
                                                                <label for="profile-image" class="hs-btn hs-btn-gray cursor-pointer">
                                                                    <input type="file" name="profile-image" accept="image/png,image/jpeg,image/jpg" id="profile-image" class="d-none" @input="getFile">
                                                                    <span v-if="form.profileImage">Change profile image</span>
                                                                    <span v-else>Upload profile image</span>
                                                                </label>  
                                                          
                                                            </div>

                          
                                                        </div>  
                                                        <span class="text-danger" v-if="formErrors.profileImage">{{formErrors.profileImage}}</span>
                                                    </div>
                                                    
                                                </div>
                                        <div class="row mt-5">
                                            <div class="col-md-12 mb-4">
                                                    <button type="submit" class="hs-btn hs-btn-signup hs-btn-primary">Set Default Account</button>        
                                            </div>
                                        </div>        
                                   </form>
                                </div>
<!--                                <div class="col-md-5">                                      -->
<!--                                    <img :src="imgUrl +'/demo-images/employee%20benefit.png'" alt="" class="stepper-image">                                     -->
<!--                                </div>-->
                            </div>

                                                                                    
                       </div>
             </div>
`,
};

export {DefaultAccount};
