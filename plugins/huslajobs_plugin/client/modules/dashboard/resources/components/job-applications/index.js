
import {Pagination} from "../../../../../../admin/modules/dashboard/resources/components/Pagination.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
const AccJobApplications = {
    props:{
        active_account_id:{
            type:Number,
            default:0,
        }
    },
    emits:['job_applications'],
    data() {
        return {
            job_applications: [],
            page: 1,
            perPage: 15,
            loading: true,
            pages: 0,
            account_id: this.active_account_id,
            sortField: 'created_at',
            sort :'desc'
        };
    },
    components: {
        LoadingBar,
        LoadingError,
        Pagination,
    },
    mounted() {
        //todo get all
        this.getJobApplications();
    },
    mixins:[ReusableFunctions],

    watch: {
        searchText: function (current) {
            if (current === ''){
                this.search();
            }
        },
    },


    methods: {
        search() {
            this.page = 1;
            this.sortField = 'created_at';
            this.sort = 'desc';
            this.getJobApplications();
        },
        sortBy(field) {
            if (this.sortField == field) {
                this.sort = this.sort == 'desc' ? 'asc' : 'desc'
            } else {
                this.sort = 'asc';
                this.sortField = field;
            }
            this.page = 1;
            this.getJobApplications();
        },
        isSortedBy(field, sort = 'asc') {
            switch (sort) {
                case 'asc':
                    return this.sortField == field && sort == this.sort;
                case 'desc':
                    return this.sortField == field && sort == this.sort;
                default:
                    return true;
            }
        },
        getJobApplications() {
            this.startLoader();
            const data = new FormData();
            data.append("action", "get_applications");
            data.append("page", this.page);
            data.append("perPage", this.perPage);
            data.append("account_id",this.account_id)
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: { "Content-Type": "multipart/form-data" },
            })
                .then(function (response) {
                    that.job_applications = response.data.data;
                    that.pages = response.data.totalPages;
                    that.$emit('job_applications',response.data.totalItems)
                    that.loading = false;
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error("An error occurred");
                    }
                    that.loading = false;
                    that.error = true;
                });
        },
        goToPage(page){
            this.page = page;
            this.getJobApplications();
        }
    },
    template: `<div class="module-content-wrapper">
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="getAccountJobs"></loading-error>
                        </div>
                        <div v-else>

                            <div v-if="job_applications.length > 0" class="hs-table-wrapper">
                                <table class="table table-striped">
                                    <tr>
                                        <th @click="sortBy('name')" style="cursor:pointer">
                                            job
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('name','desc'),'fa-sort-up':isSortedBy('name','asc'),'fa-sort':isSortedBy('name','normal')}"></i>
                                        </th>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>city</th>
                                        <th>country</th>
                                        <th>motivation</th>
                                    </tr>
                                    <tr v-for="job_application of job_applications">

                                        <td><a :href="'/jobs/'+job_application.job_slug">{{job_application.job_name}}</a>
                                        </td>
                                        <td><span>{{decodeHtml(job_application.applicant_name)}}</span></td>
                                        <td>{{job_application.applicant_email}}</td>
                                        <td>{{job_application.city}}</td>
                                        <td>{{job_application.country}}</td>
                                        <td>{{decodeHtml(job_application.motivation)}}</td>
                                    </tr>
                                </table>
                                <pagination v-if="pages > 1" :page="page" :pages="pages"
                                            @update="goToPage"></pagination>
                            </div>
                            <div v-else>
                                <p>No application</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
};
export { AccJobApplications };
