const Layout = {
    data() {
        return {
            count: 0,
        };
    },
    methods: {
        isCurrentPage(page) {
            const thisPage = this.$route.path
            return thisPage.includes(page)
        }
    },
    template: `
            <router-view></router-view>

`,
};

export {Layout};
