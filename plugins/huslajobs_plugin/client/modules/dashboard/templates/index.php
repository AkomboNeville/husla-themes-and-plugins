<?php
global $wpdb, $user_ID;
//Check whether the user is already logged in
if (!$user_ID) {
    ?>
    <script type="text/javascript">
        window.location.href = "<?php echo home_url("/signup") ?>"
    </script>
    <?php
} else {
    $user = get_user_by('id', $user_ID);
//    redirect admin to admin area
    if (in_array('administrator', (array)$user->roles)) {
        $redirect_to = home_url('/wp-admin');
        ?>
        <script type="text/javascript">
            window.location.href = "<?php echo home_url("/wp-admin") ?>"
        </script>
        <?php
    }
    get_header();
    ?>
    <!--        -->
    <script type="text/javascript">
        let payment_complete = undefined;
        let url = new URL(window.location.href);

        if (url.searchParams.get("payment_complete")) {
            payment_complete = url.searchParams.get("payment_complete");
            localStorage.removeItem('user');
            localStorage.removeItem('active_account');
            //remove payment_complete query param
            url.searchParams.delete("payment_complete");
            window.history.replaceState(null, null, url)
        }
    </script>

    <!--   vue dom element     -->
    <div id="dashboard-content" class="content-area primary vue-component"></div>
    <!--        wp_nonce-->
    <div id="my-account-nonce">
        <?php wp_nonce_field('huslajobs-my-account', 'huslajobs-my-account-nonce'); ?>
    </div>

    <script>
        const user_activation_status = <?php echo get_user_meta($user_ID, 'user_activation_status', true);  ?>
    </script>

    <script src="<?php echo HUSLA_JOBS_CLIENT_MODULE_URL ?>/dashboard/resources/index.js" type="module"></script>

    <?php get_footer(); ?>
<?php } ?>