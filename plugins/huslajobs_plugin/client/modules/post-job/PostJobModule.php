<?php

    namespace huslajobs_client;

    use huslajobs\Account;
    use huslajobs\HuslaModule;
    use huslajobs\HuslajobsEmails;
    use huslajobs\Package;

    class PostJobModule extends HuslaModule
    {
        public function __construct()
        {
            $this->parent_module = 'client';
            $this->module = 'post-job';
            $this->addActions();
            $this->addFilters();
        }

        public function addActions()
        {
            add_action('init', function () {
                add_rewrite_rule('post-job', 'index.php?post-job=$matches[0]', 'top');

            });

            add_action('template_include', function ($template) {
                if (get_query_var('post-job') == false || get_query_var('post-job') == '') {
                    return $template;
                }
                return HUSLA_JOBS_CLIENT_MODULE_DIR . '/post-job/templates/index.php';
            });

            add_action("wp_ajax_email_exist", [$this, 'emailExist']);
            add_action('wp_ajax_nopriv_email_exist', [$this, 'emailExist']);

             add_action("wp_ajax_get_user_accounts", [$this, 'getUserAccounts']);
             add_action('wp_ajax_nopriv_get_user_accounts', [$this, 'getUserAccounts']);



        }

        public function addFilters()
        {
            add_filter('query_vars', function ($query_vars) {
                $query_vars[] = 'post-job';
                return $query_vars;
            });

        }
        public function emailExist()
        {
            $email = trim(esc_attr($_POST['email']));
            if (email_exists($email)) {
                   wp_send_json_error(__("This email address already registered.You may want to login instead.",'huslajobs'), 400);
            } else{
                echo 'continue';
            }
            wp_die();
        }

        public function getUserAccounts()
        {
                            $user_id    = intval( $_POST['user_id'] );
                            $results     = Account::where( 'wp_user_id', '=', "'" . $user_id . "'" );
                            if ($_POST['account_type']){
                                $results->andWhere('account_type', 'like', "'%" . $_POST['account_type'] . "%'");
                            }
                            echo json_encode( $results->get() );
                             wp_die();
        }
    }