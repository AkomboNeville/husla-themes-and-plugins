import {world_countries as WorldCountries} from "../../../../../js/countries_data.js";
import {AnaVueSelect} from "../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {CountriesStates} from "../../../../../js/states.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
import {LoginForm} from "../../../login/resources/components/login-form.js";
import {SignUpForm} from "../../../signup/resources/components/signup-form.js";
import {CountriesData} from "../../../../../js/countries_and_codes.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";

const PostJob = {
    refs: ['jobsModal'],
    data() {
        return {
            formData: {
                title: undefined,
                jobTypeId: undefined,
                categoryId: undefined,
                location: undefined,
                work: undefined,
                salary: undefined,
                description: undefined,
                currencyId: undefined,
                experience: undefined,
                companyName: undefined,
                companyEmail: undefined,
                companyWebsite: undefined,
                postingAs: 'company',
                userEmail: undefined,
                userName: undefined,
                companyAccount: undefined,
                accountId: undefined,
                cv: 1,
                motivation: 0,
                companyCountry: undefined,
                companyCity: undefined,
                companyState: undefined,
                jobCountry: undefined,
                jobCity: undefined,
                jobState: undefined,
                countryName: undefined
                // hasDefaultAccount:false,
            },
            registered: false,
            progressSteps: [{label: 'Job', value: 'job'}],
            imgUrl: THEME_URL ?? '',
            userId: user_id ?? 0,
            progressBarStyle: "width: 0%; margin-left: 5px",
            formErrors: {},
            submitting: false,

            selectedPackage: undefined,
            packages: [],
            loading: false,
            jobCategories: [],
            jobTypes: [],
            currencies: [],
            categories: [],
            individualAccount: undefined,
            companyAccounts: undefined,
            temporalCompanyAccounts: [],
            works: [{label: 'On site', value: 'On site'}, {label: 'Remote', value: 'Remote'}],
            temporalWorks: [{label: 'On site', value: 'On site'}, {label: 'Remote', value: 'Remote'}],
            page: 1,
            perPage: -1,
            pages: 0,
            sortField: 'id',
            sort: 'desc',
            createAction: "login",
            countries: WorldCountries,
            companyStates: [],
            jobStates: [],
            error: false,
            postJobStrings: VueUiStrings.clientFrontEnd.postJob,
            currentStep: {label: VueUiStrings.clientFrontEnd.postJob.steps.job, value: 'job'},
            testString: "Diplômé en géotechnique et valorisation de matériaux avec 2ans d'expérience"
        };
    },
    mixins: [ReusableFunctions],

    watch: {
        'formData.title': function (currentVal, oldVal) {
            this.validateInput("title", currentVal, {required: true}, this.postJobStrings.formFields.jobTitle.label);
        },
        'formData.jobTypeId': function (currentVal, oldVal) {

            this.validateInput("type", currentVal, {required: true}, this.postJobStrings.formFields.jobType.label);
        },
        'formData.categoryId': function (currentVal, oldVal) {
            this.validateInput("category", currentVal, {required: true}, this.postJobStrings.formFields.jobCategory.label);
        },
        'formData.experience': function (currentVal, oldVal) {
            if (currentVal) {
                this.validateInput("experience", currentVal, {isNumeric: true}, this.postJobStrings.formFields.experience.label);
            }
        }, 'formData.salary': function (currentVal, oldVal) {
            if (currentVal) {
                this.validateInput("salary", currentVal, {isNumeric: true}, this.postJobStrings.formFields.salary.label);
            }
        },
        'formData.description': function (currentVal, oldVal) {
            this.validateInput("description", currentVal, {
                required: true,
                min: 100
            }, this.postJobStrings.formFields.description.label);
        },
        'formData.currencyId': function (currentVal, oldVal) {
            if (this.formData.salary) {
                this.validateInput("currencyId", currentVal, {required: true}, this.postJobStrings.formFields.currency.label);
            }

        },
        'formData.work': function (currentVal, oldVal) {
            this.validateInput("work", currentVal, {required: true}, this.postJobStrings.formFields.work.label);
        },
        'formData.companyName': function (currentVal, oldVal) {
            if (this.formData.postingAs === 'company') {
                this.validateInput("companyName", currentVal, {required: true}, this.postJobStrings.formFields.companyName.label);
            }
        },
        'formData.companyEmail': function (currentVal, oldVal) {
            if (this.formData.postingAs === 'company') {
                this.validateInput("companyEmail", currentVal, {required: true}, this.postJobStrings.formFields.companyEmail.label);
            }
        },
        'formData.companyWebsite': function (currentVal, oldVal) {
            if (this.formData.postingAs === 'company') {
                this.validateInput("companyWebsite", currentVal, {isUrl: true}, this.postJobStrings.formFields.companyWebsite.label);
            }
        },
        // 'formData.userEmail': function (currentVal, oldVal) {
        //
        //     this.validateInput("userEmail", currentVal, {required: true}, "Email");
        //
        // },
        // 'formData.userName': function (currentVal, oldVal) {
        //     this.validateInput("userName", currentVal, {required: true}, "Name");
        // },
        'formData.companyAccount': function (currentVal, oldVal) {
            this.formData.accountId = currentVal;
            // this.validateInput("userName", currentVal, {required: true}, "Name");
        },
        'formData.companyCountry': function (currentVal, oldVal) {
            // this.validateInput("country", currentVal, {required: true}, "Country");

            this.companyStates = [];
            if (currentVal) {
                const countriesData = CountriesData;
                let states = countriesData.filter((country) => country.code.toLowerCase() === currentVal.toLowerCase())[0].states;
                if (states.length) {
                    this.companyStates = states.map((state) => {
                        return {
                            label: state,
                            value: state
                        }
                    });
                }
            } else {
                // this.form.country =
                this.companyStates = [];
                this.formData.state = undefined

            }

        },
        'formData.jobCountry': function (currentVal, oldVal) {
            this.validateInput("jobCountry", currentVal, {required: true}, "Country");

            this.jobStates = [];
            if (currentVal) {
                // country_name
                const countriesData = CountriesData;
                let country = countriesData.filter((country) => country.code.toLowerCase() === currentVal.toLowerCase())[0];
                this.formData.countryName = country.name;
                let states = country.states
                if (states.length) {
                    this.jobStates = states.map((state) => {
                        return {
                            label: state,
                            value: state
                        }
                    });
                }
            } else {
                // this.form.country =
                this.jobStates = [];
                this.formData.jobState = undefined

            }

        },
        'formData.jobCity': function (currentVal, oldVal) {
            this.validateInput("jobCity", currentVal, {required: true}, this.postJobStrings.formFields.city.label);
        },
        'formData.jobState': function (currentVal, oldVal) {
            this.validateInput("jobState", currentVal, {required: true}, this.postJobStrings.formFields.state.label);
        },

    },
    components: {
        AnaVueSelect,
        LoginForm,
        SignUpForm,
        LoadingBar,
        LoadingError,
        VLoader

    },

    destroyed() {
        this.formData = {
            title: undefined,
            type: undefined,
            categoryId: undefined,
            location: undefined,
            work: undefined,
            salary: undefined,
            description: undefined,
            currency: undefined,
            experience: undefined,
            companyName: undefined,
            companyEmail: undefined,
            companyWebsite: undefined,
            postingAs: "company",
            jobCountry: undefined,
            jobCity: undefined,
            jobState: undefined,
            countryName: undefined

        }
    },
    methods: {

        goToNext() {
            this.formErrors = {};
            let jobData = localStorage.getItem('jobData');
            jobData = jobData ? JSON.parse(jobData) : {}
            if (Object.keys(jobData).length === 0) {
                jobData = {
                    job: {},
                    user_info: {},
                    post_as: {},
                    review: {}
                }
            }
            if (this.currentStep.value.toLowerCase() === 'job') {
                this.validateInput("title", this.formData.title, {required: true}, this.postJobStrings.formFields.jobTitle.label);
                this.validateInput("type", this.formData.jobTypeId, {required: true}, this.postJobStrings.formFields.jobType.label);
                this.validateInput("category", this.formData.categoryId, {required: true}, this.postJobStrings.formFields.jobCategory.label);
                this.validateInput("description", this.formData.description, {required: true}, this.postJobStrings.formFields.description.label);
                this.validateInput("work", this.formData.work, {required: true}, this.postJobStrings.formFields.work.label);
                this.validateInput("jobCountry", this.formData.jobCountry, {required: true}, this.postJobStrings.formFields.country.label);
                this.validateInput("jobState", this.formData.jobState, {required: true}, this.postJobStrings.formFields.state.label);
                this.validateInput("jobCity", this.formData.jobCity, {required: true}, this.postJobStrings.formFields.city.label);
                if (this.formData.salary) {
                    this.validateInput("currencyId", this.formData.currencyId, {required: true}, this.postJobStrings.formFields.currency.label);
                }

            } else if (this.currentStep.value.toLowerCase() === 'login/signup') {

                if (parseInt(this.userId)) {
                    jobData.user_info['isLoggedIn'] = true;
                } else {
                    toastr.error(this.postJobStrings.errors.login);
                    jobData.user_info['isLoggedIn'] = false;
                    return;
                }

            } else if (this.currentStep.value.toLowerCase() === 'post job as') {
                if (this.formData.postingAs === 'company' && !this.formData.companyAccount) {
                    this.validateInput("companyName", this.formData.companyName, {required: true}, this.postJobStrings.formFields.companyName.label);
                    this.validateInput("companyEmail", this.formData.companyEmail, {required: true}, this.postJobStrings.formFields.companyEmail.label);
                }

            }

            if (Object.keys(this.formErrors).length === 0) {
                if (this.currentStep.value.toLowerCase() === 'job') {
                    jobData.job = {
                        "name": this.formData.title,
                        "jobTypeId": this.formData.jobTypeId,
                        "categoryId": this.formData.categoryId,
                        "location": this.formData.location,
                        "description": this.formData.description,
                        "experience": this.formData.experience,
                        "salary": this.formData.salary,
                        "currencyId": this.formData.currencyId,
                        "work": this.formData.work,
                        "cv": this.formData.cv,
                        "motivation": this.formData.motivation,
                        "jobCountry": this.formData.jobCountry,
                        "jobState": this.formData.jobState,
                        "jobCity": this.formData.jobCity,
                        "countryName": this.formData.countryName,
                        "completed": true,
                    }
                } else if (this.currentStep.value.toLowerCase() === 'login/signup') {
                    if (!jobData.user_info.isLoggedIn) {
                        toastr.error(this.postJobStrings.errors.login);
                        return;
                    }
                    jobData.user_info['completed'] = true;
                } else if (this.currentStep.value.toLowerCase() === 'post job as') {
                    jobData.post_as = {
                        "companyName": this.formData.companyName,
                        "companyEmail": this.formData.companyEmail,
                        "companyWebsite": this.formData.companyWebsite,
                        "postingAs": this.formData.postingAs,
                        "accountId": this.formData.accountId,
                        "companyCountry": this.formData.companyCountry,
                        "companyState": this.formData.companyState,
                        "company_city": this.formData.companyCity,
                        "completed": true
                    }
                }
                localStorage.setItem('jobData', JSON.stringify(jobData));
                this.nextStep();
            }
        },
        nextStep() {
            const currentStepIndex = this.getObjectIndex(this.progressSteps, this.currentStep.value) + 1;

            this.currentStep = this.progressSteps[currentStepIndex];
            const style = this.progressSteps.length > 3 ? "margin-left: 10px;" : "margin-left: 8px;";

            let width = ((currentStepIndex) / (this.progressSteps.length - 1) * 100);
            let gap = ((width / 5) + 5) + 'px';
            width = width + '%'
            width = `width: calc(${width} - ${gap}) !important`;
            this.progressBarStyle = style + width;
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        },
        goToPrevious() {
            const currentStepIndex = this.getObjectIndex(this.progressSteps, this.currentStep.value) - 1;
            this.currentStep = this.progressSteps[currentStepIndex];
            const style = "margin-left: 10px;"
            let width = ((currentStepIndex) / (this.progressSteps.length - 1) * 100);
            let gap = '5px';
            if (width > 0) {
                gap = ((width / 5) + 5) + 'px';
                width = width + '%'
                width = `width: calc(${width} - ${gap}) !important`;
            } else {
                width = "width: 0% !important";
            }
            this.progressBarStyle = style + width;
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        },
        updatePostingAs() {

            if (this.formData.postingAs === 'company') {
                this.formData.accountId = undefined;
            } else {
                let active_user_account = localStorage.getItem('active_account') ? JSON.parse(localStorage.getItem('active_account')) : undefined
                if (this.individualAccount) {
                    this.formData.accountId = this.individualAccount.id;
                } else if (active_user_account && active_user_account.account_type.includes('individual_recruiter')) {
                    this.formData.accountId = active_user_account.id;
                } else {
                    //    do nothing
                }
            }
        },
        getWorks(searchText = undefined) {
            if (searchText) {
                const works = this.temporalWorks;
                this.works = works.filter((option) => option.label.toLowerCase().includes(searchText.toLowerCase()));
            } else {
                this.works = this.temporalWorks;
            }
        },

        submitForm() {

            this.submitting = true
            //validate inputs

            if (this.formData.postingAs === 'company' && !this.formData.companyAccount && !this.formData.accountId) {
                this.validateInput("companyName", this.formData.companyName, {required: true}, this.postJobStrings.formFields.companyName.label);
                this.validateInput("companyEmail", this.formData.companyEmail, {required: true}, this.postJobStrings.formFields.companyEmail.label);
            }

            if (Object.keys(this.formErrors).length === 0) {
                const data = new FormData();
                const that = this;
                data.append("name", this.formData.title);
                data.append("job_type_id", this.formData.jobTypeId);
                data.append("category_id", this.formData.categoryId);
                // data.append("location", this.formData.location);
                data.append("description", this.formData.description);
                data.append("companyName", this.formData.companyName);
                data.append("companyEmail", this.formData.companyEmail);
                // data.append("userName", this.formData.userName);
                // data.append("userEmail", this.formData.userEmail);
                data.append("companyWebsite", this.formData.companyWebsite);
                data.append("postingAs", this.formData.postingAs);
                data.append("experience", this.formData.experience);
                data.append("salary", this.formData.salary);
                data.append("currency_id", this.formData.currencyId);
                data.append("work", this.formData.work);
                data.append("account_id", this.formData.accountId ?? '');
                data.append('cv_required', this.formData.cv);
                data.append('motivation_required', this.formData.motivation);
                data.append("frontEnd", true);
                data.append('companyCountry', this.formData.companyCountry ?? '');
                data.append('companyCity', this.formData.companyCity ?? '');
                data.append('companyState', this.formData.companyState ?? '');
                data.append('jobCountry', this.formData.jobCountry ?? '');
                data.append('jobCity', this.formData.jobCity ?? '');
                data.append('jobState', this.formData.jobState ?? '');
                data.append('jobState', this.formData.jobState ?? '');
                data.append("country_name", this.formData.countryName ?? '');
                // data.append('hasDefaultAccount',this.formData.hasDefaultAccount)
                // data.append('my_account_nonce', document.getElementById("huslajobs-my-account-nonce").value);
                data.append("action", "save_job");
                // return

                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data.message);
                            localStorage.removeItem('active_account');
                            localStorage.setItem('jobData',null);
                            localStorage.removeItem('jobData');
                            localStorage.removeItem('user');
                            localStorage.setItem('active_account', JSON.stringify(response.data?.account))
                            localStorage.setItem('user', JSON.stringify(response.data?.user_data));

                            window.location.href = home_url + '/my-account'//response.data.redirect_url;
                        }
                        that.submitting = false;
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.postJobStrings.errors.errorOccurred);
                        }
                        that.submitting = false;
                    });
            } else {
                this.submitting = false
            }
        },
        getUserAccounts() {
            let user_data = localStorage.getItem('user');
            if (user_data) {
                user_data = JSON.parse(localStorage.getItem('user'));
                const companyAccounts = [];
                for (const account of user_data.accounts) {
                    if (account.account_type === 'individual_recruiter') {
                        this.individualAccount = account;
                    } else if (account.account_type === 'company_recruiter') {
                        companyAccounts.push(account);
                    } else {
                        //    do nothing
                    }
                }
                if (companyAccounts.length) {
                    this.companyAccounts = companyAccounts.map((account) => {
                        return {
                            label: account.name,
                            value: account.id
                        }
                    });
                }
            } else {
                this.startLoader();
                const data = new FormData();
                const that = this;
                data.append("user_id", this.userId);
                data.append("action", "get_user_accounts");
                data.append('perPage', -1);

                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            const accounts = response.data;

                            if (accounts.length) {
                                // that.hasDefaultAccount = true;
                                const companyAccounts = [];

                                for (const account of accounts) {
                                    if (account.account_type === 'individual_recruiter') {
                                        that.individualAccount = account;
                                    } else if (account.account_type === 'company_recruiter') {
                                        companyAccounts.push(account);
                                    } else {
                                        //    do nothing
                                    }
                                }
                                if (companyAccounts.length) {
                                    that.companyAccounts = companyAccounts.map((account) => {
                                        return {
                                            label: account.name,
                                            value: account.id
                                        }
                                    });
                                }

                                // that.temporalCompanyAccounts = that.companyAccounts;
                            }
                        }
                        that.submitting = false;
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.postJobStrings.errors.errorOccurred);
                        }
                        that.submitting = false;
                    });
            }
        },
        getCompanyAccounts(searchText) {
            if (searchText) {
                const accounts = this.temporalCompanyAccounts;
                this.companyAccounts = accounts.filter((option) => option.label.toLowerCase().includes(searchText.toLowerCase()));
            } else {
                this.companyAccounts = this.temporalCompanyAccounts;
            }
        },
        setCurrentStep() {
            //job data
            let jobData = localStorage.getItem('jobData');

            if (jobData) {
                jobData = JSON.parse(jobData);
                if (jobData.job) {
                    this.formData.title = jobData.job.name;
                    this.formData.jobTypeId = jobData.job.jobTypeId;
                    this.formData.categoryId = jobData.job.categoryId;
                    this.formData.location = jobData.job.location;
                    this.formData.description = jobData.job.description;
                    this.formData.experience = jobData.job.experience;
                    this.formData.salary = jobData.job.salary;
                    this.formData.currencyId = jobData.job.currencyId;
                    this.formData.work = jobData.job.work;
                    this.formData.cv = jobData.job.cv;
                    this.formData.motivation = jobData.job.motivation;
                    this.formData.jobCountry = jobData.job.jobCountry;
                    this.formData.jobState = jobData.job.jobState;
                    this.formData.jobCity = jobData.job.jobCity;
                    this.formData.countryName = jobData.job.countryName;
                    let index = this.getObjectIndex(this.progressSteps, 'job')
                    this.currentStep = this.progressSteps[index];
                }
                if (jobData.user_info) {
                    if (parseInt(this.userId)) {
                        jobData.user_info['isLoggedIn'] = true;
                        let index = this.getObjectIndex(this.progressSteps, 'login/signup') ;
                        if (index === -1){
                            index= this.getObjectIndex(this.progressSteps, 'job')
                        }
                        this.currentStep = this.progressSteps[index];
                    } else {
                        jobData.user_info['isLoggedIn'] = false;
                        let index = this.getObjectIndex(this.progressSteps, 'job')
                        this.currentStep = this.progressSteps[index];
                    }


                }
                if (jobData.post_as?.completed) {
                    this.formData.companyName = jobData.post_as.companyName;
                    this.formData.companyEmail = jobData.post_as.companyEmail;
                    this.formData.companyWebsite = jobData.post_as.companyWebsite;
                    this.formData.postingAs = jobData.post_as.postingAs;
                    this.formData.accountId = jobData.post_as.accountId;
                    this.formData.companyCountry = jobData.post_as.companyCountry;
                    this.formData.companyState = jobData.post_as.companyState;
                    this.formData.companyCity = jobData.post_as.companyCity;
                    let index = this.getObjectIndex(this.progressSteps, 'post job as')
                    this.currentStep = this.progressSteps[index];
                }
                console.log('this.progressSteps');
                console.log(this.progressSteps)

                // if (jobData.post_as?.completed && jobData.user_info?.isLoggedIn) {
                //
                // } else if (jobData.user_info?.isLoggedIn && !jobData.post_as?.completed) {
                //     let index = this.getObjectIndex(this.progressSteps,'review' )
                //     this.currentStep = this.progressSteps[index];
                //
                // }
                this.nextStep();
            }
        },
        redirectUser(data) {
            let jobData = localStorage.getItem('jobData');
            jobData = JSON.parse(jobData);
            jobData.user_info.isLoggedIn = true;
            localStorage.setItem('signupAccountId', JSON.stringify(data.accountId))
            localStorage.setItem('jobData', JSON.stringify(jobData));
            this.getUserAccounts();

            this.nextStep();
        },
        selectedJobType(jobTypeId) {
            const types = this.jobTypes;
            return types.filter((type) => type.value == jobTypeId)[0].label
        },
        selectedJobCat(categoryId) {
            const categories = this.jobCategories;
            return categories.filter((category) => category.value == categoryId)[0].label
        },
        selectedCurrency(currencyId) {
            const currencies = this.currencies;
            return currencies.filter((currency) => currency.value == currencyId)[0].label
        },
        bindModal() {
            this.loading = false;
            this.closeLoader();
            // Get the modal
            let modal = document.getElementById("jobs-modal");
            // Get the <span> element that closes the modal
            let closeModal = document.getElementById("close-modal");
            let cancelBtn = document.getElementById("cancel-btn");
            let continueBtn = document.getElementById("continue-btn");
            const pathname = window.location.pathname;
            if (localStorage.getItem('jobData')) {

//           show modal on page load
                modal.style.display = "block";
                //  show modal only once
            }

            // When the user clicks on <span> (x), close the modal
            closeModal.onclick = function () {
                modal.style.display = "none";
                localStorage.removeItem('jobData')
            };
            // When the user clicks on <span> (x), close the modal
            cancelBtn.onclick = function () {
                modal.style.display = "none";
                localStorage.removeItem('jobData')
            };
            const that = this;
            // When the user clicks on <span> (x), close the modal
            continueBtn.onclick = function () {
                modal.style.display = "none";
                //set current state
                that.setCurrentStep()
            };
            //

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                    localStorage.removeItem('jobData')

                }
            };
        }

    },
    mounted() {

        /**
         * check if user is login
         */
        if (parseInt(this.userId)) {
            const active_account = localStorage.getItem('active_account') ? JSON.parse(localStorage.getItem('active_account')) : undefined;
            /**
             * Hide posting as if current account is company recruiter
             */
            if (active_account && active_account.account_type.includes('company_recruiter')) {
                this.progressSteps = [...this.progressSteps, ...[{label: 'Review', value: 'review'}]]
                this.formData.accountId = active_account.id;
            } else {
                this.progressSteps = [...this.progressSteps, ...[{
                    label: this.postJobStrings.steps.postingAs,
                    value: 'post job as'
                }, {label: this.postJobStrings.steps.review, value: 'review'}]];
                this.getUserAccounts();
            }

        } else {
            this.progressSteps = [...this.progressSteps, ...[{
                label: this.postJobStrings.steps.login,
                value: 'login/signup'
            }, {
                label: this.postJobStrings.steps.postingAs,
                value: 'post job as'
            }, {label: this.postJobStrings.steps.review, value: 'review'}]]
        }
        this.getJobDependencies();
    },
    template: `<VLoader :active="loading


" :translucent="true">
    <div class="job-form">
        <div v-if="error">
            <loading-error @update="getJobDependencies"></loading-error>
        </div>
        <div v-else>
            <div class="row text-center">
                <div class="col-md-12">
                    <h1 class="page-title pt-0 mb-4">
                        <span v-if="currentStep.value.toLowerCase() !== 'review'">{{postJobStrings.headings.normal}}</span>
                        <span v-if="currentStep.value.toLowerCase() === 'review'">{{postJobStrings.headings.review}}</span>
                    </h1>
                </div>

            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <!--        progress bar                        -->
                <div class="col-md-8">
                    <div class="d-flex justify-content-between align-items-center sign-up-stepper-wrapper position-relative">
                        <div id="progress" :style="progressBarStyle"></div>
                        <div v-for="(step,index) in progressSteps" :key="index" :data-title="step"
                             class="sign-up-stepper d-flex justify-content-center align-items-center progress-step"
                             :class="{ 'current-step': step.value.toLowerCase()===currentStep.value.toLowerCase(), 'completed-step': getObjectIndex(progressSteps,currentStep.value) > index }">
                            <span>{{step.label}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <!--                                  steps             -->
            <div class="row mt-5">
                <div class="col-md-2">
                    <!--                    <img :src="imgUrl +'/hs-employee.png'" alt="" class="stepper-image">-->
                </div>
                <div class="col-md-8">
                    <form class="husla-form" @submit.prevent="submitForm">
                        <!--       job         -->
                        <div v-if="currentStep.value.toLowerCase() === 'job'">
                            <div class="mb-4">
                                <input type="text" class="form-control"
                                       :placeholder="postJobStrings.formFields.jobTitle.placeholder"
                                       v-model="formData.title">
                                <span class="text-danger" v-if="formErrors.title">{{formErrors.title}}</span>

                            </div>

                            <div class="row">
                                <div class="col-md-6 mb-4">
                                    <AnaVueSelect @search-change="getAllJobTypes" :options="jobTypes"
                                                  :placeholder="postJobStrings.formFields.jobType.placeholder"
                                                  v-model="formData.jobTypeId"></AnaVueSelect>
                                    <span class="text-danger" v-if="formErrors.type">{{formErrors.type}}</span>
                                </div>
                                <div class="col-md-6 mb-4">
                                    <AnaVueSelect @search-change="getCategories" :options="jobCategories"
                                                  :placeholder="postJobStrings.formFields.jobCategory.placeholder"
                                                  v-model="formData.categoryId"></AnaVueSelect>
                                    <span class="text-danger"
                                          v-if="formErrors.category">{{formErrors.category}}</span>
                                </div>
                            </div>
                            <div class="mb-4">
                            <textarea id="description" class="form-control" rows="5" row="10"
                                      :placeholder="postJobStrings.formFields.description.placeholder"
                                      v-model="formData.description"></textarea>
                                <span class="text-danger"
                                      v-if="formErrors.description">{{formErrors.description}}</span>
                            </div>
                            <div class="row">

                                <div class="col-md-6 mb-4">
                                    <AnaVueSelect v-model="formData.jobCountry" :options="countries"
                                                  :placeholder="postJobStrings.formFields.country.placeholder"></AnaVueSelect>
                                    <span class="text-danger validation-error" v-if="formErrors.jobCountry">{{formErrors.jobCountry}}</span>

                                </div>
                                <div class="col-md-6 mb-4">
                                    <AnaVueSelect v-model="formData.jobState" :options="jobStates"
                                                  :placeholder="postJobStrings.formFields.state.placeholder"
                                                  :fieldName="postJobStrings.formFields.state.label"
                                                  :disabled="!formData.jobCountry"></AnaVueSelect>
                                    <small>{{postJobStrings.formFields.state.tip}}</small>
                                    <span class="text-danger d-block validation-error" v-if="formErrors.jobState">{{formErrors.jobState}}</span>
                                </div>
                            </div>


                            <div class="mb-4">
                                <input type="text" name="city" id="city" v-model="formData.jobCity"
                                       :placeholder="postJobStrings.formFields.city.placeholder"
                                       class="form-control">
                                <span class="text-danger validation-error"
                                      v-if="formErrors.jobCity">{{formErrors.jobCity}}</span>
                            </div>


                            <div class="mb-4">
                                <input type="number" class="form-control" id="experience"
                                       :placeholder="postJobStrings.formFields.experience.placeholder"
                                       v-model="formData.experience" min="0"/>
                                <span class="text-danger validation-error"
                                      v-if="formErrors.experience">{{formErrors.experience}}</span>
                            </div>

                            <div class="row">

                                <div class="col-md-7 mb-4">
                                    <input type="number" class="form-control" id="salary"
                                           :placeholder="postJobStrings.formFields.salary.placeholder"
                                           v-model="formData.salary" min="1"/>
                                    <span class="text-danger validation-error"
                                          v-if="formErrors.salary">{{formErrors.salary}}</span>
                                </div>
                                <div class="col-md-5 mb-4">
                                    <AnaVueSelect @search-change="getCurrencies" :options="currencies"
                                                  :placeholder="postJobStrings.formFields.currency.placeholder"
                                                  v-model="formData.currencyId"></AnaVueSelect>
                                    <span class="text-danger"
                                          v-if="formErrors.currencyId">{{formErrors.currencyId}}</span>
                                </div>

                            </div>

                            <div class="mb-4">
                                <AnaVueSelect @search-change="getWorks" :options="works"
                                              :placeholder="postJobStrings.formFields.work.placeholder"
                                              v-model="formData.work"></AnaVueSelect>
                                <span class="text-danger" v-if="formErrors.work">{{formErrors.work}}</span>
                            </div>


                            <div class="mb-4">


                                <label for="cv">
                                    <span class="ml-2">{{postJobStrings.formFields.cv.placeholder}}</span>
                                </label>
                                <div class="d-flex flex-wrap">
                                    <div class="mr-5">
                                        <input type="radio" class="mr-2" id="yes" name="cv"
                                               v-model="formData.cv" value="1"/>
                                        <label for="yes"
                                               class="form-label cursor-pointer">{{postJobStrings.yes}}</label>
                                    </div>
                                    <div>
                                        <input type="radio" class="mr-2" id="no" name="cv"
                                               v-model="formData.cv" value="0"/>
                                        <label cursor-pointer for="no"
                                               class="form-label cursor-pointer">{{postJobStrings.no}}</label>
                                    </div>

                                </div>


                            </div>
                            <div class="mb-4">

                                <label for="cv">
                                    <span class="ml-2">{{postJobStrings.formFields.motivation.placeholder}}</span>
                                </label>
                                <div class="d-flex flex-wrap">
                                    <div class="mr-5">
                                        <input type="radio" class="mr-2" id="motivation-yes" name="motivation"
                                               v-model="formData.motivation" value="1"/>
                                        <label for="motivation-yes"
                                               class="form-label cursor-pointer">{{postJobStrings.yes}}</label>
                                    </div>
                                    <div>
                                        <input type="radio" class="mr-2" id="motivation-no" name="motivation"
                                               v-model="formData.motivation" value="0"/>
                                        <label cursor-pointer for="motivation-no"
                                               class="form-label cursor-pointer">{{postJobStrings.no}}</label>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <!--            Login/signup                   -->
                        <div v-if="currentStep.value.toLowerCase() === 'login/signup'">

                            <div class="d-flex justify-content-between flex-column ">
                                <div class="mb-4 row">
                                    <div class="col-md-4">
                                        <input type="radio" class="mr-2" id="login-account" name="create_account"
                                               v-model="createAction" value="login"/>
                                        <label for="login-account" class="form-label cursor-pointer">{{postJobStrings.login}}</label>
                                    </div>
                                    <div class="col-md-4">
                                        <h5 class="hs-title">{{postJobStrings.or}}</h5>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="radio" class="mr-2" id="signup-account" name="create_account"
                                               v-model="createAction" value="signup"/>
                                        <label cursor-pointer for="signup-account"
                                               class="form-label cursor-pointer">{{postJobStrings.signUp}}</label>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="mb-4 col-sm-12">
                                        <login-form v-if=" createAction=== 'login'"
                                                    @user-logged-in="redirectUser"></login-form>
                                        <sign-up-form v-else @success-message="redirectUser"
                                                      current-page="post job"></sign-up-form>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!--               post job as                   -->
                        <div v-if="currentStep.value.toLowerCase() === 'post job as'">
                            <div class="mb-4 row">
                                <div class="col-md-4">
                                    <input type="radio" class="mr-2" id="individual-account" name="posting_as"
                                           @change="updatePostingAs()" v-model="formData.postingAs"
                                           value="individual"/>
                                    <label for="individual-account" class="form-label cursor-pointer">
                                        {{postJobStrings.individual.toString()}}</label>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="hs-title">{{postJobStrings.or.toString()}}</h5>
                                </div>
                                <div class="col-md-4">
                                    <input type="radio" class="mr-2" id="company-account" name="posting_as"
                                           @change="updatePostingAs()" v-model="formData.postingAs"
                                           value="company"/>
                                    <label for="company-account" class="form-label cursor-pointer">
                                        {{postJobStrings.company.toString()}}</label>
                                </div>

                            </div>
                            <div v-if="formData.postingAs === 'company'">

                                <div v-if="companyAccounts && companyAccounts.length">
                                    <div class="mb-4">
                                        <AnaVueSelect @search-change="getCompanyAccounts" :options="companyAccounts"
                                                      :placeholder="postJobStrings.formFields.companyAccount.placeholder"
                                                      v-model="formData.companyAccount"></AnaVueSelect>
                                    </div>
                                </div>
                                <div v-if="!formData.companyAccount">
                                    <div v-if="companyAccounts && companyAccounts.length" class="mb-4 text-center">
                                        <h5 class="hs-title">{{postJobStrings.or.toString()}}</h5>
                                    </div>
                                    <div class="mb-4">
                                        <input type="text" class="form-control" id="experience"
                                               :placeholder="postJobStrings.formFields.companyName.placeholder"
                                               v-model="formData.companyName"/>
                                        <span class="text-danger"
                                              v-if="formErrors.companyName">{{formErrors.companyName.toString()}}</span>
                                    </div>
                                    <div class="mb-4">
                                        <input type="text" class="form-control"
                                               :placeholder="postJobStrings.formFields.companyEmail.placeholder"
                                               v-model="formData.companyEmail"/>
                                        <span class="text-danger"
                                              v-if="formErrors.companyEmail">{{formErrors.companyEmail.toString()}}</span>
                                    </div>
                                    <div class="mb-4">
                                        <input type="text" class="form-control"
                                               :placeholder="postJobStrings.formFields.companyWebsite.placeholder"
                                               v-model="formData.companyWebsite"/>
                                    </div>
                                    <div class="mb-4">
                                        <AnaVueSelect @search-change="getCountries" v-model="formData.companyCountry"
                                                      :options="countries"
                                                      :placeholder="postJobStrings.formFields.country.placeholder"></AnaVueSelect>

                                    </div>

                                    <div class="mb-4">
                                        <AnaVueSelect v-model="formData.companyState" :options="companyStates"
                                                      :placeholder="postJobStrings.formFields.state.placeholder"
                                                      fieldName="State/Region"
                                                      :disabled="!formData.companyCountry"></AnaVueSelect>
                                        <small>{{postJobStrings.formFields.state.tip}}</small>

                                    </div>

                                    <div class="mb-4">
                                        <input type="text" name="city" id="city" v-model="formData.companyCity"
                                               placeholder="City/Town"
                                               class="form-control"
                                        >
                                    </div>
                                </div>


                            </div>
                        </div>

                        <!--           review                            -->
                        <div v-if="currentStep.value.toLowerCase() === 'review'">
                            <div class="mb-4 row">
                                <div class="col-md-12">
                                    <h5 class="hs-title mb-4">{{formData.title}}</h5>

                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.jobTitle.label}}</h5>
                                        <p>{{selectedJobType(formData.jobTypeId)}}</p>
                                    </div>

                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.jobCategory.label}}</h5>
                                        <p>{{selectedJobCat(formData.categoryId)}}</p>
                                    </div>

                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.description.label}}</h5>
                                        <p>{{formData.description}}</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.country.label}}</h5>
                                        <p>{{selectedCountry(formData.jobCountry)}}</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.state.label}}</h5>
                                        <p>{{formData.jobState}}</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.city.label}}</h5>
                                        <p>{{formData.jobCity}}</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.experience.label}}</h5>
                                        <p>{{formData.experience}}</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.salary.label}}</h5>
                                        <p v-if="formData.salary">
                                            {{formData.salary}}{{selectedCurrency(formData.currencyId)}}</p>
                                        <p v-else>--</p>
                                    </div>

                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.work.label}}</h5>
                                        <p>{{formData.work}}</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.cv.label}}</h5>
                                        <p>{{parseInt(formData.cv) ? postJobStrings.yes :postJobStrings.no }}</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{postJobStrings.formFields.motivation.label}}</h5>
                                        <p>{{parseInt(formData.motivation) ? postJobStrings.yes :postJobStrings.no
                                            }}</p>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="my-5">
                            <hr>
                        </div>

                        <div class="mt-5 d-flex"
                             :class="{'justify-content-end':!getObjectIndex(progressSteps,currentStep.value),'justify-content-between':getObjectIndex(progressSteps,currentStep.value) }">
                        <span v-if="getObjectIndex(progressSteps,currentStep.value)"
                              class="fas fa-chevron-left cursor-pointer stepper-btn hs-round"
                              @click="goToPrevious" :disabled="submitting"></span>
                            <div v-if="(getObjectIndex(progressSteps,currentStep.value)+1) === progressSteps.length">
                                <button type="submit"
                                        class="hs-btn hs-btn-primary">{{postJobStrings.formFields.button}}<i
                                        v-if="submitting"
                                        class="fas fa-spinner fa-pulse ml-1"></i>
                                </button>
                            </div>

                            <span v-if="(getObjectIndex(progressSteps,currentStep.value)+1) !== progressSteps.length"
                                  class="fas fa-chevron-right cursor-pointer stepper-btn hs-round"
                                  :class="{'next':registered}" @click="goToNext"></span>
                        </div>

                    </form>
                </div>
                <div class="col-md-2">
                    <!--                    <img :src="imgUrl +'/hs-employee.png'" alt="" class="stepper-image">-->
                </div>
            </div>

            <!-- MODAL WINDOW -->
        </div>
        <div id="jobs-modal" class="modal">
            <div class="modal-content">
                <span style="color:black;" id="close-modal" class="close">&times;</span>
                <div class="modal-inner-content text-center">
                    <h2 style="font-size:30px !important; color:#004475; padding-bottom: 20px; font-weight: bold;">
                        {{postJobStrings.continueModal.heading}}</h2>
                    <p style="font-size:25px !important; color: #004475;">
                        <!--                        <span style="font-weight: bold;">{{postJobStrings.continueModal.subHeading}}</span>-->
                        <span>{{postJobStrings.continueModal.subHeading}}</span>
                    </p>
                    <div class="buttons">
                        <button id="cancel-btn" type="button" class="hs-btn hs-btn-primary-outline mr-5">
                            {{postJobStrings.continueModal.buttons.cancel}}
                        </button>
                        <button id="continue-btn" class="hs-btn hs-btn-primary">
                            {{postJobStrings.continueModal.buttons.continue}}
                        </button>

                    </div>
                </div>

            </div>

        </div>
    </div>
</VLoader>
`,
};

export {PostJob};
