import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";
import {ReusableFunctions} from "../../../../../js/functions.js";

const NewPassword = {
    data() {
        return {
            form:{
                new_password:undefined,
                current_password:undefined,
            },
            passwordReset:undefined,
            successMessage:undefined,
            formErrors:{},
            showPassword:false,
            submitting:false,
            loading: false,
            lost_pwd:parseInt( lost_password ),
            user_id :current_user_id,
            current_input:undefined,
            newPasswordStrings:VueUiStrings.clientFrontEnd.newPassword



        };
    },
    refs:['current_password','new_password'],
    watch: {
        'form.new_password':function(currentVal, oldVal){
            this.validateInput("new_password", currentVal,{required: true},this.newPasswordStrings.formFields.newPassword.label);
        },
        'form.current_password':function(currentVal, oldVal){
            if(!this.lost_pwd){
                this.validateInput("current_password", currentVal,{required: true},this.newPasswordStrings.formFields.currentPassword.label);
            }

        },
    },
    mixins:[ReusableFunctions],
    mounted(){
        setTimeout(()=>{
            this.closeLoader();
        },200)
    },
    components:{
        VLoader
    },
    methods: {
        newPassword(){
            /**
             * todo
             * validate form on
             */
            this.submitting = true

            //validate inputs
            this.validateInput("new_password", this.form.new_password,{required: true},this.newPasswordStrings.formFields.newPassword.label);

            if(!this.lost_pwd ){
                this.validateInput("current_password", this.form.current_password,{required: true},this.newPasswordStrings.formFields.currentPassword.label);
            }

            if (Object.keys(this.formErrors).length === 0){
                const data = new FormData();
                const that = this;
                data.append("password", this.form.new_password);
                data.append("current_password", this.form.current_password ?? '');
                data.append('user_id',this.user_id)
                data.append('wpnonce',document.getElementById("huslajobs-new-password-nonce").value);
                data.append("action", "new_password");
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: { "Content-Type": "multipart/form-data" },
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data.message, 'Success');
                            setTimeout(function(){
                                document.location.href = response.data.redirect;
                                that.submitting = false
                            }, 2000);
                        }
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error("An error occurred");
                        }
                    });
            }
            else{
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )

            }


        },
        togglePassword(type) {
            this.current_input = type
            const passwordInput = type==='new'? this.$refs.new_password : this.$refs.current_password
            if (passwordInput.getAttribute('type') == 'password') {
                this.showPassword = true
                passwordInput.setAttribute('type', 'text')
            } else {
                this.showPassword = false
                passwordInput.setAttribute('type', 'password')
            }
        },
    },

    template: `
    <section class="hs-section">


        <div class="row text-center">
            <div class="col-md-12">
                <div class="hs-page-header">
                    <h1 class="page-title mb-5 mt-0 mx-0 p-0">
                        {{newPasswordStrings.heading}}
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div v-if="successMessage">
                    <p class="mb-4">
                        {{successMessage}}
                    </p>
                    <a href="/login" class="hs-btn hs-btn-primary hs-btn-signup">Login</a>
                </div>
                <div v-else>
                    <form id="wp_signup_form" method="post" @submit.prevent="newPassword">
                        <div class="mb-4" v-if="!lost_pwd">
                            <div class="input-group icon-right position-relative">
                                <i v-if="showPassword && current_input == 'current'"
                                   class="fas fa-eye icon cursor-pointer"
                                   @click="togglePassword('current')"></i>
                                <i v-else class="fas fa-eye-slash icon cursor-pointer"
                                   @click="togglePassword('current')"></i>
                                <input type="password" class="form-control" id="current_password" ref="current_password"
                                       :placeholder="newPasswordStrings.formFields.currentPassword.placeholder"
                                       name="current_password" v-model="form.current_password">
                               
                            </div>
                             <span class="text-danger validation-error"
                                      v-if="formErrors.current_password">{{formErrors.current_password}}</span>
                        </div>
                        <div class="mb-4">

                            <div class="input-group icon-right position-relative">
                                <i v-if="showPassword && current_input == 'new'" class="fas fa-eye icon cursor-pointer"
                                   @click="togglePassword('new')"></i>
                                <i v-else class="fas fa-eye-slash icon cursor-pointer"
                                   @click="togglePassword('new')"></i>
                                <input type="password" class="form-control" id="new_password" ref="new_password"
                                       :placeholder="newPasswordStrings.formFields.newPassword.placeholder"
                                       name="new_password" v-model="form.new_password">
                               
                            </div>
                             <span class="text-danger validation-error"
                                      v-if="formErrors.new_password">{{formErrors.new_password}}</span>
                        </div>

                        <div class="mb-5">
                            <button type="submit" class="hs-btn hs-btn-primary w-100"
                                    :disabled="submitting || Object.keys(formErrors).length > 0">
                                {{newPasswordStrings.formFields.button}}<i v-if="submitting"
                                                                           class="fa fa-spinner fa-pulse"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
        <div class="mb-5"></div>
    </section>

`,
};

export {NewPassword};
