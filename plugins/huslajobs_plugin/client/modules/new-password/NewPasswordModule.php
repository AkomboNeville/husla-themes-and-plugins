<?php
    namespace huslajobs_client;

    use huslajobs\HuslaModule;

    class NewPasswordModule extends HuslaModule
    {

        public function __construct() {
            $this->parent_module = 'client';
            $this->module        = 'new-password';
            $this->addActions();
            $this->addFilters();
        }

        public function addActions() {

            add_action( 'init', function () {
                add_rewrite_rule( 'new-password', 'index.php?new-password=$matches[0]', 'top' );
            } );

            add_action( 'template_include', function ( $template ) {
                if ( get_query_var( 'new-password' ) == false || get_query_var( 'new-password' ) == '' ) {
                    return $template;
                }
                return HUSLA_JOBS_CLIENT_MODULE_DIR . '/new-password/templates/index.php';
            } );

            //  userLogin
            add_action("wp_ajax_new_password", [$this, "newPassword"]);
            add_action("wp_ajax_nopriv_new_password", [$this, "newPassword"]);
        }

        public function addFilters() {
            add_filter( 'query_vars', function ( $query_vars ) {
                $query_vars[] = 'new-password';
                return $query_vars;
            } );
        }

        public function newPassword(){
            global $user_ID;
            if (
                isset($_POST['wpnonce'])
                && wp_verify_nonce($_POST['wpnonce'], 'huslajobs-new-password')
            ) {
                $password = esc_attr($_POST['password']);
                $user_id = intval( esc_attr($_POST['user_id']) );
                $user = get_user_by( 'id', $user_id );
                $current_password = esc_attr($_POST['current_password']);
//                check if user current password matches existing passow password
                if ( $current_password  &&  ! wp_check_password( $current_password, $user->user_pass, $user->ID ) ) {
                    wp_send_json_error("The current password you entered is incorrect.",400);
                }else {
                    reset_password(get_userdata($user_id) ,$password );
                    echo json_encode(['message'=> 'Password updated','redirect'=>home_url('/login')]);
                }
            }
            else {
                wp_send_json_error("Sorry,you are not allowed to change password", 400);
            }

            wp_die();
        }
    }