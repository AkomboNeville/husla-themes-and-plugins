import {SignUp} from "./components/SignUp.js";

// VUE 3 initialization
const app = Vue.createApp({
    render() {
        return Vue.h(SignUp, {});
    },
});
app.mount("#signup-content");
