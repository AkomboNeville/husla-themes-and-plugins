<?php

namespace huslajobs_client;

use huslajobs\Account;
use huslajobs\HuslaModule;
use huslajobs\HuslajobsEmails;
use huslajobs\Package;
use KMSubMenuPage;

class SignupModule extends HuslaModule {
	public function __construct() {
		$this->parent_module = 'client';
		$this->module        = 'signup';
		$this->addActions();
		$this->addFilters();
	}

	public function addActions() {
		add_action( 'init', function () {

			add_rewrite_rule( 'signup', 'index.php?signup=$matches[0]', 'top' );
		} );

		add_action( 'template_include', function ( $template ) {
			if ( get_query_var( 'signup' ) == false || get_query_var( 'signup' ) == '' ) {
				return $template;
			}
			return HUSLA_JOBS_CLIENT_MODULE_DIR . '/signup/templates/index.php';
		} );

    }

	public function addFilters() {
		add_filter( 'query_vars', function ( $query_vars ) {
			$query_vars[] = 'signup';
			return $query_vars;
		} );
	}

}