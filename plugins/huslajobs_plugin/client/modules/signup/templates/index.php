<?php
    global $wpdb, $user_ID;
    //Check whether the user is already logged in
    if ($user_ID) {
    ?>
    <script type="text/javascript">
        window.location.href = "<?php echo home_url("/my-account") ?>"
    </script>
    <?php
}
    else{
    get_header();

?>

<div id="signup-content" class="content-area primary vue-component"></div>

<!--        Removed nonce since we are using recapcha -->

<!--<div id="signup-nonce">-->
<!--    --><?php //wp_nonce_field( 'huslajobs-register', 'huslajobs-register-nonce' ); ?>
<!--</div>-->


<script src="<?php echo HUSLA_JOBS_CLIENT_MODULE_URL ?>/signup/resources/index.js" type="module"></script>
<?php get_footer(); ?>
<?php } ?>