<?php

use huslajobs\HuslajobsEmails;

get_header();
$redirect_page_url = "";
$verification_complete = false;
if (isset($_REQUEST['user_verification_action']) && trim($_REQUEST['user_verification_action']) == 'email_verification') {

    $activation_key = isset($_REQUEST['activation_key']) ? sanitize_text_field($_REQUEST['activation_key']) : '';

    $messages = "";
    $verification_success = __('Thanks for Verifying.', 'huslajobs');

    $invalid_key = __('Sorry, activation key is not valid.', 'huslajobs');
    $verification_fail = __('Sorry! Verification failed.', 'huslajobs');
    $please_wait = __('Please wait.', 'user-verification');
    $redirect_after_verify = __('You will redirect after verification', 'huslajobs');
    $not_redirect = __('Click if not redirect automatically', 'huslajobs');

    $title_checking_verification = __('Checking Verification', 'huslajobs');

    $redirect_page_url = home_url('/login');

    global $wpdb;

    if (is_multisite()) {
        $table = $wpdb->base_prefix . "usermeta";
    } else {
        $table = $wpdb->prefix . "usermeta";
    }
    $meta_data = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table WHERE meta_value = %s", $activation_key));
    $user_id = $meta_data->user_id;
    if (!empty($meta_data)) {
        $user_activation_status = get_user_meta($meta_data->user_id, 'user_activation_status', true);
        if ($user_activation_status != 0) {
            $messages = $verification_fail;
        } else {
            update_user_meta($meta_data->user_id, 'user_activation_status', 1);
            $messages = $verification_success;
            $userVerificationEmails = new HuslajobsEmails();
            $emailTemplatesData = $userVerificationEmails->emailTemplatesData();

            $emailTemplatesData = $emailTemplatesData['email_confirmed'];
            $email_bcc = $emailTemplatesData['email_bcc'] ?? '';
            $email_from = $emailTemplatesData['email_from'] ?? '';
            $email_from_name = $emailTemplatesData['email_from_name'] ?? '';
            $reply_to = $emailTemplatesData['reply_to'] ?? '';
            $reply_to_name = $emailTemplatesData['reply_to_name'] ?? '';
            $email_subject = $emailTemplatesData['subject'] ?? '';
            $email_body = $emailTemplatesData['html'] ?? '';

            $email_body = do_shortcode($email_body);
            $email_body = wpautop($email_body);
            update_user_meta($user_id, 'user_activation_status', 1);

            $user_data = get_userdata($user_id);

            $user_roles = ["administrator"];

            if (!empty($exclude_user_roles))
                foreach ($exclude_user_roles as $role):

                    if (in_array($role, $user_roles)) {
                        update_user_meta($user_id, 'user_activation_status', 1);
                        return;
                    }

                endforeach;

            $site_name = get_bloginfo('name');
            $site_description = get_bloginfo('description');
            $site_url = get_bloginfo('url');

            $vars = array(
                '{site_name}' => esc_html($site_name),
                '{site_description}' => esc_html($site_description),
                '{site_url}' => esc_url_raw($site_url),
                '{first_name}' => esc_html($user_data->first_name),
                '{last_name}' => esc_html($user_data->last_name),
                '{user_display_name}' => esc_html($user_data->display_name),
                '{user_email}' => esc_html($user_data->user_email),
                '{user_name}' => esc_html($user_data->first_name),
                '{user_avatar}' => get_avatar($user_data->user_email, 60),
            );
            $email_data['email_to'] = $user_data->user_email;
            $email_data['email_bcc'] = $email_bcc;
            $email_data['email_from'] = $email_from;
            $email_data['email_from_name'] = $email_from_name;
            $email_data['reply_to'] = $reply_to;
            $email_data['reply_to_name'] = $reply_to_name;

            $email_data['subject'] = strtr($email_subject, $vars);
            $email_data['html'] = strtr($email_body, $vars);
            $email_data['attachments'] = array();


            $userVerificationEmails->sendEmail($email_data);
            $verification_complete = true;

        }
    } else {
        $messages = $invalid_key;
    }
    ?>
    <section class="content-area">
        <div class="card">
            <div class="card-body p-5">
                <div class="row">
                    <div class="col-md-8 hs-center-column">

                        <div class="check-email-verification">
                            <div class="inner">
                                <span id="close-dialog" class="close" onclick="closeDialog()"><i
                                            class="fas fa-times"></i></span>
                                <h2 class="status-title"><?php echo $title_checking_verification; ?></h2>

                                <div class="status">
                                                    <span class="status-icon"><i
                                                                class="fas fa-spin fa-spinner"></i></span>
                                    <span class="status-text"><?php echo $please_wait; ?></span>
                                </div>

                                <?php if ($verification_complete):

                                    ?>
                                    <div class="redirect">
                                        <p><?php echo $messages; ?></p>
                                        <a href="<?php echo esc_url_raw($redirect_page_url); ?>">
                                            <?php echo $not_redirect; ?>
                                        </a>
                                    </div>
                                <?php

                                endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script>

        setTimeout(function () {
            window.location.href = "<?php echo esc_url_raw($redirect_page_url); ?>"

        }, 4000);

        function closeDialog() {
            document.getElementById("check-email-verification").classList.add('hidden')
        }

    </script>
    <style type="text/css">
        .check-email-verification {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #50505094;
            z-index: 99999999;
        }

        .inner {
            width: 350px;
            background: #fff;
            top: 50%;
            position: absolute;
            left: 50%;
            transform: translate(-50%, -50%);
            padding: 15px;
            text-align: center;
            border-radius: 4px;
            box-shadow: -1px 11px 11px 0 rgb(152 152 152 / 50%);
            overflow: hidden;
        }

        .hidden {
            display: none !important;
        }

        .close {
            position: absolute;
            right: 0;
            top: 0;
            background: #dc4b1e;
            padding: 10px 15px;
            color: #fff;
            cursor: pointer;
        }

        .status-title {
            font-size: 20px;
            padding: 20px 0;
        }

        .status {
            margin: 20px 0;
        }

        .resend {
            display: none;
        }

        .status .status-icon {
            font-size: 30px;
            vertical-align: middle;
        }

        .redirect {
            margin: 50px 0 30px 0;
        }

    </style>
    <?php


}

get_footer();