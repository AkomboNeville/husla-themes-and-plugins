<?php
    namespace huslajobs_client;

    use huslajobs\HuslaModule;

    class EmailVerificationModule extends HuslaModule
    {


        public function __construct() {
            $this->parent_module = 'client';
            $this->module        = 'email-verification';
            $this->addActions();
            $this->addFilters();
        }

        public function addActions() {

            add_action( 'init', function () {
                add_rewrite_rule( 'email-verification', 'index.php?email-verification=$matches[0]', 'top' );
            } );

            add_action( 'template_include', function ( $template ) {
                if ( get_query_var( 'email-verification' ) == false || get_query_var( 'email-verification' ) == '' ) {
                    return $template;
                }
                return HUSLA_JOBS_CLIENT_MODULE_DIR . '/email-verification/templates/index.php';
            } );
        }

        public function addFilters() {
            add_filter( 'query_vars', function ( $query_vars ) {
                $query_vars[] = 'email-verification';
                return $query_vars;
            } );
        }
    }