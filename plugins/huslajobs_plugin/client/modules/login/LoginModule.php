<?php

    namespace huslajobs_client;

    use huslajobs\HuslaModule;
    use huslajobs\HuslajobsEmails;
    use KMSubMenuPage;
    use WP_Error;

    class LoginModule extends HuslaModule
    {
        public function __construct()
        {
            $this->parent_module = 'client';
            $this->module = 'login';
            $this->addActions();
            $this->addFilters();
        }

        public function addActions()
        {

            add_action('init', function () {
                add_rewrite_rule('login', 'index.php?login=$matches[0]', 'top');
            });

            add_action('template_include', function ($template) {
                if (get_query_var('login') == false || get_query_var('login') == '') {
                    return $template;
                }
                return HUSLA_JOBS_CLIENT_MODULE_DIR . '/login/templates/index.php';
            });


        }

        public function addFilters()
        {

            add_filter('query_vars', function ($query_vars) {
                $query_vars[] = 'login';

                return $query_vars;
            });
            //            authenticate user
          //  add_filter("authenticate",[$this,"authenticateUser"],9999, 3);
        }



    }