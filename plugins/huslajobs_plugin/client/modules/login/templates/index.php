<?php
    global $wpdb, $user_ID;
     //Check whether the user is already logged in
    if ($user_ID) {
        ?>
        <script type="text/javascript">
            window.location.href = "<?php echo home_url("/my-account") ?>"
        </script>
    <?php
    }
    else{
    get_header();

?>
<div id="login-content" class="content-area primary vue-component"></div>
<div id="login-nonce">
    <?php wp_nonce_field( 'huslajobs-login', 'huslajobs-login-nonce' ); ?>
</div>
<script src="<?php echo HUSLA_JOBS_CLIENT_MODULE_URL ?>/login/resources/index.js" type="module"></script>

<?php get_footer(); ?>

<?php } ?>

