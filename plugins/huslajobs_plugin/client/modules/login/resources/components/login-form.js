import {ReusableFunctions} from "../../../../../js/functions.js";
const LoginForm = {
    emits:['user-logged-in'],
    data() {
        return {
            form:{
                email:undefined,
                password:undefined,
            },
            homeUrl: home_url,
            successMessage:undefined,
            formErrors:{},
            submitting:false,
            showPassword:false,
            loginFormStrings:VueUiStrings.clientFrontEnd.loginForm.formFields
        };
    },
    mixins:[ReusableFunctions],
    watch: {
        'form.email':function(currentVal, oldVal){
            this.validateInput("email", currentVal,{required: true,isEmail:true},this.loginFormStrings.email.label);
        },
        'form.password':function(currentVal, oldVal){
            this.validateInput("password", currentVal,{required: true},this.loginFormStrings.email.label);
        },
    },
    methods: {

        userLogin(){
            /**
             * todo
             * validate form on
             */
            this.submitting = true

            //validate inputs
            this.validateInput("email", this.form.email,{required: true},"email");
            this.validateInput("password", this.form.password,{required: true},"Password");
            if (Object.keys(this.formErrors).length === 0){
                const data = new FormData();
                const that = this;
                data.append("email", this.form.email);
                data.append("password", this.form.password);
                data.append('login_nonce',document.getElementById("huslajobs-login-nonce").value);

                data.append("action", "user_login");
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: { "Content-Type": "multipart/form-data" },
                })
                    .then(function (response) {
                        if (response.data) {
                            /**
                             * todo take user to dashboard
                             */
                            toastr.success(response.data.message, 'Success');
                            if (response.data.user_data) {
                                const default_account = response.data.user_data.accounts.filter((account) => parseInt( account.default_account) == 1)[0]
                                if (default_account != undefined) {
                                    localStorage.setItem('active_account', JSON.stringify(default_account))
                                    localStorage.setItem('user', JSON.stringify(response.data.user_data))
                                }
                            }
                            that.setUserLogout()
                            that.$emit('user-logged-in',{'message':response.data.message,'redirect_to':response.data.redirect_to})
                        }

                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error?.response) {
                            toastr.error(error?.response?.data?.data);
                        } else {
                            toastr.error("An error occurred");
                        }
                        that.submitting = false;
                    });
            }else{
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )}
        },
        togglePassword() {
            const passwordInput = this.$refs.password;

            if (passwordInput.getAttribute('type') == 'password') {
                this.showPassword = true
                passwordInput.setAttribute('type', 'text')
            } else {
                this.showPassword = false
                passwordInput.setAttribute('type', 'password')
            }
        },
    },
    template: `<div>
    <form id="wp_signup_form" method="post" @submit.prevent="userLogin">
        <div class="mb-4">
            <input type="text" class="form-control" id="email" name="email" :placeholder="loginFormStrings.email.placeholder"
                   v-model="form.email">

            <span class="text-danger validation-error" v-if="formErrors.email">{{formErrors.email.toString()}}</span>
        </div>
        <div class="mb-4">

                <div class="input-group icon-right position-relative">
                    <i v-if="showPassword" class="fas fa-eye icon cursor-pointer" @click="togglePassword"></i>
                    <i v-else class="fas fa-eye-slash icon cursor-pointer" @click="togglePassword"></i>
                    <input type="password" class="form-control" id="password" ref="password" :placeholder="loginFormStrings.password.placeholder"
                           name="password" v-model="form.password">
                </div>
                <span class="text-danger validation-error" v-if="formErrors.password">{{formErrors.password.toString()}}</span>
        </div>
        <div class="mb-5">
            <button type="submit" class="hs-btn hs-btn-signup hs-btn-primary w-100 cursor-pointer"
                    :disabled="submitting || Object.keys(formErrors).length > 0">{{loginFormStrings.button.toString()}}<i v-if="submitting"
                                                                                        class="fa fa-spinner fa-pulse"></i>
            </button>
        </div>
    </form>
</div>

`,
};

export {LoginForm};
