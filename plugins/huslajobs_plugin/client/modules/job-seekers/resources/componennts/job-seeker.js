import {LoadingError} from "../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";

const JobSeeker = {
    data() {
        return {
            form: {
                name: undefined,
                email: undefined,
                message: undefined
            },
            homeUrl: THEME_URL,
            userDataHeaders: [{key: 'about', value: 'About'}],
            defaultHeader: 'about',
            loading: false,
            id: job_seeker_id,
            account: undefined,
            profiles: undefined,
            categories: undefined,
            error: false,
            submitting: false,
            formErrors: {},
            imgUrl: THEME_URL,
            jobSeekerStrings: VueUiStrings.clientFrontEnd.jobSeeker
        }
    },
    components: {LoadingError, LoadingBar, VLoader},
    mixins: [ReusableFunctions],
    methods: {
        updateDefaultHeader(currentHeader) {
            this.defaultHeader = currentHeader;
        },
        getAccount() {
            this.startLoader()
            const data = new FormData();
            data.append("action", "get_account");
            data.append("account_id", this.id);
            data.append("account_type", '_seeker');

            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    let profileCategories = [];
                    that.account = response.data.account;
                    that.profiles = response.data.profiles;
                    if (that.profiles.length) {
                        for (const profile of that.profiles) {
                            profileCategories = [...profileCategories, ...profile.categories];
                        }
                        that.userDataHeaders.push({key: 'resume', value: 'Resume', total: that.profiles.length})
                        // profileCategories = profileCategories.filter((category, index, self) => self.indexOf(category) === index)

                    }
                    that.categories = [...new Set(profileCategories)]

                    that.error = false;

                    setTimeout(() => {
                        that.loading = false;
                        that.closeLoader();
                    }, 100)
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.jobSeekerStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.closeLoader();
                    that.error = true;
                });
        },
        contactSeeker() {
            this.submitting = true;
            this.validateInput("name", this.form.name, {required: true}, this.jobSeekerStrings.contactForm.fields.name.label);
            this.validateInput("message", this.form.message, {required: true}, this.jobSeekerStrings.contactForm.fields.message.label);
            this.validateInput("email", this.form.email, {required: true}, this.jobSeekerStrings.contactForm.fields.email.label);
            if (Object.keys(this.formErrors).length === 0) {
                const data = new FormData();
                data.append("action", "contact_job_seeker");
                data.append("sender_email", this.form.email);
                data.append("sender_name", this.form.name);
                data.append("sender_message", this.form.message);
                data.append("job_seeker_email", this.account.email);

                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data.message)
                            that.form = {
                                name: undefined,
                                email: undefined,
                                message: undefined
                            }

                            window.scrollTo({
                                top: 0,
                                left: 0,
                                behavior: 'smooth'
                            });
                        }


                        that.submitting = false
                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.jobSeekerStrings.errors.errorOccurred);
                        }
                        that.submitting = false
                    });

            } else {
                this.submitting = false
            }
        },
    },
    watch: {
        'form.name': function (currentVal, oldVal) {
            if (currentVal !== undefined) {
                this.validateInput("name", currentVal, {required: true}, this.jobSeekerStrings.contactForm.fields.name.label);
            }

        },
        'form.message': function (currentVal, oldVal) {
            if (currentVal !== undefined) {
                this.validateInput("message", currentVal, {required: true}, this.jobSeekerStrings.contactForm.fields.message.label);
            }
        },
        'form.email': function (currentVal, oldVal) {
            if (currentVal !== undefined) {
                this.validateInput("email", currentVal, {required: true}, this.jobSeekerStrings.contactForm.fields.email.label);
            }
        }
    },
    mounted() {

        this.getAccount();


    },
    template: `<VLoader :active="loading" :translucent="true">
    <div id="job-seeker">
        <div v-if="error">
            <loading-error @update="getAccount"></loading-error>
        </div>
        <div v-else-if="account">
            <!--            job seeker details -->
            <section class="hs-section">
                <div class="row">
                    <div class="col-md-8 mb-4">
                        <div class="d-flex flex-column hs-border-1">

                            <div class="userdata-menu d-flex">
                                <div v-for="(header,index) in userDataHeaders" :key="index"
                                     @click="updateDefaultHeader(header.key)"
                                     class="userdata-menu-item px-5 py-4 cursor-pointer"
                                     :class="{'active':header.key === defaultHeader}">
                                    <span class="mr-1">{{header.value}}</span>
                                    <span class="total"
                                          v-if="header.total===0 || header.total>0">{{header.total}}</span>
                                </div>
                            </div>
                            <div class="p-5">
                                <!--    resume            -->
                                <div :class="{'d-block':defaultHeader==='resume' ,'d-none':defaultHeader!=='resume'}"
                                     class="userdata-body-item">
                                    <div v-for="(profile,index) in profiles" class="mb-4 hs-border-1 p-4 search-result">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <a :href="profile.cv" target="_blank">
                                                    <img :src="imgUrl+'/document-type/pdf-logo.png'" alt="Cv">
                                                </a>
                                                <small>{{jobSeekerStrings.resume.tip}}</small>

                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="hs-subtitle"> {{decodeHtml(profile.name)}}</h5>
                                                <p>{{decodeHtml(profile.description)}}</p>
                                            </div>

                                        </div>


                                    </div>
                                </div>

                                <!--                  about                  -->
                                <div :class=" {'d-block':defaultHeader==='about' ,'d-none':defaultHeader!=='about'} "
                                     class="userdata-body-item">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="hs-profile-img">
                                                <div class="hs-profile-img-inner">
                                                    <img :src="setImage(account.profile_image,false)"
                                                         alt=""></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <h2 class="section_title">{{decodeHtml(account.name)}}</h2>
                                            <p><i class="fa fa-envelope mb-0"></i><span
                                                    class="ml-2">{{account.email}}</span>
                                            </p>
                                            <p><i class="fa fa-phone mb-0"></i><span
                                                    class="ml-2">{{account.phone_number}}</span>
                                            </p>
                                            <p><i class="fa fa-map-marker mb-0"></i><span class="ml-2">{{decodeHtml(account.city)}} {{decodeHtml(account.state)}} ,{{selectedCountry(account.country)}}</span>
                                            </p>
                                            <div v-if="categories.length">

                                                <span class="mr-2 d-inline-block hs-title">{{jobSeekerStrings.jobCategories}}:</span>
                                                <span v-for="(category,index) in categories" :key="index"
                                                      class="hs-bg-gray-color hs-round hs-btn-sm mr-3 d-inline-block text-capitalize hs-subtitle">{{decodeHtml(category.name)}}</span>
                                            </div>

                                        </div>

                                        <div class="col-md-12">
                                            <div class="mt-5">


                                                <p>{{decodeHtml(account.bio)}}</p>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mb-4">

                        <div class="d-flex flex-column hs-border-1">
                            <div class="userdata-menu userdata-menu-item cursor-pointer active text-center py-3">
                                <h2 class="section-title mb-0">{{jobSeekerStrings.contactForm.heading}}</h2>
                            </div>
                            <div class="p-3">

                                <form id="wp_signup_form" @submit.prevent="contactSeeker">
                                    <div class="mb-4">
                                        <input type="text" class="form-control" id="username" name="name"
                                               :placeholder="jobSeekerStrings.contactForm.fields.name.placeholder"
                                               v-model="form.name"
                                               >
                                        <span class="text-danger"
                                              v-if="formErrors.name">{{formErrors.name}}</span>
                                    </div>
                                    <div class="mb-4">
                                        <input type="email" class="form-control" id="password" name="email"
                                               :placeholder="jobSeekerStrings.contactForm.fields.email.placeholder"
                                               v-model="form.email">
                                        <span class="text-danger d-block"
                                              v-if="formErrors.email">{{formErrors.email}}</span>
                                    </div>
                                    <div class="mb-4">
                                        <textarea name="message" v-model="form.message" rows="4"
                                                  :placeholder="jobSeekerStrings.contactForm.fields.message.placeholder"></textarea>
                                        <span class="text-danger"
                                              v-if="formErrors.message">{{formErrors.message}}</span>

                                    </div>
                                    <div class="mb-5">
                                        <button type="submit" class="hs-btn hs-btn-signup hs-btn-primary"
                                                :class="{'not-allowed':submitting || Object.keys(formErrors).length > 0}"
                                                :disabled="submitting || Object.keys(formErrors).length > 0"
                                        >
                                            <i
                                                    v-if="submitting" class="fas fa-spinner fa-pulse"></i>
                                            {{jobSeekerStrings.contactForm.fields.button}}
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>

                    </div>


                </div>
            </section>
        </div>
    </div>
</VLoader>

    `,
}

export {JobSeeker}