
import {ReusableFunctions} from "../../../../../js/functions.js";

const JobSeekerCard = {
    props: {
        job_seeker_data: {
            type: [Array],
            required: true,
            default: () => ([])
        },
        job_category:{
            type: String,
            required: false,
            default : ""
        }


    },
    mixins:[ReusableFunctions],
    data() {
        return {
            imgUrl: THEME_URL,
            homeUrl: home_url
        }
    },
    template: `

 <a :href="homeUrl+'/job-seekers/'+job_seeker_data[0].id" class="hs-seeker-card">
                            
                                <div class="hs-seeker-card-header">
                                    <div class="hs-seeker-card-image"
                                         :style="setImage(job_seeker_data[0].profile_image)"></div>
                                    <div class="hs-overlay-box"></div>
                                    <span v-if="job_category" class="hs-seeker-card-categories-label"><a href="javascript:;">{{job_category}}</a>
                                                </span>
<!--                                    <a href="javascript:;" class="add-favorite sf-featured-item" data-proid="405"-->
<!--                                       data-userid="1"><i class="fa fa-heart-o"></i></a>-->
                                    <div class="hs-seeker-card-info">
                                        <div class="hs-seeker-name">{{decodeHtml(job_seeker_data[0].name)}}</div>
                                        <div class="hs-seeker-location"><i class="fa fa-map-marker"></i> <span class="ml-2">{{decodeHtml(job_seeker_data[0].city)}} {{decodeHtml(job_seeker_data[0].state)}}, {{selectedCountry(job_seeker_data[0].country)}}</span></div>

                                    </div>
                                    <a :href="homeUrl+'/job-seekers/'+job_seeker_data[0].id"
                                       class="sf-profile-link"></a>
                                </div>
                                <div class="hs-seeker-card-body">
                                    <div class="hs-seeker-about" role="group"
                                         aria-label="Basic example">
                                        <p>{{decodeHtml(reduceString(job_seeker_data[0].bio))}}</p>
                                    </div>
    
                                </div>
                          

</a>
    `
}

export {JobSeekerCard}