<?php
global $wpdb, $user_ID;
get_header();

?>

<div id="jobs-seekers" class="content-area primary vue-component"></div>

<script src="<?php echo HUSLA_JOBS_CLIENT_MODULE_URL ?>/job-seekers/resources/index.js" type="module"></script>
<?php get_footer(); ?>
