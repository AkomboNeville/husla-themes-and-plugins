<?php

namespace huslajobs_client;

use huslajobs\HuslaModule;

/**
 * Add files/classes included on the client module
 */

add_filter( 'husla_jobs_includes_filter', function ( $includes ) {
	$files = HuslaModule::getModules( HUSLA_JOBS_CLIENT_MODULE_DIR );
	array_push( $files,
		HUSLA_JOBS_CLIENT_DIR . '/HuslaClient.php' );
	$includes = array_merge( $includes, $files );

	return $includes;
} );