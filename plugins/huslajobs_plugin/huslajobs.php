<?php
/*
Plugin Name: HuslaJobs Plugin
Plugin URI: http://zingersystems.com/
Description: This is a plugin for all booking related functionality
Version: 1.0
Author: zingersystems
Author URI: http://zingersystems.com
* Domain Path: /languages
*/

namespace huslajobs;

require 'constants.php';
require HUSLA_JOBS_INC_DIR . '/HuslaJob.php';
require HUSLA_JOBS_INC_DIR . '/HuslaModel.php';
require HUSLA_JOBS_INC_DIR . '/HuslaMigration.php';
require HUSLA_JOBS_INC_DIR . '/HuslaSeeder.php';
require HUSLA_JOBS_INC_DIR . '/HuslaValidator.php';
require HUSLA_JOBS_INC_DIR . '/HuslajobsEmails.php';
require HUSLA_JOBS_INC_DIR . '/WooPayment.php';
require HUSLA_JOBS_INC_DIR . '/Crawler.php';
require HUSLA_JOBS_INC_DIR . '/HuslaModule.php'; //todo: fix error with this module not working in foreach loop
require HUSLA_JOBS_LIB_DIR . '/faker/autoload.php';

defined('ABSPATH') or die('Giving To Cesar What Belongs To Caesar');


/**
 * Scan directories for files to include
 */
foreach (scandir(__DIR__) as $dir) {
    if (strpos($dir, '.') === false && is_dir(__DIR__ . '/' . $dir) && is_file(__DIR__ . '/' . $dir . '/includes.php')) {
        require __DIR__ . '/' . $dir . '/includes.php';
    }
}

/**
 * @since v1.0
 * Shows error messages on the dashboard
 */
function huslajobsErrorNotice($message = '')
{
    if (trim($message) != '') :
        ?>
        <div class="error notice is-dismissible">
            <p><b>Husla Jobs: </b><?php echo $message ?></p>
        </div>
    <?php
    endif;
}

/**
 * @since v1.0
 * loads classes / files
 */
function huslajobsLoader()
{
    $error = false;

    $includes = apply_filters('husla_jobs_includes_filter', []);

    foreach ($includes as $file) {
        if (!$filepath = file_exists($file)) {
            huslajobsErrorNotice(sprintf(__('Error locating <b>%s </b> for inclusion', 'huslajobs'), $file));
            $error = true;
        } else {
            include_once $file;
        }
    }

    return $error;
}

/**
 * @since v1.0
 * Starts the plugin
 */
function huslajobsStart()
{
//	HuslaMigration::refreshAndSeed();
//    HuslaMigration::refresh();
    $huslajobs = new HuslaJob();
    $huslajobs->run();
}


if (!huslajobsLoader()) {
    huslajobsStart();
}


// remove options upon deactivation

register_deactivation_hook(__FILE__, 'huslajobs\huslajobsDeactivation');

/**
 * @since v1.0
 */
function huslajobsDeactivation()
{
    // set options to remove here
    HuslaMigration::dropAll();
}

register_uninstall_hook(__FILE__, 'huslajobs\huslajobsUninstall');

/**
 * @since v1.0
 */
function huslajobsUninstall()
{
    // set options to remove here
    HuslaMigration::dropAll();
}

register_activation_hook(__FILE__, 'huslajobs\huslajobsActivation');
/**
 * @since v1.0
 */
function huslajobsActivation()
{
    // setup database tables
    global $wp_rewrite;
    $custom_fields = [];
    HuslaMigration::runMigrations();
    HuslaMigration::alterUserTable();
    HuslaMigration::seedDefaultTables();
    huslajobsCleanUserMeta();
    HuslaMigration::createCrawler();
    \huslajobs\createProduct();
    $wp_rewrite->set_permalink_structure('/%postname%/');
    //Set the option
    update_option("rewrite_rules", true);
    $wp_rewrite->flush_rules();
}

function huslajobsCleanUserMeta()
{
    $meta_query = array();
    $meta_query[] = array(
        'relation' => 'AND',
        array(
            'key' => 'user_activation_status',
            'value' => '1',
            'compare' => '=',
        ),
        array(
            'key' => 'user_activation_key',
            'compare' => 'EXISTS',
        )
    );
    $users = get_users(
        array(
            //'role'    => 'administrator',
            'orderby' => 'ID',
            'order' => 'ASC',
            'number' => 20,
            'paged' => 1,
            'meta_query' => $meta_query,

        )
    );

    foreach ($users as $user) {
        $user_id = $user->ID;
        $user_email = $user->user_email;

        delete_user_meta($user_id, 'user_activation_key', $meta_value = '');


    }
}


function createProduct(): void
{
    $product_id = get_option('husla_jobs_woo_product_id', false);
    $create_product = false;

//    if (class_exists('woocommerce')) {
        if ($product_id) {
            $product = wc_get_product($product_id);
            if (!$product || $product->get_status() != 'publish') {
                $create_product = true;
            }
        } else {
            $create_product = true;
        }
        if ($create_product) {
            $product = new \WC_Product_Simple;
            $product->set_name("Husla Subscription"); // Name (title).
            $product->set_description("Product used for membership subscriptions");
            $product->set_status('publish');
            $product->set_catalog_visibility('hidden');
            $product->set_regular_price(0);
            $product_id = $product->save();
            add_option('husla_jobs_woo_product_id', $product_id);
        }
}

/**
 * @param $name
 * @return array
 */
function split_name($name): array
{
    $name_array = explode(' ', $name);
    $first_name = $name_array[0];
    $last_name = '';
    if (sizeof($name_array) > 0) {
        for ($i = 0; $i < sizeof($name_array); $i++) {
            if ($i > 0) {
                $last_name = $last_name . $name_array[$i] . ' ';
            }
        }
    }
    return ['first_name' => $first_name, 'last_name' => $last_name];
}
/**
 * @param $title
 * @param int $position
 * @return string
 */
function createSlug($title, int $position = 0): string
{
    $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $title)));
    if ($position){
        $slug =$slug.'-'.$position;
    }
    return $slug;
}

function loadTextDomain()
{
    load_plugin_textdomain('huslajobs', false, basename(dirname(__FILE__)) . ' / languages');
//    create product

}
add_action('plugins_loaded', 'huslajobs\loadTextDomain', 0);

