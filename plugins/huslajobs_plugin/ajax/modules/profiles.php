<?php

namespace huslajobs;
// Actions
add_action('wp_ajax_add_profile', 'huslajobs\addProfile');
add_action('wp_ajax_get_profile', 'huslajobs\getProfile');
add_action('wp_ajax_update_profile', 'huslajobs\updateProfile');
add_action('wp_ajax_delete_profile', 'huslajobs\deleteProfile');

//
add_action('wp_ajax_get_user_profiles', 'huslajobs\getUserProfiles');
add_action('wp_ajax_nopriv_get_user_profiles', 'huslajobs\getUserProfiles');

// Methods

function deleteProfile()
{
    $validator = HuslaValidator::validator(['profile_id' => 'required'], $_POST);
    if ($validator->validate()) {
        $profile_id = $_POST['profile_id'];
        $profile = Profile::find($profile_id);
        $profile->delete();
        echo json_encode(__('Profile deleted','huslajobs'));
    }

    wp_die();
}

function assignProfileCategories(HuslaModel $profile, array $categories)
{
    foreach ($categories as $category) {
        $profile_category = new ProfileCategory();
        $profile_category->profile_id = $profile->id;
        $profile_category->category_id = $category;
        $profile_category->save();
    }
}

function getProfile()
{
    $validator = HuslaValidator::validator(['profile_id' => "required"], $_POST);
    if ($validator->validate()) {
        $id = $_POST['profile_id'];
        $profile = Profile::find($id);
        $profile->categories = $profile->categories();

        echo json_encode($profile);
    }
    wp_die();
}

function updateProfile()
{
    $data = array_merge($_POST, $_FILES);
    $validator = HuslaValidator::validator([
        'profile_name' => 'required',
        'profile_description' => 'required',
        'profile_categories' => 'required',
        'profile_id' => 'required',

    ], $data);


    if ($validator->validate()) {
        $name = stripslashes(sanitize_text_field($_POST['profile_name']));
        $id = $_POST['profile_id'];
        $description = stripslashes(sanitize_text_field($_POST['profile_description']));
        $categories = explode(',', $_POST['profile_categories']);

        $profile = Profile::find($id);
        $profile->description = $description;
        $profile->name = $name;

        if ($_FILES['profile_cv']) {
            $cv = $_FILES['profile_cv'];
            $cv = saveFile($cv)['url'];
            $profile->cv = $cv;
        }else{
            $profile->cv = $_POST['profile_cv'];
        }

        $profile->save();

        foreach ($profile->categories() as $category) {

            $category->delete();

        }

        assignProfileCategories($profile, $categories);


        echo json_encode(__('Profile updated','huslajobs'));
    }
    wp_die();
}

function addProfile()
{
    $data = array_merge($_POST, $_FILES);
    $validator = HuslaValidator::validator([
        'profile_name' => 'required',
        'profile_description' => 'required',
        'profile_categories' => 'required',
        'account_id' => 'required',
        'profile_cv' => 'required|pdf',
    ], $data);

    if ($validator->validate()) {
        $cv = $_FILES['profile_cv'];
        $name = stripslashes(sanitize_text_field($_POST['profile_name']));
        $description = stripslashes(sanitize_text_field($_POST['profile_description']));
        $account_id = $_POST['account_id'];
        $categories = explode(',', $_POST['profile_categories']);
        $cv = saveFile($cv)['url'];

        $profile = new Profile();
        $profile->account_id = $account_id;
        $profile->description = $description;
        $profile->name = $name;
        $profile->cv = $cv;
        $profile->save();

        assignProfileCategories($profile, $categories);


        echo json_encode(__('Profile created','huslajobs'));
    }
    wp_die();
}

function getProfileCategories($profile)
{
    $categories = $profile->getCategories();
    $profile->categories = $categories;
    return $profile;
}
function getUserProfiles(){
    global $user_ID;
    $profiles = Profile::paginate(-1)->orderBy('id', 'desc');
    $profiles->leftJoin('accounts')
        ->on('account_id','id')
        ->whereJoin('wp_user_id','=',intval($user_ID),Account::tableName());
    $select_fields = [
        Profile::tableName() . '.*',
    ];
    $profiles = $profiles->get($select_fields);
    echo json_encode($profiles);
    wp_die();
}