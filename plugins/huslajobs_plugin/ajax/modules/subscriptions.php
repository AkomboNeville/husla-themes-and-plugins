<?php

namespace huslajobs;

// Actions
add_action( 'wp_ajax_get_subscriptions', 'huslajobs\getSubscriptions' );

// Methods
function getSubscriptions() {
	$page          = intval( $_POST['page'] );
	$per_page      = intval( $_POST['perPage'] );
	$sort_by       = $_POST['sortBy'] ?? 'id';
	$order         = $_POST['order'] ?? 'desc';
	$results       = Subscription::paginate( $per_page, $page )->orderBy( $sort_by, $order )->get();
	$subscriptions = $results['data'];
	for ( $i = 0; $i < sizeof( $subscriptions ); $i ++ ) {
		$user                         = $subscriptions[ $i ]->user();
		$package                      = $subscriptions[ $i ]->package();
		$subscriptions[ $i ]->user    = $user;
		$subscriptions[ $i ]->package = $package;
	}
	$results['data'] = $subscriptions;
	echo json_encode( $results );
	wp_die();
}