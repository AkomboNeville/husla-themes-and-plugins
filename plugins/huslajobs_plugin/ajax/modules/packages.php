<?php

namespace huslajobs;

// Actions
add_action( 'wp_ajax_get_packages', 'huslajobs\getPackages' );
add_action( 'wp_ajax_save_package', 'huslajobs\savePackage' );
add_action( 'wp_ajax_get_package', 'huslajobs\getPackage' );
add_action( 'wp_ajax_update_package', 'huslajobs\updatePackage' );
add_action( 'wp_ajax_delete_package', 'huslajobs\deletePackage' );

add_action('wp_ajax_get_admin_packages','huslajobs\getAdminPackages');
add_action('wp_ajax_nopriv_get_admin_packages','huslajobs\getAdminPackages');
// Methods
function getPackages() {
	$page        = intval( $_POST['page'] );
	$per_page    = intval( $_POST['perPage'] );
	$sort_by     = $_POST['sortBy'] ?? 'id';
	$order       = $_POST['order'] ?? 'desc';
	$searchField = $_POST['searchField'] ?? '';
	$searchText  = $_POST['searchText'] ?? '';
	$results     = Package::paginate( $per_page, $page )->orderBy( $sort_by, $order );
	if ( trim( $searchField ) != '' && trim( $searchText ) != '' ) {
		$results->where( $searchField, 'like', "'%" . $searchText . "%'" );
	}
	echo json_encode( $results->get() );

	wp_die();
}

function savePackageLimits( string $limits, HuslaModel $package ) {
	$limits = explode( ",", $limits );
	$limits = array_filter( $limits, function ( $limit ) {
		return $limit != '';
	} );
	foreach ( $limits as $limit ) {
		$data                       = explode( ":", $limit );
		$package_limit              = new PackageLimit();
		$package_limit->package_id  = $package->id;
		$package_limit->job_type_id = $data[0];
		$package_limit->limit       = $data[1];
		$package_limit->save();
	}
}

function savePackage() {
	global $user_ID;
	if ( HuslaValidator::validate(
		[
			'name'        => 'required',
			'price'       => 'required|numeric',
			'duration'    => 'required|numeric',
			'profiles'    => 'required|numeric',
            'jobs'    => 'required|numeric',
            'job_applications'    => 'required|numeric',
//			'currency_id' => 'required',
			'benefits'    => 'required',
		], $_POST ) ) {
		$name           = $_POST['name'];
		$package_exists = Package::where( 'name', 'like', "'" . $name . "'" )->get();
		if ( sizeof( $package_exists ) > 0 ) {
			wp_send_json_error( __('A package with this name already exists','huslajobs'), 400 );
		} else {
			$price                = doubleval( $_POST['price'] );
			$description          = stripslashes(sanitize_text_field( $_POST['description']));
			$duration             = intval( $_POST['duration'] );
			$limits               = $_POST['limits'];
//			$currency             = $_POST['currency_id'];
			$package              = new Package();
			$package->duration    = $duration;
			$package->description = $description;
			$package->price       = $price;
            $package->jobs = intval( $_POST['jobs'] );
            $package->job_applications = intval( $_POST['job_applications'] );
            $package->price       = $price;
			$package->name        = $name;
            $package->profiles    = intval( $_POST['profiles'] );
//			$package->currency_id = $currency;
			$package->benefits    = esc_attr( $_POST['benefits']);
			if ( $package->save() ) {
//				savePackageLimits( $limits, $package );
				echo 'saved';
			} else {
				wp_send_json_error( 'An error occurred', 400 );
			}

		}
	}
	wp_die();
}

function getPackage() {
	$id      = intval( $_POST['package_id'] );
	$package = Package::find( $id );
	if ( $package ) {
		echo json_encode( $package );
	} else {
		wp_send_json_error( __('Package not found','huslajobs'), 400 );
	}
	wp_die();
}


function updatePackage() {
	if ( HuslaValidator::validate( [
		'package_id'  => 'required|numeric',
		'name'        => 'required',
		'price'       => 'required|numeric',
		'duration'    => 'required|numeric',
		'profiles'    => 'required|numeric',
        'jobs'    => 'required|numeric',
        'job_applications'    => 'required|numeric',
//		'currency_id' => 'required'
	], $_POST ) ) {
		$id                   = intval( $_POST['package_id'] );
		$name                 = stripslashes(sanitize_text_field($_POST['name']));
		$price                = doubleval( $_POST['price'] );
		$description          = stripslashes(sanitize_text_field($_POST['description']));
		$duration             = intval( $_POST['duration'] );
//		$limits               = $_POST['limits'];
		$package              = Package::find( $id );
		$package->duration    = $duration;
		$package->description = $description;
		$package->profiles    = intval( $_POST['profiles'] );
//		$package->currency_id = $_POST['currency_id'];
        $package->jobs = intval( $_POST['jobs'] );
        $package->job_applications = intval( $_POST['job_applications'] );
		$package->price       = $price;
		$package->name        = $name;

		if ( $package->save() ) {
//			$limitsToDelete = $package->limits();
//			foreach ( $limitsToDelete as $limit ) {
//				$limit->delete();
//			}
//			savePackageLimits( $limits, $package );
			echo "updated";
		} else {
			wp_send_json_error( 'An error occurred', 400 );
		}
	}
	wp_die();
}

function deletePackage() {
	$id      = intval( $_POST['package_id'] );
	$package = Package::find( $id );
	if ( sizeof( $package->subscriptions() ) > 0 ) {
		wp_send_json_error( 'Package can not be deleted since it has current subscriptions', 400 );
	} else {
		echo json_encode( $package->delete() );
	}
	wp_die();
}

function getAdminPackages()
{

    $page = intval($_POST['page']);
    $per_page = intval($_POST['perPage']);
    $sort_by = $_POST['sortBy'] ?? 'id';
    $order = $_POST['order'] ?? 'desc';
    $results = Package::paginate($per_page, $page)->orderBy($sort_by, $order)->where('wp_user_id', '=', 0);
//    $results->leftJoin('currencies')
//        ->on('currency_id','id')

//    $select_fields = [
//        Package::tableName().'.'.'*',
//        Currency::tableName().'.'.'code AS currency_code',
//        Currency::tableName().'.name AS currency_name',
//    ];
    echo json_encode($results->get());
    wp_die();
}