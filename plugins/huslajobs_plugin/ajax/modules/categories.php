<?php

namespace huslajobs;

// Actions
add_action('wp_ajax_get_categories', 'huslajobs\getCategories');
add_action('wp_ajax_nopriv_get_categories', 'huslajobs\getCategories');
add_action('wp_ajax_get_category', 'huslajobs\getCategory');
add_action('wp_ajax_save_category', 'huslajobs\saveCategory');
add_action('wp_ajax_update_category', 'huslajobs\saveCategory');
add_action('wp_ajax_delete_category', 'huslajobs\deleteCategory');

// categories
function getCategories()
{
    $page = intval($_POST['page']);
    $per_page = intval($_POST['perPage']);
    $sort_by = $_POST['sortBy'] ?? 'id';
    $order = $_POST['order'] ?? 'desc';
    $searchText = $_POST['searchText'];
    $results = Category::paginate($per_page, $page)->orderBy($sort_by, $order);
    if (trim($searchText) != '') {
        $results->where('name', 'like', "'%" . $searchText . "%'");
        $results->orWhere('description', 'like', "'%" . $searchText . "%'");
    }
    echo json_encode($results->get());
    wp_die();

}

function deleteCategory()
{
    $id = intval($_POST['category_id']);
    $category = Category::find($id);
    if (sizeof($category->jobs()) > 0) {
        wp_send_json_error(__('Category cannot be deleted since it has been used as a job category', 'huslajobs'), 400);
    } elseif (sizeof($category->profiles()) > 0) {
        wp_send_json_error(__('Category can not be deleted since it has been assigned to a profile', 'huslajobs'), 400);
    } else {
        echo json_encode($category->delete());
    }
    wp_die();
}

function getCategory()
{
    $id = intval($_POST['category_id']);
    $category = Category::find($id);
    if ($category) {
        echo json_encode($category);
    } else {
        wp_send_json_error(__('Category not found', 'huslajobs'), 400);
    }
    wp_die();
}

function saveCategory()
{

    $validator = HuslaValidator::validator([
        'description' => 'required',
        'name' => 'required',
    ], $_POST);
    if ($validator->validate()) {
        $name = $_POST['name'];
        if (!$_POST['category_id'] && sizeof(Category::name($name))) {
            wp_send_json_error(__('A category with this name already exists', 'huslajobs'), 400);
        } else {
            $id = $_POST['category_id'];
            $category = $id ? Category::find($id) : new Category();
            $category->description = stripslashes(sanitize_text_field($_POST['description']));
            $category->name = stripslashes(sanitize_text_field($_POST['name']));
            $result = $category->save();
            if ($result) {
                echo json_encode($result);
            } else {
                wp_send_json_error(__('An error occurred', 'huslajobs'), 400);
            }
        }

    }
    wp_die();
}
