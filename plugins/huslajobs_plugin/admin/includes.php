<?php

namespace huslajobs_admin;

use huslajobs\HuslaModule;

/**
 * Add files/classes included on the admin module
 */


add_filter( 'husla_jobs_includes_filter', function ( $includes ) {
	$files = HuslaModule::getModules( HUSLA_JOBS_ADMIN_MODULE_DIR );
	array_push( $files,
		HUSLA_JOBS_ADMIN_DIR . '/HuslaAdmin.php' );
	$includes = array_merge( $includes, $files );

	return $includes;
} );
