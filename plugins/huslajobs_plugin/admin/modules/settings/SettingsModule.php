<?php

namespace huslajobs_admin;

use huslajobs\HuslaModule;
use huslajobs\Package;
use KMSetting;
use KMSubMenuPage;

class SettingsModule extends HuslaModule {
	public function __construct() {
		$this->parent_module = 'admin';
		$this->module        = 'settings';
		$this->addActions();
		$this->addSettings();
		add_filter( 'husla_jobs_admin_sub_menu_pages_filter', [ $this, 'addSettingsSubMenuPage' ] );
	}

	/**
	 * @since v1.0
	 * Adds dashboard submenu page
	 */
	function addSettingsSubMenuPage( $sub_menu_pages ) {
		$dashboard_page = new KMSubMenuPage(
			array(
				'page_title' => 'Settings',
				'menu_title' => 'Settings',
				'capability' => 'manage_options',
				'menu_slug'  => 'husla-jobs-settings',
				'position'   => 1,
				'function'   => [ $this, 'settingsPageContent' ]
			) );
		array_push( $sub_menu_pages, $dashboard_page );

		return $sub_menu_pages;
	}

	/**
	 * @since v1.0
	 * Displays content on dashboard sub menu page
	 */
	function settingsPageContent() {
		$this->renderContent( 'index' );
	}

	public function addActions(): void {
		// add actions here
	}

	public function addSettings() {

		$settings = new KMSetting( 'husla-jobs-settings' );
		$settings->add_section( 'husla-jobs-settings' );
        $settings->add_field(
            array(
                'type'  => 'text',
                'id'    => 'husla_jobs_woo_product_id',
                'label' => 'Product Id',
                'tip'   => "<span class='text-primary'>The product id to be used for membership subscriptions</span>"
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_profile_upload_size',
                'label' => 'Profile upload size(MB)',
                'tip'   => '',

            )
        );

        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_maximum_login',
                'label' => 'Maximum login',
                'tip'   => 'Number of times a user can login if they have not very their email'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_quarter_discount_rate',
                'label' => '3 months discount rate(%)',
                'tip'   => 'Discount rate for users paying upfront for membership plans'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_biannual_discount_rate',
                'label' => '6 months discount rate(%)',
                'tip'   => 'Discount rate for users paying upfront for membership plans'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_yearly_discount_rate',
                'label' => '1 year discount rate(%)',
                'tip'   => 'Discount rate for users paying upfront for membership plans'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_price_per_job',
                'label' => 'Price per job',
                'tip'   => 'The cost per job for guest members'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_limit',
                'label' => 'Job limit',
                'default'=>20,
                'tip'   => 'The number of jobs non logged in users can view'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_price_per_internship',
                'label' => 'Price per paid internship',
                'tip'   => 'The cost per paid internship for guest members'
            )
        );
		$settings->add_field(
			array(
				'type'    => 'select',
				'id'      => 'husla_jobs_account_type',
				'label'   => 'Default account type: ',
				'options' => array(
                    'Individual job seeker' =>'individual_job_seeker',
                    'Individual recruiter' =>'individual_recruiter',

				),
				 'default_option' => 'individual_job_seeker'
			)
		);

        $settings->add_field(
            array(
                'type'    => 'select',
                'id'      => 'husla_jobs_plugin_mode',
                'label'   => 'Plugin mode: ',
                'options' => array(
                    'Development' =>'development',
                    'Testing' =>'testing',
                    'Production' =>'production',
                ),
                'default_option' => 'development'
            )
        );

        $settings->add_field(
            array(
                'type'    => 'select',
                'id'      => 'husla_jobs_per_page',
                'label'   => 'Items per page: ',
                'options' => array(
                    '15' =>15,
                    '25' =>25,
                    '50' =>50,
                ),
                'default_option' => 15
            )
        );

		$settings->save();
	}
}