<?php

/**
 * packages template
 */
?>
<div id="km-content">
    <div id="loading-screen">
        <img src="<?php echo HUSLA_JOBS_IMAGE_URL ?>/logo.png" style="animation-name: km-img-loading;
          animation-duration: 2s;
          animation-iteration-count: infinite;"/>
    </div>

</div>

<script>
    const THEME_URL = "<?php echo HUSLA_JOBS_IMAGE_URL?>";
    const home_url = "<?php echo home_url()?>";
</script>
<script src="<?php echo HUSLA_JOBS_ADMIN_MODULE_URL ?>/dashboard/resources/index.js" type="module"></script>

<style>
    .card {
        max-width: 100% !important;
    }

    #loading-screen {
        position: absolute;
        top: 30%;
        left: 45%;
        margin: auto;
    }

    @keyframes km-img-loading {
        0% {
            width: 120px;
            opacity: 0.5;
        }

        50% {
            width: 150px;
            opacity: 1;
        }

        100% {
            width: 120px;
            opacity: 0.5;
        }
    }
</style>