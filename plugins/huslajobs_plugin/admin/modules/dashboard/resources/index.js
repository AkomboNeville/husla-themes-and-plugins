import {Dashboard} from "./components/Dashboard.js";
import {Index as JobProvider} from "./components/job-providers/index.js";
import {Jobs} from "./components/jobs/index.js";
import {CreateJob} from "./components/jobs/create.js";
import {EditJob } from "./components/jobs/edit.js";
import { Index as JobSeeker } from "./components/job-seekers/index.js";

// accounts
// import {View as ViewAccount} from "./components/accounts/view.js"
// packages
import {Index as Package} from "./components/packages/index.js";
import {Create as CreatePackage} from "./components/packages/create.js";
import {Edit as EditPackage} from "./components/packages/edit.js";

// job types
import {JobTypes} from "./components/job-types/index.js";
import {EditJobType} from "./components/job-types/edit.js";
import {CreateJobType} from "./components/job-types/create.js";

// import { Layout } from "./components/Layout.js";
// import { Index as Transaction } from "./components/transactions/index.js";
// import { View as ViewJob } from "./components/jobs/view.js";
// job application
import {Index as JobApplication} from "./components/job-applications/index.js";

import {Layout} from "./components/Layout.js";
import {Index as Transaction} from "./components/transactions/index.js";
// import {View as ViewJob} from "./components/jobs/view.js";
import {Index as Subscription} from "./components/subscriptions/index.js";

// currencies
import {EditCurrency} from "./components/currency/edit.js";
import {CreateCurrency} from "./components/currency/create.js";
import {Currencies} from "./components/currency/index.js";

// categories
import {CreateCategory} from "./components/categories/create.js";
import {Categories} from "./components/categories/index.js";
import {EditCategory} from "./components/categories/edit.js";
// import


// 2. Define some routes
// Each route should map to a component. The "component" can
// either be an actual component constructor created via
// `Vue.extend()`, or just a component options object.

const routes = [
    {path: "/", component: Dashboard},

    {path: "/providers", component: JobProvider},
    {path: "/seekers", component: JobSeeker},

    // accounts
    // {path: "/accounts/:id", component: ViewAccount},

    // jobs
    {path: "/jobs", component: Jobs},
    {path: "/jobs/create", component: CreateJob},
    // {path: "/jobs/:id", component: ViewJob, params: true, name: "jobs"},
  { path: "/jobs/:slug/edit", component: EditJob },

    // job applications
    {path: "/job-applications", component: JobApplication},

    // packages
    {path: "/packages", component: Package},
    {path: "/packages/create", component: CreatePackage},
    {path: "/packages/:id/edit", component: EditPackage},

    // job types
    {path: "/job-types", component: JobTypes},
    {path: "/job-types/:id/edit", component: EditJobType},
    {path: "/job-types/create", component: CreateJobType},

    // transactions
    {path: "/transactions", component: Transaction},

    // subscriptions
    {path: "/subscriptions", component: Subscription},

  // currencies
  { path: "/currencies", component: Currencies },
  { path: "/currencies/:id/edit", component: EditCurrency },
  { path: "/currencies/create", component: CreateCurrency },

    // categories
    { path: "/categories", component: Categories },
    { path: "/categories/:id/edit", component: EditCategory },
    { path: "/categories/create", component: CreateCategory },
];

//  Create the router instance and pass the `routes` option
// You can pass in additional options here, but let's

const router = VueRouter.createRouter({
    // 4. Provide the history implementation to use. We are using the hash history so that it will not interfere with WordPress routes.
    history: VueRouter.createWebHashHistory(),
    // history: VueRouter.createWebHistory(),
    routes, // short for `routes: routes`
});

// VUE 3 initialization
const app = Vue.createApp({
    render() {
        return Vue.h(Layout, {});
    },
});
app.use(router);
app.mount("#km-content");
