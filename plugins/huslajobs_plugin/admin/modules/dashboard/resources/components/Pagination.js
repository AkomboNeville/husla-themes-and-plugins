// Check this link for explanations. Don't ask me to explain (^_^)
// https://stackoverflow.com/questions/7835752/how-to-do-page-navigation-for-many-many-pages-logarithmic-page-navigation

const Pagination = {
    props: {
        pages: {
            required: true,
            default: 0
        },
        page: {
            required: true,
            default: 1
        }
    },
    data() {
        return {
            links_per_step: 5,
            links: [],
            paginatedPage: 0,
            totalpages : 0,
            URL: '/page=',
            paginationStrings: VueUiStrings.pagination
        }
    },
    emits: ['update'],
    mounted() {
        this.paginatedPage = this.page;
        this.totalpages = this.pages;
        if (this.pages >1){
            this.getPagination();
        }

    },
    methods: {
        goToPage(currentPage) {
            if (currentPage != this.paginatedPage){
                this.$emit('update', currentPage);
                this.paginatedPage =currentPage;
                this.getPagination();
            }
        },
        getPagination() {
            this.setLimitPerPage();
            if (this.totalpages === 1){
                return;
            }else if(this.links_per_step >= this.totalpages){
                this.links = [];
                let counter = 1;
                while (counter < this.totalpages ){
                    this.links.push(counter);
                    counter++;
                }
            }else if (this.totalpages > this.links_per_step){
                const lastLink = this.totalpages;
                this.links = [];
                const firstLink = 1;
                this.links.push( firstLink );
                let newLinksPerPage = this.links_per_step -1; //we minus one because we added the first link to the links array

                /**
                 * checks if the current page is lessthan or equal to newLinksPerPage
                 */
                if (this.paginatedPage <= newLinksPerPage){
                    let pageMinusOne = this.paginatedPage -1;
                    /**
                     * this block checks if the difference between the current page and 1 is greater than 1
                     * to add eclipse
                     */
                    if (pageMinusOne > 1){
                        /**
                         * the value four is to make sure that the available width can contain the
                         * -first and last links
                         * -the two seperators
                         */
                        if (newLinksPerPage>4){
                            this.links.push( 0 );
                            // newLinksPerPage;
                            let counter = this.paginatedPage;
                            newLinksPerPage  = newLinksPerPage -3;
                            while (counter < (newLinksPerPage + this.paginatedPage)){
                                this.links.push( counter );
                                counter++;
                            }
                        }else{
                            this.links.push( this.paginatedPage );
                        }

                        this.links.push( 0 );
                    }else{
                        let counter = 2;
                        while (counter < newLinksPerPage){
                            this.links.push( counter );
                            counter++;
                        }
                        this.links.push( 0 );
                    }

                }else{
                    /**
                     * the value four is to make sure that the available width can contain the
                     * -first and last links
                     * -the two seperators
                     */
                    if (newLinksPerPage<=4){
                        this.links.push( 0 );
                    }
                    /**
                     * this block sets the counter to the current page and loop to totalpages -1
                     */
                    else if ((newLinksPerPage + this.paginatedPage) === this.totalpages){
                        this.links.push( 0 );
                        let counter = this.paginatedPage;
                        while (counter < this.totalpages ){
                            this.links.push( counter );
                            counter++;
                        }
                    }
                    /***
                     * this block adds eclipse after the first link
                     * it sets the counter to the difference of total pages and newlinksPerPage
                     */
                    else if (this.paginatedPage === newLinksPerPage){
                        this.links.push( 0 );
                        let counter = this.totalpages - newLinksPerPage;
                        while (counter < this.totalpages ){
                            this.links.push( counter );
                            counter++;
                        }
                    }
                    /**
                     * this block calculates the difference between the total pages aabd the current page
                     * to determine number of links to be added to the right and left of the current page
                     */
                    else if((newLinksPerPage + this.paginatedPage) > this.totalpages ){

                      let dif =  this.totalpages - this.paginatedPage;
                      let leftGap = newLinksPerPage - dif ;
                        this.links.push( 0 );
                        let counter = this.paginatedPage - leftGap;

                        // loop 1
                        while (counter < this.paginatedPage){
                            this.links.push( counter );
                            counter++;
                        }

                    //    loop 2
                        if (dif > 0){
                            counter = this.paginatedPage;
                            while (counter < this.totalpages ){
                                this.links.push( counter );
                                counter++;
                            }
                        }

                    }
                    else if((newLinksPerPage + this.page) < this.totalpages ){
                        newLinksPerPage = newLinksPerPage - 2;
                        let rightGap = Math.ceil( newLinksPerPage/2);
                        let leftGap = Math.floor( newLinksPerPage / 2 );
                        this.links.push( 0 );
                        //  first loop
                        let counter = this.paginatedPage - leftGap;
                        while (counter<this.paginatedPage){

                            this.links.push( counter );
                            counter++;
                        }
                        //    loop 2
                         counter = this.paginatedPage;
                        while (counter <= (this.paginatedPage + rightGap) ){
                           this.links.push( counter );
                            counter++;
                        }
                        this.links.push( 0 );
                    }
                }
            }
        },
        setLimitPerPage(){
            /**
             * This function calculates the number of links that can be on the page at a given time
             * @type {*|number}
             */
        //    get ul.pagination width
            const paginationWidth = document.querySelector('#husla-pagination').offsetWidth;
            let lastListWidth = document.querySelector('li#last-page').offsetWidth;
            let listWidth = lastListWidth; // ;
            if (this.paginatedPage < 100) {
                let firstListWidth = document.querySelector('#husla-pagination ul li:first-child').offsetWidth;
                const averageListWidth = (parseInt(lastListWidth) + parseInt( firstListWidth) ) /2;
                listWidth =  Math.ceil (averageListWidth);
            }
            const widthOfFourList = 5 * parseInt(listWidth);
            const availableWidth = parseInt(paginationWidth) - widthOfFourList;
            this.links_per_step = Math.floor(availableWidth/listWidth);
        }
    },
    template: `<div class="row" xmlns="http://www.w3.org/1999/html">
    <div v-if="pages > 1" class="col-md-12">

        <nav id="husla-pagination" aria-label="Page navigation example" style="width: 100%">

            <ul class="pagination mx-auto">
                <li class="page-item" v-if="page > 1" @click="goToPage(1)" style="cursor:pointer">
                    <span class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo; </span>
                        <span class="sr-only">First Page</span>
                    </span>
                </li>
                <li class="page-item" v-if="page > 1" @click="goToPage(page - 1)" style="cursor:pointer">
                    <span class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&lt;&nbsp; </span>
                        <span class="sr-only">Previous</span>
                    </span>
                </li>
                <li class="page-item disabled" v-if="page <= 1">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo; </span>
                        <span class="sr-only">First Page</span>
                    </a>
                </li>
                <li class="page-item disabled" v-if="page <= 1">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&lt;&nbsp; </span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <li class="page-item" v-for="link of links"
                    v-bind:class="{ active: link == page }">
                    <span v-if="link" class="page-link" @click="goToPage(link)" style="cursor:pointer">{{link}}</span>
                    <span v-else="link" class="page-link">...</span>
                </li>
                <li id="last-page" class="page-item"
                    v-bind:class="{ active: page == pages }">
                    <span class="page-link" @click="goToPage(pages)" style="cursor:pointer">{{pages}}</span>
                </li>

                <li class="page-item" v-if="page < pages" @click="goToPage(page + 1)" style="cursor:pointer">
                
                    <span class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&gt;</span>
                        <span class="sr-only">Last Page</span>
                    </span>
                </li>
                <li class="page-item" v-if="page < pages" @click="goToPage(pages)" style="cursor:pointer">
                    <span class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </span>
                </li>
                <li class="page-item disabled" v-if="page >= pages">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&gt;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
                <li class="page-item disabled" v-if="page >= pages">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
            <div class="text-center">
                <p>
                    <span class="mr-1">{{paginationStrings.page}}</span>
                    <span class="mr-1">{{page}}</span>
                    <span class="mr-1">{{paginationStrings.of}}</span>
                    <span class="mr-1">{{pages}}</span>
                </p>
            </div>
        </nav>
    </div>

</div>`
}

export {Pagination};