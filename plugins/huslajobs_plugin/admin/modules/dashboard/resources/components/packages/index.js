import {LoadingBar} from "../LoadingBar.js";
import {LoadingError} from "../LoadingError.js";
import {Pagination} from "../Pagination.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
const Index = {
    data() {
        return {
            packages: [],
            loading: true,
            error: false,
            router: {},
            pages: 0,
            page: 1,
            sortField: 'id',
            sort: 'desc',
            searchField: 'name',
            searchText: '',
            packageIndexStrings:VueUiStrings.adminBackend.packages.index
        };
    },
    components: {LoadingBar, LoadingError, Pagination},
    mounted() {
        this.router = VueRouter.useRouter()
        this.getPackages();
    },
    mixins:[ReusableFunctions],
    methods: {
        search() {
            this.page = 1;
            this.sortField = 'id';
            this.sort = 'desc';
            this.getPackages();
        },
        sortBy(field) {
            if (this.sortField == field) {
                this.sort = this.sort == 'desc' ? 'asc' : 'desc'
            } else {
                this.sort = 'asc';
                this.sortField = field;
            }
            this.page = 1;
            this.getPackages();
        },
        isSortedBy(field, sort = 'asc') {
            switch (sort) {
                case 'asc':
                    return this.sortField == field && sort == this.sort;
                case 'desc':
                    return this.sortField == field && sort == this.sort;
                default:
                    return true;
            }
        },
        goToAddPackagePage() {
            this.router.push('/packages/create')
        },
        getPackages() {
            this.loading = true
            const data = new FormData();
            data.append("action", "get_packages");
            data.append("page", this.page);
            data.append("perPage", jobs_per_page);
            data.append("sortBy", this.sortField);
            data.append("order", this.sort);
            data.append("searchText", this.searchText);
            data.append("searchField", this.searchField);
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    that.packages = response.data.data;
                    that.pages = response.data.totalPages;
                    that.loading = false
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.packageIndexStrings.errors.errorOccurred);
                    }
                    that.loading = false
                    that.error = true;
                });
        },
        deletePackage(Package) {
            const confirm_delete = confirm(this.packageIndexStrings.confirm+" " + Package.name + '?');
            if (confirm_delete) {
                this.loading = true
                const data = new FormData();
                data.append("action", "delete_package");
                data.append("package_id", Package.id);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        toastr.success(that.packageIndexStrings.successMessage.packageDeleted);
                        that.getPackages();
                        that.loading = false
                    })
                    .catch(function (error) {
                        console.log(error);
                        that.loading = false

                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.packageIndexStrings.errors.errorOccurred);
                        }

                    });
            }
        },
        goToPage(page) {
            this.page = page
            this.getPackages();
        }
    },
    template: `<div>
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <router-link to="/" href="#">{{packageIndexStrings.dashboard}}</router-link>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{packageIndexStrings.packages}}</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">{{packageIndexStrings.packages}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="getPackages"></loading-error>
                        </div>
                        <div v-else>
                            <button type="button" class="hs-btn-primary hs-btn mb-4 float-right" @click="goToAddPackagePage">
                                {{packageIndexStrings.buttons.addPackage}}
                            </button>
                            <form class="form-inline mt-2">
                                <div class="input-group mb-2 mr-2">
                                    <select class="form-control" v-model="searchField">
                                        <option value="name">{{packageIndexStrings.name}}</option>
                                        <option value="duration">{{packageIndexStrings.duration}}</option>
                                        <option value="price">{{packageIndexStrings.price}}</option>
                                    </select>
                                </div>
                                <div class="form-group mb-2">
                                    <input type="text" class="form-control"
                                           placeholder="search text..." v-model="searchText">
                                </div>
                                <button type="submit" class="hs-btn-primary hs-btn ml-2 mb-2" @click="search()">
                                    {{packageIndexStrings.buttons.search}}
                                </button>
                            </form>
                            <div class="hs-table-wrapper">
                            <table class="table table-striped thead-dark mt-2">
                                <tr>
                                    <th @click="sortBy('id')" style="cursor:pointer">{{packageIndexStrings.id}}
                                        <i class="fa fa-fw"
                                           v-bind:class="{'fa-sort-down':isSortedBy('id','desc'),'fa-sort-up':isSortedBy('id','asc'),'fa-sort':isSortedBy('id','normal')}"></i>
                                    </th>
                                    <th @click="sortBy('name')" style="cursor:pointer">{{packageIndexStrings.name}}
                                        <i class="fa fa-fw"
                                           v-bind:class="{'fa-sort-down':isSortedBy('name','desc'),'fa-sort-up':isSortedBy('name','asc'),'fa-sort':isSortedBy('name','normal')}"></i>
                                    </th>
                                    <th @click="sortBy('duration')" style="cursor:pointer">{{packageIndexStrings.duration}}
                                        <i class="fa fa-fw"
                                           v-bind:class="{'fa-sort-down':isSortedBy('duration','desc'),'fa-sort-up':isSortedBy('duration','asc'),'fa-sort':isSortedBy('duration','normal')}"></i>
                                    </th>
                                    <th @click="sortBy('price')" style="cursor:pointer">{{packageIndexStrings.price}}
                                        <i class="fa fa-fw"
                                           v-bind:class="{'fa-sort-down':isSortedBy('price','desc'),'fa-sort-up':isSortedBy('price','asc'),'fa-sort':isSortedBy('price','normal')}"></i>
                                    </th>
                                    <th>{{packageIndexStrings.actions}}</th>
                                </tr>
                                <tr v-for="package of packages">
                                    <td>{{package.id}}</td>
                                    <td>{{decodeHtml(package.name)}}</td>
                                    <td>{{package.duration}}</td>
                                    <td>{{package.price}}</td>
                                    <td>
                                        <router-link :to="'/packages/'+package.id+'/edit'"
                                                     class="btn mr-2 mb-2 btn-primary btn-sm"><i
                                                class="fa-pencil-alt fa fa-2x"></i>
                                        </router-link>
                                        <button class="btn btn-danger mb-2 btn-sm" @click="deletePackage(package)"><i
                                                class="fa fa-trash fa-2x"></i></button>
                                    </td>
                                </tr>
                            </table>
                            </div>
                            <pagination :page="page" :pages="pages" @update="goToPage"></pagination>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
};

export {Index};
