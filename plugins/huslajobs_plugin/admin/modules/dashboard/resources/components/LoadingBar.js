const LoadingBar = {
    data() {
        return {
            theme_url: THEME_URL
        }
    },
    template: `
<div class="hs-loader d-flex align-items-center ">
<div id="loading-screen">
     <img :src="theme_url+'/logo.png'" style="animation-name: km-img-loading;
          animation-duration: 2s;
          animation-iteration-count: infinite;"/>
<!--          <p><i class="fa fa-spinner fa-pulse text-white d-inline-block"></i></p>-->
                
    </div>
</div>`
}

export {LoadingBar};