import { CurrencyForm } from "./add-form.js";

const CreateCurrency = {
    props: {
        formParams: {
            type: Object,
            default: function () {
                return { id: undefined, name: undefined, symbol: undefined };
            },
        },
    },
    data(){
        return{
            currencyCreateStrings: VueUiStrings.adminBackend.currency.create
        }
    },
    components: {
        CurrencyForm,
    },

    template: `

<div class="module-content-wrapper">
  <div class="row page-title">
    <div class="col-md-12">
        <nav  aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><router-link to="/">{{currencyCreateStrings.dashboard}}</router-link></li>
                <li class="breadcrumb-item"><router-link to="/job-types">{{currencyCreateStrings.currencies}}</router-link></li>
                <li class="breadcrumb-item active" aria-current="page">{{currencyCreateStrings.addCurrency}}</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">{{currencyCreateStrings.addCurrency}}</h4>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body"> 
         <currency-form ></currency-form >
        </div>
      </div>
    </div>
  </div>

</div>
`,
};
export { CreateCurrency };
