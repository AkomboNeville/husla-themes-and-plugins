import {ReusableFunctions} from "../../../../../../js/functions.js";

const CurrencyForm = {
    props: {
        formParams: {
            type: Object,
            default: function () {
                return {id: undefined, name: undefined, code: undefined};
            },
        },
    },
    data() {
        return {
            name: this.formParams.name,
            // symbol: this.formParams.symbol,
            code: this.formParams.code,
            formErrors: {},
            submitting: false,
            addFormStrings: VueUiStrings.adminBackend.currency.addForm
        };
    },
    mixins: [ReusableFunctions],
    watch: {
        name: function (current) {
            this.validateInput("name", current, {required: true}, this.addFormStrings.formFields.name.label);
        },
        code: function (current) {
            this.validateInput("code", current, {required: true}, this.addFormStrings.formFields.code.label);
        },

    },
    methods: {
        submitForm() {
            this.submitting = true;
            //validate inputs
            this.validateInput("name", this.name, {required: true}, this.addFormStrings.formFields.name.label);
            this.validateInput("code", this.code, {required: true}, this.addFormStrings.formFields.code.label);
            if (Object.keys(this.formErrors).length === 0) {
                const data = new FormData();
                this.submitting = true;
                const that = this;
                let action = "save_currency";

                if (this.formParams.id) {
                    action = "update_currency";
                    data.append("currency_id", this.formParams.id);
                }
                data.append("name", this.name);
                data.append('code', this.code)
                data.append("action", action);
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            if (that.formParams.id) {
                                toastr.success(that.addFormStrings.successMessage.currencyUpdated, 'Success')
                            } else {

                                toastr.success(that.addFormStrings.successMessage.currencyCreated, 'Success')
                            }
                            that.$router.push({
                                path: "/currencies",
                            });
                            that.submitting = false;

                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.addFormStrings.errors.errorOccurred);
                        }
                        that.submitting = false;
                    });
            } else {
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }
        },
    },
    template: `
          <form @submit.prevent="submitForm">
              <div class="mb-4">
                  <label for="name" class="form-label">{{addFormStrings.formFields.name.label}} <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="name" v-model="name">
                  <span class="text-danger validation-error" v-if="formErrors.name">{{formErrors.name}}</span>
              </div>
              <div class="mb-4">
                  <label for="code" class="form-label">{{addFormStrings.formFields.code.label}} <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="code" v-model="code">
                  <span class="text-danger validation-error" v-if="formErrors.code">{{formErrors.code}}</span>
              </div>
              <div class="mb-4">
                <button class="hs-btn-signup hs-btn-primary hs-btn" :disabled="submitting || Object.keys(this.formErrors).length > 0">{{addFormStrings.formFields.button}} <i v-if="submitting" class="fa fa-spinner fa-pulse"></i></button>
              </div>
          </form>
`,
};
export {CurrencyForm};
