import { JobForm } from "./add-form.js";
import { LoadingBar } from "../LoadingBar.js";
import { LoadingError } from "../LoadingError.js";

const EditJob = {
    data() {
        return {
            loading: true,
            error: false,
            form: {},
            editJobStrings: VueUiStrings.adminBackend.jobs.edit
        };
    },
    components: {
        JobForm,
        LoadingBar,
        LoadingError,
    },

    mounted() {
        this.getJob();
    },
    methods: {
        getJob() {
            const data = new FormData();
            data.append("job_slug", this.$route.params.slug);
            data.append("action", "get_job");

            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: { "Content-Type": "multipart/form-data" },
            })
                .then(function (response) {
                    that.form = response.data;
                    that.form.cv_required = parseInt(that.form.cv_required);
                    that.loading = false;
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    that.loading = false;
                    that.error = true;
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.editJobStrings.errors.errorOccurred);
                    }
                });
        },
    },
    template: `
  <div class="module-content-wrapper">
      <loading-bar v-if="loading"></loading-bar>
      <div v-if="!loading">
        <div class="row page-title">
          <div class="col-md-12">
              <nav  aria-label="breadcrumb" class="float-right mt-1">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><router-link to="/">{{editJobStrings.dashboard.toString()}}</router-link></li>
                      <li class="breadcrumb-item"><router-link to="/jobs">{{editJobStrings.jobs.toString()}}</router-link></li>
                      <li class="breadcrumb-item active" aria-current="page">{{editJobStrings.editJob.toString()}}</li>
                  </ol>
              </nav>
              <h4 class="mb-1 mt-0">{{editJobStrings.editJob.toString()}}</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
  
                <div v-if="error">
                  <loading-error @update="getJob"></loading-error>
                </div>
                <div v-else>
                  <div v-if="Object.keys(form).length">
                      <job-form :formParams="form" :showLoader="false"></job-form >
                  </div>
                </div>
           
            
          </div>
        </div>
      </div>
  </div> 
`,
};
export { EditJob };
