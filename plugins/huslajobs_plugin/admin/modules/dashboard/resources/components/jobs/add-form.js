import {LoadingBar} from "../LoadingBar.js";
import {LoadingError} from "../LoadingError.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {CountriesData} from "../../../../../../js/countries_and_codes.js";
import {world_countries as WorldCountries} from "../../../../../../js/countries_data.js";
const JobForm = {
    props: {
        formParams: {
            type: Object,
            required:false
        },
        showLoader:{

            default:true
        }
    },
    components: {LoadingBar, LoadingError,AnaVueSelect},
    data() {
        return {
            jobTitle: this.formParams?.name ?? undefined,
            description: this.formParams?.description?? undefined,
            categoryId: this.formParams?.category_id ?? undefined,
            job_type_id: this.formParams?.job_type_id ?? undefined,
            // expiry_date: this.formParams.expiry_date,
            salary: this.formParams?.salary ?? undefined,
            account_id:this.formParams?.account_id ?? undefined,
            currency_id: this.formParams?.currency_id ?? undefined,
            experience: this.formParams?.experience ?? undefined,
            cv_required: this.formParams?.cv_required ?? 1,
            motivation_required: this.formParams?.motivation_required ?? 0,
            country: undefined,
            city: this.formParams?.city ?? undefined,
            state: this.formParams?.state ?? undefined,
            country_name: this.formParams?.country_name ?? undefined,
            formErrors: {},
            work:this.formParams?.work,
            submitting: false,
            loading: true,
            error: false,

            jobTypes: [],
            jobCategories: [],
            currencies:[],
            accounts:[],
            formErrors: {},
            addFormStrings:VueUiStrings.adminBackend.jobs.addForm,
            works: [{label: 'On site', value: 'On site'}, {label: 'Remote', value: 'Remote'}],
            countries: WorldCountries,
            jobStates:[],
        };
    },
    mixins:[ReusableFunctions],
    watch: {
        name: function (current) {
            this.validateInput("jobTitle", current, {required: true}, this.addFormStrings.formFields.jobTitle.label);
        },
        description: function (current) {
            this.validateInput("description", current, {required: true,min:100}, this.addFormStrings.formFields.description.label);
        },
        categories: function (current) {
            this.validateInput("categoryId", current, {required: true}, this.addFormStrings.formFields.jobCategory.label);
        },
        job_type_id: function (current) {
            this.validateInput("job_type_id", current, {required: true}, this.addFormStrings.formFields.jobType.label);
        },
        account_id: function (current) {
            this.validateInput("account_id", current, {required: true}, this.addFormStrings.formFields.account.label);
        },
        currency_id: function (current) {
            this.validateInput("currency_id", current, {isCurrency: true}, this.addFormStrings.formFields.currency.label);
        },
        country: function (currentVal, oldVal) {
            this.validateInput("country", currentVal, {required: true}, this.addFormStrings.formFields.country.label);
            this.setState(currentVal)
        },
        city:function (currentVal, oldVal) {
            this.validateInput("city", currentVal, {required: true}, this.addFormStrings.formFields.city.label);
        },
        state: function (currentVal, oldVal) {
            this.validateInput("state", currentVal, {required: true}, this.addFormStrings.formFields.state.label);
        },
        // experience: function (currentVal, oldVal) {
        //     if (currentVal) {
        //         this.validateInput("experience", currentVal, {isNumeric: true},this.addFormStrings.formFields.experience.label);
        //     }
        // },
        salary: function (currentVal, oldVal) {
            if (currentVal) {
                this.validateInput("salary", currentVal, {isNumeric: true}, this.addFormStrings.formFields.salary.label);
            }
        },
    },

    methods: {
        setState(currentVal){
            this.jobStates = [];
            if (currentVal) {
                const countriesData = CountriesData;
                let country = countriesData.filter((country) => country.code.toLowerCase() === currentVal.toLowerCase())[0];
                let states = country.states;
                this.country_name = country.name;
                if (states.length) {
                    this.jobStates = states.map((state) => {
                        return {
                            label: state,
                            value: state
                        }
                    });
                }
            } else {
                // this.form.country =
                this.jobStates = [];
                this.formData.jobState = undefined

            }
        },
        submitForm() {
            //validate inputs
            this.validateInput("jobTitle", this.jobTitle,{required: true},this.addFormStrings.formFields.jobTitle.label);
            this.validateInput("description", this.description,{required: true},this.addFormStrings.formFields.description.label);
            this.validateInput("account_id", this.account_id, {required: true}, this.addFormStrings.formFields.account.label);
            // this.validateInput("experience", this.experience, {required: true}, this.addFormStrings.formFields.experience.label);
            this.validateInput("job_type_id", this.job_type_id, {required: true}, this.addFormStrings.formFields.jobType.label);
            this.validateInput("currency_id", this.currency_id, {isCurrency: true}, this.addFormStrings.formFields.currency);
            this.validateInput("categoryId", this.categoryId, {required: true}, this.addFormStrings.formFields.jobCategory.label);
            this.validateInput("country", this.country, {required: true}, this.addFormStrings.formFields.country.label);
            this.validateInput("state", this.state, {required: true}, this.addFormStrings.formFields.state.label);
            this.validateInput("city", this.city, {isCurrency: true}, this.addFormStrings.formFields.city.label);


            if (Object.keys(this.formErrors).length === 0) {
                const data = new FormData();
                this.submitting = true;
                const that = this;
                let action = "save_job";
                if (this.formParams?.id) {
                    action = "update_job";
                    data.append("job_id", this.formParams.id);
                }
                data.append("name", this.jobTitle);
                data.append("description", this.description);
                data.append("category_id", this.categoryId);
                data.append("currency_id", this.currency_id);
                data.append("experience", this.experience);
                data.append("cv_required", this.cv_required);
                data.append("motivation_required", this.cv_required);
                data.append("salary", this.salary);
                data.append("job_type_id", this.job_type_id);
                data.append("work", this.work);
                data.append("account_id",this.account_id);
                data.append("country_name",this.country_name);
                data.append('jobCountry', this.country ?? '');
                data.append('jobCity', this.city ?? '');
                data.append('jobState', this.state ?? '');
                data.append("action", action);

                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            if (that.formParams?.id){
                                toastr.success( that.addFormStrings.successMessage.jobUpdated,'Success')
                            }else {
                                toastr.success(that.addFormStrings.successMessage.jobCreated, 'Success')
                            }
                            that.$router.push({
                                path: "/jobs",
                            });
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.addFormStrings.errors.errorOccurred);
                        }
                        that.submitting = false;
                    });
            }
            else{
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }
        },

    },
    mounted() {
        this.country = this.formParams?.country ?? undefined,
        //todo get all
        this.getJobDependencies(false);


    },
    template: `
 <div>
    <loading-bar v-if="loading && showLoader"></loading-bar>
    <div v-if="!loading" class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body"> 
                    
                        <div v-if="jobTypes.length > 0 && jobCategories.length> 0 && currencies.length > 0 && accounts.length > 0">
                            <form class="husla-form" @submit.prevent="submitForm">
                            <div class="mb-4">
                                <label for="name" class="form-label">{{addFormStrings.formFields.jobTitle.label.toString()}} <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" id="name" v-model="jobTitle" :placeholder="addFormStrings.formFields.jobTitle.placeholder">
                                <span class="text-danger" v-if="formErrors.jobTitle">{{formErrors.jobTitle.toString()}}</span>
                            </div>
                            <div v-if="jobTypes.length" class="mb-4">
                            
                                <label for="job-type" class="form-label">{{addFormStrings.formFields.jobType.label.toString()}}<span class="text-danger">*</span></label>
                                                                            <AnaVueSelect  :options="jobTypes"
                                                          :placeholder="addFormStrings.formFields.jobType.placeholder"
                                                          v-model="job_type_id"></AnaVueSelect>
                                <span class="text-danger" v-if="formErrors.job_type_id">{{formErrors.job_type_id.toString()}}</span>
                            </div>
                            <div class="mb-4">
                                <label for="job-categories" class="form-label">{{addFormStrings.formFields.jobCategory.label.toString()}} <span class="text-danger">*</span></label>
                                            <AnaVueSelect :options="jobCategories"
                                                          :placeholder="addFormStrings.formFields.jobCategory.placeholder"
                                                          v-model="categoryId"></AnaVueSelect>
                                <span class="text-danger" v-if="formErrors.categoryId">{{formErrors.categoryId.toString()}}</span>
                            </div>
                            <div class="mb-4">
                                <label for="description" class="form-label">{{addFormStrings.formFields.description.label.toString()}} <span class="text-danger">*</span></label>
                                <textarea id="description" class="form-control"  row="4" v-model="description" :placeholder="addFormStrings.formFields.jobCategory.placeholder"></textarea>
                                <span class="text-danger" v-if="formErrors.description">{{formErrors.description.toString()}}</span>
                            </div>
                                                    <div class="row">
                            
                                    <div class="col-md-6 mb-4">
                                <AnaVueSelect v-model="country" :options="countries"
                                              :placeholder="addFormStrings.formFields.country.placeholder"></AnaVueSelect>
                                <span class="text-danger validation-error" v-if="formErrors.country">{{formErrors.country}}</span>

                            </div>
                            <div class="col-md-6 mb-4">
                                <AnaVueSelect v-model="state" :options="jobStates"
                                              :placeholder="addFormStrings.formFields.state.placeholder"
                                              :fieldName="addFormStrings.formFields.state.label"
                                              :disabled="!country"></AnaVueSelect>
                                <small>{{addFormStrings.formFields.state.tip}}</small>
                                <span class="text-danger d-block validation-error" v-if="formErrors.state">{{formErrors.state}}</span>
                            </div>                    
</div>


                            <div class="mb-4">
                                <input type="text" name="city" id="city" v-model="city"
                                       :placeholder="addFormStrings.formFields.city.placeholder"
                                       class="form-control">
                                <span class="text-danger validation-error"
                                      v-if="formErrors.city">{{formErrors.city}}</span>
                            </div>
                            <div class="mb-4">
                                <label for="experience" class="form-label">Experience </label>
                                     <input type="number" class="form-control" id="experience" v-model="experience" placeholder="Employee experience in years"/> 
<!--                                <span class="text-danger" v-if="formErrors.experience">{{formErrors.experience}}</span>-->
                            </div>
                            <div class="mb-4">
                                <label for="salary" class="form-label">salary</label>
                                <div class="row">
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" id="salary" v-model="salary" />
                                        <span class="text-danger" v-if="formErrors.salary">{{formErrors.salary}}</span>
    
                                    </div>
                                    <div class="col-md-3">
                                            <AnaVueSelect  :options="currencies"
                                                          :placeholder="addFormStrings.formFields.currency.placeholder"
                                                          v-model="currency_id"></AnaVueSelect>
                                                          
                                      <span class="text-danger" v-if="formErrors.currency_id">{{formErrors.currency_id}}</span>
                                    </div>
                                
                                </div>
                            </div> 
                            <div class="mb-4">
                                <label  class="form-label">{{addFormStrings.formFields.work.label}}<span class="text-danger">*</span></label>
                                    <AnaVueSelect  :options="works" :placeholder="addFormStrings.formFields.work.placeholder"
                                                  v-model="work"></AnaVueSelect>
                                
                                <span class="text-danger" v-if="formErrors.work">{{formErrors.work}}</span>
                            </div>
                            <div class="mb-4">
                                <label for="account_id" class="form-label">{{addFormStrings.formFields.account.label}} <span class="text-danger">*</span></label>
                                 <AnaVueSelect  :options="accounts"
                                                          :placeholder="addFormStrings.formFields.account.placeholder"
                                                          v-model="account_id"></AnaVueSelect>

                                <span class="text-danger" v-if="formErrors.account_id">{{formErrors.account_id}}</span>
                            </div>
                            <div class="mb-4">
                                    <label for="cv_required" class="form-label">{{addFormStrings.formFields.cv.placeholder}}</label>               
                                                              <div class="d-flex flex-wrap">
                                    <div class="mr-5">
                                        <input type="radio" class="mr-2" id="yes" name="create_account"
                                               v-model="cv_required" value="1"/>
                                        <label for="yes" class="form-label cursor-pointer">{{addFormStrings.yes}}</label>
                                    </div>
                                    <div >
                                        <input type="radio" class="mr-2" id="no" name="create_account"
                                               v-model="cv_required" value="0"/>
                                        <label cursor-pointer for="no"
                                               class="form-label cursor-pointer">{{addFormStrings.no}}</label>
                                    </div>

                                </div>              
                            </div> 
                            <div class="mb-4">
                                    <label for="cv_required" class="form-label">{{addFormStrings.formFields.motivation.placeholder}}</label>               
                                                              <div class="d-flex flex-wrap">
                                    <div class="mr-5">
                                        <input type="radio" class="mr-2" id="motivation-yes" name="motivation"
                                               v-model="motivation_required" value="1"/>
                                        <label for="motivation-yes" class="form-label cursor-pointer">{{addFormStrings.yes}}</label>
                                    </div>
                                    <div >
                                        <input type="radio" class="mr-2" id="motivation-no" name="motivation"
                                               v-model="motivation_required" value="0"/>
                                        <label cursor-pointer for="motivation-no"
                                               class="form-label cursor-pointer">{{addFormStrings.no}}</label>
                                    </div>

                                </div>              
                            </div>                             
                                             
                            <div class="mb-3">
                                <button class="hs-btn hs-btn-signup hs-btn-primary" :disabled="submitting || Object.keys(this.formErrors).length > 0">{{addFormStrings.formFields.button}} <i v-if="submitting" class="fa fa-spinner fa-pulse"></i></button>
                            </div>
                    
                            </form>
                        </div>
                        <div v-else>
                            <p v-if="accounts.length < 1">{{addFormStrings.noAccounts}}</p>
                         
                            <p v-else>
                                <span class="mr-1">{{addFormStrings.noAvailable}}</span>
                            
                                <span v-if="jobTypes.length < 1">
                                 {{addFormStrings.jobTypes}}
                                </span> 
                                <span v-if="jobCategories.length < 1 && jobTypes.length < 1">,</span>
                                  <span v-if="jobCategories.length < 1">
                                     {{addFormStrings.jobCategories}}
                                 </span>
                             <span v-if="(jobTypes.length < 1 || jobCategories.length < 1 ) && currencies.length < 1">,</span>
                             <span v-if="currencies.length < 1">
                                 {{addFormStrings.currency}}
                             </span>
                            </p>
                      </div>  
                </div>
            </div>
        </div>
    </div>
</div>
        
`,
};
export {JobForm};
