import {ReusableFunctions} from "../../../../../js/functions.js";

const Dashboard = {
    data() {
        return {
            count: 0,
            statistics:undefined,
            loading:false,
            error:false,
            dashboardStrings: VueUiStrings.adminBackend.dashboard
        }
    },
    mixins:[ReusableFunctions],
    methods:{
      getDashboardStats(){
          this.loading = true
          const data = new FormData();
          data.append('action','hs_admin_dashboard_stats');
          data.append('page',1);
          data.append('perPage',4);
          const that = this;
          axios({
              method: "post",
              url: ajaxurl,
              data: data,
              headers: {"Content-Type": "multipart/form-data"},
          })
              .then(function (response) {
                  if (response.data) {
                      /** response.data ={
                       *                       'jobs,
                       *                       job_applications,
                       *                       job_categories,
                       *                       packages,
                       *                       job_types,
                       *                       subscriptions,
                       *                       recruiters,
                       *                           seekers
                       *                           }
                        */

                      that.statistics = response.data
                  }
                  // that.submitting = false;
              })
              .catch(function (error) {
                  console.log(error)
                  if (error.response) {
                      toastr.error(error.response.data.data);
                  } else {
                      toastr.error(that.dashboardStrings.errors.errorOccurred);
                  }
                  that.submitting = false;
              });
      }
    },
    mounted() {
        this.getDashboardStats();
    },
    template: `   
   <div  id="hs-statistics">
    <div class="row page-title align-items-center" >
        <div class="col-sm-4 col-xl-6">
            <h4 class="mb-1 mt-0">{{dashboardStrings.dashboard}}</h4>
        </div>
    </div>

    <!-- content -->
    <div class="row">
        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{dashboardStrings.jobs}}</span>
                            <h2 class="mb-0">{{statistics?.jobs.totalItems || 0}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                         <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{dashboardStrings.jobApplications}}</span>
                            <h2 class="mb-0">{{statistics?.job_applications.totalItems || 0}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{dashboardStrings.categories}}</span>
                            <h2 class="mb-0">{{statistics?.job_categories.totalItems || 0}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{dashboardStrings.jobTypes}}</span>
                            <h2 class="mb-0">{{statistics?.job_types.totalItems || 0}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                            <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{dashboardStrings.jobSeekers}}</span>
                            <h2 class="mb-0">{{statistics?.seekers.totalItems || 0}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                         <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{dashboardStrings.recruiters.toString()}}</span>
                            <h2 class="mb-0">{{statistics?.recruiters.totalItems || 0}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{dashboardStrings.packages}}</span>
                            <h2 class="mb-0">{{statistics?.packages.totalItems || 0}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6 col-xl-3 mb-4">
            <div class="card">
                <div class="card-body p-0">
                    <div class="media p-3">
                        <div class="media-body">
                                <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{dashboardStrings.subscriptions}}</span>
                            <h2 class="mb-0">{{statistics?.subscriptions.totalItems || 0}}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- stats + charts -->
    <!-- row -->

    <!-- products -->
    <div class="row">
    
        <div class="col-xl-12">
            <div class="card recent-subscriptions">
                <div class="card-body">
                    <h5 class="card-title mt-0 mb-0 header-title">{{dashboardStrings.recentSubscriptions}}</h5>

                    <div v-if="statistics?.subscriptions.data.length" class="table-responsive mt-4">
                        <table class="table table-hover table-nowrap mb-0">
                            <thead>
                            <tr>
                                <th scope="col">{{dashboardStrings.tableHeaders.package}}</th>
                                <th scope="col">{{dashboardStrings.tableHeaders.cost}}</th>
                                <th scope="col">{{dashboardStrings.tableHeaders.subscriber}}</th>
                                <th scope="col">{{dashboardStrings.tableHeaders.status}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr v-for="(subscription,index) in statistics.subscriptions.data">
                                <td>{{decodeHtml(subscription.package_name)}}</td>
                                <td>{{subscription.package_price}}</td>
                                <td>{{decodeHtml(subscription.first_name)}} {{decodeHtml(subscription.last_name)}}</td>
<!--                                <td>{{subscription.created_at}}</td>-->
                                <td>
                                     <span v-if="parseInt( subscription.status)" class="badge badge-soft-info py-1">{{dashboardStrings.active}}</span>
                                     <span v-else="parseInt( subscription.status)" class="badge badge-soft-danger py-1">{{dashboardStrings.expired}}</span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div> <!-- end table-responsive-->
                    <div v-else-if="statistics?.subscriptions.data.length == 0">
                    <h5 class="hs-subtitle">{{dashboardStrings.noSubscription}}</h5>
</div>
                </div> <!-- end card-body-->
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
    <!-- end row -->

    <!-- widgets -->
    </div> 
    <!-- end row -->`
}

export {Dashboard};