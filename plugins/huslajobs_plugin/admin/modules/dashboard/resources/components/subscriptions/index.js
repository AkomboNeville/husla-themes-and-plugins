import {LoadingBar} from "../LoadingBar.js";
import {LoadingError} from "../LoadingError.js";
import {Pagination} from "../Pagination.js";

const Index = {
    data() {
        return {
            subscriptions: [],
            loading: true,
            error: false,
            router: {},
            pages: 0,
            page: 1,
            subscriptionsStrings: VueUiStrings.adminBackend.subscriptions
        };
    },
    components: {LoadingBar, LoadingError, Pagination},
    mounted() {
        this.router = VueRouter.useRouter()
        this.getSubscriptions();
    },
    methods: {
        getSubscriptions() {
            this.loading = true
            const data = new FormData();
            data.append("action", "get_subscriptions");
            data.append("page", this.page);
            data.append("perPage", jobs_per_page);
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    that.subscriptions = response.data.data;
                    that.pages = response.data.totalPages;
                    that.loading = false
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.subscriptionsStrings.errors.errorOccurred);
                    }
                    that.loading = false
                    that.error = true;
                });
        },
        goToPage(page) {
            this.page = page
            this.getSubscriptions();
        }
    },
    template: `<div>
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <router-link to="/" href="#">{{subscriptionsStrings.dashboard}}</router-link>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{subscriptionsStrings.subscriptions}}</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">{{subscriptionsStrings.subscriptions}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="getSubscriptions"></loading-error>
                        </div>
                        <div v-else>
        <div class="hs-table-wrapper">
                            <table class="table table-striped thead-dark mt-2">
                                <tr>
                                    <th>{{subscriptionsStrings.tableHeaders.id}}</th>
                                    <th>{{subscriptionsStrings.tableHeaders.user}}</th>
                                    <th>{{subscriptionsStrings.tableHeaders.package}}</th>
                                    <th>{{subscriptionsStrings.tableHeaders.startDate}}</th>
                                    <th>{{subscriptionsStrings.tableHeaders.endDate}}</th>
                                </tr>
                                <tr v-for="subscription of subscriptions">
                                    <td>{{subscription.id}}</td>
                                    <td><router-link :to="'/accounts/'+subscription.user.ID">{{subscription.user.user_login}}</router-link></td>
                                    <td><router-link :to="'/packages/'+subscription.package.id+'/edit'">{{subscription.package.name}}</router-link> </td>
                                    <td>{{subscription.start_date}}</td>
                                    <td>{{subscription.end_date}}</td>
                                   
                                </tr>
                            </table>
                            </div>
                            <pagination :page="page" :pages="pages" @update="goToPage"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
};

export {Index};
