import {LoadingBar} from "../LoadingBar.js";
import {LoadingError} from "../LoadingError.js";
import {Pagination} from "../Pagination.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const Categories = {
    data() {
        return {
            categories: undefined,
            page: 1,
            perPage: jobs_per_page,
            loading: true,
            pages: 0,
            error: false,
            deleting: false,
            sortField: 'id',
            sort: 'desc',
            searchText:'',
            searchFields: "name,description",
            categoryIndexStrings: VueUiStrings.adminBackend.categories.index
        };
    },
    components: {LoadingBar, LoadingError, Pagination},
    mounted() {
        //todo get all
        this.getCategories();
    },
    watch: {
        searchText: function (current) {
            if (current === ''){
                this.search();
            }
        },
    },
    mixins:[ReusableFunctions],
    methods: {
        search() {
            this.page = 1;
            this.sortField = 'id';
            this.sort = 'desc';
            this.getCategories();
        },
        sortBy(field) {
            if (this.sortField == field) {
                this.sort = this.sort == 'desc' ? 'asc' : 'desc'
            } else {
                this.sort = 'asc';
                this.sortField = field;
            }
            this.page = 1;
            this.getCategories();
        },
        isSortedBy(field, sort = 'asc') {
            switch (sort) {
                case 'asc':
                    return this.sortField == field && sort == this.sort;
                case 'desc':
                    return this.sortField == field && sort == this.sort;
                default:
                    return true;
            }
        },
        getCategories() {
            this.loading = true;
            const data = new FormData();
            data.append("page", this.page);
            data.append("perPage", this.perPage);
            data.append("sortBy", this.sortField);
            data.append("order", this.sort);
            data.append("searchText", this.searchText);
            data.append("searchFields", this.searchFields);
            data.append("action", "get_categories");

            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    that.categories = response.data.data;
                    that.loading = false;
                    that.pages = response.data.totalPages;
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.categoryIndexStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.error = true;
                });
        },
        deleteCategory(category) {
            const confirm_delete = confirm(this.categoryIndexStrings.confirm +" " +this.decodeHtml(category.name) + "?");
            if (confirm_delete) {
                this.loading = true;
                const data = new FormData();
                data.append("action", "delete_category");
                data.append("category_id", category.id);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        toastr.success(category.name + " "+ that.categoryIndexStrings.deleted);
                        that.getCategories();
                        that.loading = false;
                    })
                    .catch(function (error) {
                        console.log(error);
                        that.loading = false;

                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.categoryIndexStrings.errors.errorOccurred);
                        }
                    });
            }
        },
        goToPage(page) {
            this.page = page
            this.getCategories();
        }
    },
    template: `<div class="module-content-wrapper">
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <router-link to="/">{{categoryIndexStrings.dashboard}}</router-link>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{categoryIndexStrings.categories}}</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">{{categoryIndexStrings.categories}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="getCategories"></loading-error>
                        </div>
                        <div v-else>
                                                <div class="d-flex justify-content-between align-items-center mb-4">
                            <form class="form-inline mt-2">
                                <div class="form-group mb-2">
                                    <input type="text" class="form-control"
                                           :placeholder="categoryIndexStrings.formFields.searchText.placeholder" v-model="searchText">
                                </div>
                                <button type="submit" class="hs-btn hs-btn-primary ml-2 mb-2 btn-sm" @click="search()">
                                    {{categoryIndexStrings.formFields.button}}
                                </button>
                            </form>
                            <router-link to="/categories/create" class="hs-btn hs-btn-primary">{{categoryIndexStrings.addCategory}}
                            </router-link>
                        
                        </div>

                            <div v-if="categories?.length > 0" class="hs-table-wrapper">
                                <table class="table table-striped">
                                    <tr>
                                        <th @click="sortBy('id')" style="cursor:pointer">
                                            {{categoryIndexStrings.tableHeaders.id}}
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('id','desc'),'fa-sort-up':isSortedBy('id','asc'),'fa-sort':isSortedBy('id','normal')}"></i>
                                        </th>
                                        <th @click="sortBy('name')" style="cursor:pointer">
                                            {{categoryIndexStrings.tableHeaders.name}}
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('name','desc'),'fa-sort-up':isSortedBy('name','asc'),'fa-sort':isSortedBy('name','normal')}"></i>

                                        </th>
                                        <th>{{categoryIndexStrings.tableHeaders.description}}</th>
                                        <th>{{categoryIndexStrings.tableHeaders.actions}}</th>
                                    </tr>
                                    <tr v-for="category of categories">
                                        <td>{{category.id}}</td>
                                        <td>{{decodeHtml(category.name)}}</td>
                                        <td>{{decodeHtml(category.description)}}</td>
                                        <td>
                                            <router-link :to="'/categories/'+category.id+'/edit'"
                                                         class="btn mr-2 mb-2 btn-primary btn-sm"><i
                                                    class="fa fa-pencil-alt fa-2x"></i>
                                            </router-link>
                                            <button class="btn btn-danger btn-sm mb-2" @click="deleteCategory(category)"><i
                                                    class="fa fa-trash fa-2x"></i></button>
                                        </td>
                                    </tr>
                                </table>

                            </div>

                            <div v-else-if="categories?.length == 0">
                                <p>{{categoryIndexStrings.noCategories}}</p>
                            </div>
                                                                                        <pagination v-if="pages > 1" :page="page" :pages="pages"
                                            @update="goToPage"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
};
export {Categories};
