import {ReusableFunctions} from "../../../../../../js/functions.js";

const CategoryForm = {
    props: {
        formParams: {
            type: Object,
            default: function () {
                return {
                    id: undefined,
                    name: undefined,
                    description: undefined,

                };
            },
        },
    },
    mixins:[ReusableFunctions],
    data() {
        return {
            name: this.formParams.name,
            description: this.formParams.description,
            formErrors: {},
            submitting: false,
            addFormStrings: VueUiStrings.adminBackend.categories.addForm
        };
    },
    watch: {
        name: function (current) {
            this.validateInput("name", current, {required: true}, this.addFormStrings.formFields.name.label);
        },
        description: function (current) {
            this.validateInput("description", current, {required: true}, this.addFormStrings.formFields.description.label);
        },
    },
    methods: {
        submitForm() {
            this.submitting = true;
            //validate inputs
            this.validateInput("name", this.name, {required: true}, this.addFormStrings.formFields.name.label);
            this.validateInput("description", this.description, {required: true}, this.addFormStrings.formFields.description.label);

            if (Object.keys(this.formErrors).length === 0) {
                const data = new FormData();
                this.submitting = true;
                const that = this;
                let action = "save_category";
                if (this.formParams.id) {
                    action = "update_category";
                    data.append("category_id", this.formParams.id);
                }
                data.append("name", this.name.toString());
                data.append("description", this.description.toString());
                data.append("action", action);


                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: { "Content-Type": "application/json" },
                })
                    .then(function (response) {
                        if (response.data) {
                            that.form = {
                                name: undefined,
                                description: undefined,
                            };
                            if (that.formParams.id){
                                toastr.success( that.addFormStrings.successMessage.categoryUpdated,'Success')
                            }else {
                                toastr.success(that.addFormStrings.successMessage.categoryCreated, 'Success')
                            }

                            that.$router.push({
                                path: "/categories",
                            });
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.addFormStrings.errors.errorOccurred);
                        }
                        that.submitting = false;
                    });
            }else{
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }
        },
         serialize (data) {
    let obj = {};
    for (let [key, value] of data) {
        if (obj[key] !== undefined) {
            if (!Array.isArray(obj[key])) {
                obj[key] = [obj[key]];
            }
            obj[key].push(value);
        } else {
            obj[key] = value;
        }
    }
    return obj;
}
    },
    template: `
 
          <form @submit.prevent="submitForm">
              <div class="mb-4">
                  <label for="name" class="form-label">{{addFormStrings.formFields.name.label}} <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="name" v-model="name">
                  <span class="text-danger validation-error" v-if="formErrors.name">{{formErrors.name}}</span>
              </div>
              <div class="mb-4">
                  <label for="description" class="form-label">{{addFormStrings.formFields.description.label}} <span class="text-danger">*</span></label>
                  <textarea class="form-control" id="description" v-model="description"></textarea>
                  <span class="text-danger validation-error" v-if="formErrors.description">{{formErrors.description}}</span>
              </div>
              <div class="mb-4">
                <button class="hs-btn hs-btn-primary hs-btn-signup" :disabled="submitting || Object.keys(this.formErrors).length >0">{{addFormStrings.formFields.button}} <i v-if="submitting" class="fa fa-spinner fa-pulse"></i></button>
              </div>
          </form>
        
`,
};
export { CategoryForm };
