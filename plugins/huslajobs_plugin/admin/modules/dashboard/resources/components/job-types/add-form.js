import {ReusableFunctions} from "../../../../../../js/functions.js";

const JobTypeForm = {
  props: {
    formParams: {
      type: Object,
      default: function () {
        return { id: undefined, name: undefined, description: undefined ,

        };
      },
    },
  },
  data() {
    return {
      name: this.formParams.name,
      description: this.formParams.description,
      formErrors: {},
      jobTypeCreated: false,
      submitting: false,
      addFormStrings: VueUiStrings.adminBackend.jobTypes.addForm
    };
  },
  mixins:[ReusableFunctions]
    ,
  watch: {
    name: function (current) {
      this.validateInput("name", current, {required: true}, this.addFormStrings.formFields.name.label);
    },
    description: function (current) {
      this.validateInput("description", current, {required: true}, this.addFormStrings.formFields.description.label);
    },
  },
  methods: {

    submitForm() {
      this.submitting = true;
      //validate inputs
      this.validateInput("name", this.name, {required: true}, this.addFormStrings.formFields.name.label);
      this.validateInput("description", this.description, {required: true}, this.addFormStrings.formFields.description.label);

      if (Object.keys(this.formErrors).length === 0) {
        const data = new FormData();
        this.submitting = true;
        const that = this;
        let action = "save_job_types";
        if (this.formParams.id) {
          action = "update_job_type";
          data.append("job_type_id", this.formParams.id);
        }
        data.append("name", this.name);
        data.append("description", this.description);
        data.append("action", action);

        axios({
          method: "post",
          url: ajaxurl,
          data: data,
          headers: { "Content-Type": "multipart/form-data" },
        })
          .then(function (response) {
            if (response.data) {
              that.form = {
                name: undefined,
                description: undefined,
              };
              if (that.formParams.id){
                toastr.success( that.addFormStrings.successMessage.jobTypeUpdated,'Success')
              }else {
                toastr.success(that.addFormStrings.successMessage.jobTypeCreated, 'Success')
              }
              // that.packageCreated = true;
              // that.submitting = false;
              that.$router.push({
                path: "/job-types",
              });
            }
          })
          .catch(function (error) {
            console.log(error);
            if (error.response) {
              toastr.error(error.response.data.data);
            } else {
              toastr.error(that.addFormStrings.errors.errorOccurred);
            }
            that.submitting = false;
          });
      }            else{
        this.submitting = false
        const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
        // Scroll to first error element
        window.scrollTo(
            firstErrorControl?.offsetLeft || 0,
            (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
        )
      }
    },
  },
  template: `
 
          <form @submit.prevent="submitForm">
              <div class="mb-4">
                  <label for="name" class="form-label">{{addFormStrings.formFields.name.label}} <span class="text-danger">*</span></label>
                  <input type="text" class="form-control" id="name" v-model="name">
                  <span class="text-danger validation-error" v-if="formErrors.name">{{formErrors.name}}</span>
              </div>
              <div class="mb-4">
                  <label for="description" class="form-label">{{addFormStrings.formFields.description.label}} <span class="text-danger">*</span></label>
                  <textarea class="form-control" id="description" v-model="description"></textarea>
                  <span class="text-danger validation-error" v-if="formErrors.description">{{formErrors.description}}</span>
              </div>
              <div class="mb-4">
                <button class="hs-btn hs-btn-primary hs-btn-signup" :disabled="submitting || Object.keys(this.formErrors).length>0">{{addFormStrings.formFields.button}} <i v-if="submitting" class="fa fa-spinner fa-pulse"></i></button>
              </div>
          </form>
        
`,
};
export { JobTypeForm };
