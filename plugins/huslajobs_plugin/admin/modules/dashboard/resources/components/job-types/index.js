import {LoadingBar} from "../LoadingBar.js";
import {LoadingError} from "../LoadingError.js";
import {Pagination} from "../Pagination.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const JobTypes = {
    data() {
        return {
            jobTypes: undefined,
            page: 1,
            perPage: jobs_per_page,
            loading: true,
            pages: 0,
            error: false,
            deleting: false,
            sortField: 'id',
            sort: 'desc',
            searchText:'',
            searchFields: "name,description",
            jobTypeIndexStrings: VueUiStrings.adminBackend.jobTypes.index
        };
    },
    components: {LoadingBar, LoadingError, Pagination},
    mounted() {
        //todo get all
        this.getAllJobTypes();
    },
    watch: {
        searchText: function (current) {
            if (current === ''){
                this.search();
            }
        },
    },
    mixins:[ReusableFunctions],
    methods: {
        search() {
            this.page = 1;
            this.sortField = 'id';
            this.sort = 'desc';
            this.getAllJobTypes();
        },
        sortBy(field) {
            if (this.sortField == field) {
                this.sort = this.sort == 'desc' ? 'asc' : 'desc'
            } else {
                this.sort = 'asc';
                this.sortField = field;
            }
            this.page = 1;
            this.getAllJobTypes();
        },
        isSortedBy(field, sort = 'asc') {
            switch (sort) {
                case 'asc':
                    return this.sortField == field && sort == this.sort;
                case 'desc':
                    return this.sortField == field && sort == this.sort;
                default:
                    return true;
            }
        },
        getAllJobTypes() {
            this.loading = true;
            const data = new FormData();

            data.append("action", "get_job_types");
            data.append("page", this.page);
            data.append("perPage", this.perPage);
            data.append("sortBy", this.sortField);
            data.append("order", this.sort);
            data.append("searchText", this.searchText);
           // data.append("searchFields", `${this.searchFields}`);
            data.append("searchFields",this.searchFields);



            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data:data,
              headers: { "Content-Type": "multipart/form-data" },

            })
                .then(function (response) {
                    that.jobTypes = response.data.data;
                    that.pages = response.data.totalPages;
                    that.loading = false;
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.jobTypeIndexStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.error = true;
                });
        },
        deleteJobType(jobType) {
            const confirm_delete = confirm(this.jobTypeIndexStrings.confirm+" " + jobType.name + "?");
            if (confirm_delete) {
                this.loading = true;
                const data = new FormData();
                data.append("action", "delete_job_type");
                data.append("job_type_id", jobType.id);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        toastr.success(jobType.name + " " + that.jobTypeIndexStrings.deleted);
                        that.getAllJobTypes();
                        // that.loading = false;
                    })
                    .catch(function (error) {
                        console.log(error);
                        that.loading = false;

                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.jobTypeIndexStrings.errors.errorOccurred);
                        }
                    });
            }
        },
        goToPage(page) {
            this.page = page
            this.getAllJobTypes();
        }
    },

    template: `<div class="module-content-wrapper">
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <router-link to="/">{{jobTypeIndexStrings.dashboard}}</router-link>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{jobTypeIndexStrings.jobTypes}}</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">{{jobTypeIndexStrings.jobTypes}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="getAllJobTypes"></loading-error>
                        </div>
                        <div v-else>
                        <div class="d-flex justify-content-between align-items-center mb-4">
                            <form class="form-inline mt-2">
                                <div class="form-group mb-2">
                                    <input type="text" class="form-control"
                                           :placeholder="jobTypeIndexStrings.formFields.searchText.placeholder" v-model="searchText">
                                </div>
                                <button type="submit" class="hs-btn hs-btn-primary ml-2 mb-2" @click="search()">
                                {{jobTypeIndexStrings.formFields.button}}
                                  
                                </button>
                            </form>
                            <router-link to="/job-types/create" class="hs-btn hs-btn-primary">{{jobTypeIndexStrings.addJobType}}
                            </router-link>
                        
                        </div>
                            
                            <div v-if="jobTypes?.length > 0" class="hs-table-wrapper">
                                <table class="table table-striped">
                                    <tr>
                                        <th @click="sortBy('id')" style="cursor:pointer">
                                           {{ jobTypeIndexStrings.tableHeaders.id}}
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('id','desc'),'fa-sort-up':isSortedBy('id','asc'),'fa-sort':isSortedBy('id','normal')}"></i>
                                        </th>
                                        <th @click="sortBy('name')" style="cursor:pointer">
                                            {{jobTypeIndexStrings.tableHeaders.name}}
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('name','desc'),'fa-sort-up':isSortedBy('name','asc'),'fa-sort':isSortedBy('name','normal')}"></i>
                                        </th>
                                        <th>{{jobTypeIndexStrings.tableHeaders.description}}</th>
                                        <th>actions</th>
                                    </tr>
                                    <tr v-for="jobType of jobTypes">
                                        <td>{{jobType.id}}</td>
                                        <td>{{decodeHtml(jobType.name)}}</td>
                                        <td>{{decodeHtml(jobType.description)}}</td>
                                        <td>
                                            <router-link :to="'/job-types/'+jobType.id+'/edit'"
                                                         class="btn mr-2 mb-2 btn-primary btn-sm"><i
                                                    class="fa fa-pencil-alt fa-2x"></i>
                                            </router-link>
                                            <button class="btn btn-danger btn-sm mb-2" @click="deleteJobType(jobType)"><i
                                                    class="fa fa-trash fa-2x"></i></button>
                                        </td>
                                    </tr>
                                </table>

                            </div>

                            <div v-else-if="jobTypes?.length == 0">
                                <p>{{jobTypeIndexStrings.noJobTypes}}</p>
                            </div>
                                                                                        <pagination v-if="pages > 1" :page="page" :pages="pages"
                                            @update="goToPage"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
};
export {JobTypes};
