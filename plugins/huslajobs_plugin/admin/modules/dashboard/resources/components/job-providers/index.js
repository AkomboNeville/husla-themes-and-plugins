import {Accounts} from "../accounts/accounts.js";

const Index = {
    components: {Accounts},
    template: `<accounts type="recruiter"></accounts>`
}

export {Index};