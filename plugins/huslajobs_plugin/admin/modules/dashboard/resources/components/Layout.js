const Layout = {
    data() {
        return {
            count: 0,
            layoutStrings: VueUiStrings.adminBackend.layout.menuOptions
        };
    },
    methods: {
        isCurrentPage(page) {
            const thisPage = this.$route.path
            return thisPage.includes(page)
        }
    },
    template: `<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu" style="position:absolute; top: 20px;">
    <div class="sidebar-content mm-active">
        <!--- Sidemenu -->
        <div class="slimScrollDiv mm-show" style="position: relative; overflow: hidden; width: auto; height: 418px;">
            <div id="sidebar-menu" class="slimscroll-menu mm-active"
                 style="overflow: hidden; width: auto;">
                <ul class="metismenu mm-show" id="menu-bar">
                    <li class="menu-title">Navigation</li>

                    <li>
                        <router-link to="/">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>{{layoutStrings.dashboard.toString()}}</span>
                        </router-link>
                    </li>
                    <hr/>
                    <li>
                        <router-link to="/job-types" :class="{'active':isCurrentPage('job-types')}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>{{layoutStrings.jobTypes.toString()}}</span>
                        </router-link>
                    </li>
                    <li>
                        <router-link to="/categories" :class="{'active':isCurrentPage('categories')}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>{{layoutStrings.categories.toString()}}</span>
                        </router-link>
                    </li>
                    <li>
                        <router-link to="/jobs" :class="{'active':isCurrentPage('jobs')}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>{{layoutStrings.jobs.toString()}}</span>
                        </router-link>
                    </li>

                    <li>
                        <router-link to="/job-applications" :class="{'active':isCurrentPage('job-applications')}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>{{layoutStrings.jobApplications.toString()}}</span>
                        </router-link>
                    </li>
                    <hr/>
                    <li>
                        <router-link to="/providers" :class="{'active':isCurrentPage('providers')}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>{{layoutStrings.recruiters.toString()}}</span>
                        </router-link>
                    </li>

                    <li>
                        <router-link to="/seekers" :class="{'active':isCurrentPage('seekers')}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>{{layoutStrings.jobSeekers .toString()}}</span>
                        </router-link>
                    </li>
                    <hr/>
                    <li>
                        <router-link to="/packages" :class="{'active':isCurrentPage('packages')}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>{{layoutStrings.packages .toString()}}</span>
                        </router-link>
                    </li>
                    <li>
                        <router-link to="/subscriptions" :class="{'active':isCurrentPage('subscriptions')}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>{{layoutStrings.subscriptions .toString()}}</span>
                        </router-link>
                    </li>
                    <!--                    <li>-->
                    <!--                        <router-link to="/transactions" :class="{'active':isCurrentPage('transactions')}">-->
                    <!--                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"-->
                    <!--                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"-->
                    <!--                                 stroke-linejoin="round" class="feather feather-home">-->
                    <!--                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>-->
                    <!--                                <polyline points="9 22 9 12 15 12 15 22"></polyline>-->
                    <!--                            </svg>-->
                    <!--                            <span> Transactions </span>-->
                    <!--                        </router-link>-->
                    <!--                    </li>-->
                    <li>
                        <router-link to="/currencies" :class="{'active':isCurrentPage('currencies')}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span> {{layoutStrings.currencies .toString()}}</span>
                        </router-link>
                    </li>
                </ul>
            </div>
        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->
<div class="content-page" style="margin-top:20px;">
    <div class="content">
        <div class="container-fluid">
            <router-view></router-view>
        </div>
    </div>
</div>

`,
};

export {Layout};
