<?php

    namespace huslajobs;

    class HuslaModel {
        public int $id = 0;
        protected static string $where = '';
        protected static array $orderBys = [];
        protected static string $pagination = '';
        protected static string $join = '';
        protected static string $join_table = '';
        protected static int $per_page = 0;
        protected static int $limit_per_page = 0;
        protected static int $current_page = 1;
        protected static bool $open_conditions = false;

        public function __construct() {
            // do something here
        }

        /**
         * @since v1.0
         * Saves a new model in the database
         * Returns boolean|object
         */
        public function save(){
            global $wpdb;

            $model  = get_called_class();
            $fields = get_object_vars( $this );
            $result =false;
            $wpdb->show_errors = true;
            $this_migration    = HuslaMigration::getMigration( $model::$table_name, true );
            if ( $this->id == 0 ) { // we are creating
                if ( $this_migration && $this_migration->hasColumn( 'created_at' ) ) {
                    $fields['created_at'] = date( "Y-m-d H:i" );
                    $fields['updated_at'] = date( "Y-m-d H:i" );
                }
                $result = $wpdb->insert( $model::$table_name, $fields );
            } else { // we are updating
                if ( $this_migration && $this_migration->hasColumn( 'created_at' ) ) {
                    $fields['updated_at'] = date( "Y-m-d H:i" );
                }
                unset( $fields['id'] );
                $result = $wpdb->update( $model::$table_name, $fields, [ 'id' => $this->id ] );
            }
            if ( $result !== false ) {
                if ( $this_migration->hasColumn( 'id' ) ) {
                    $this->id = $wpdb->insert_id ?: $this->id;
                }
            }
            $wpdb->flush();
            return $result;
        }

        /**
         * @since v1.0
         * Finds a model in the database
         * Returns boolean|object
         */
        public static function find( int $id ): HuslaModel {
            return self::where( "id", "=", $id )->first();
        }

        /**
         * @since v1.0
         * Finds a model by name in the database
         * Returns boolean|object
         */
        public static function name( string $name ) {
            return self::where( 'name', 'like', "'" . $name . "'" )->get();
        }

        /**
         * @since v1.0
         * Hard delete a model from the database
         * Also does soft delete if table has deleted column
         */
        public function delete(): bool {
            global $wpdb;
            $model = get_called_class();

            $this_migration = HuslaMigration::getMigration( $model::$table_name, true );
            if ( $this_migration && $this_migration->hasColumn( 'deleted' ) ) {
                return self::softDelete();
            } else {
                // delete the package here
                return $wpdb->delete( $model::$table_name, array( 'id' => $this->id ) );
            }
        }

        /**
         * @since v1.0
         * Soft deletes a model from the database
         */
        public function softDelete(): bool {
            global $wpdb;
            $model = get_called_class();

            return $wpdb->update( $model::$table_name, [ 'deleted' => 1 ], [ 'id' => $this->id ] );
        }


        /**
         * @return array<HuslaModel>
         * @since v1.0
         * Gets all data in the database
         */
        public static function all(): array {
            return self::orderBy( 'id', 'desc' )->get();
        }

        public static function where( string $field, string $comparison, $value ): HuslaModel {
            $model       = get_called_class();

            if (self::$open_conditions){
                self::$where .=  " ". $model::$table_name . '.' . $field . " " . $comparison . " " . $value;
            }else{
                self::$where =  " WHERE ". $model::$table_name . '.' . $field . " " . $comparison . " " . $value;
            }
            return new static();
        }
        public static function addConditions($operator): HuslaModel {
            self::$where .= "  " . strtoupper($operator)." ( ";
            self::$open_conditions = true;
            return new static();
        }
        public static function closeConditions(): HuslaModel {
            self::$where .= "  )";
            self::$open_conditions = false;
            return new static();
    }
        public static function andWhere( string $field, string $comparison, $value ): HuslaModel {
            $model       = get_called_class();
            self::$where .= " AND " . $model::$table_name . '.' . $field . " " . $comparison . " " . $value;
            return new static();
        }

        public static function orWhere( string $field, string $comparison, $value ): HuslaModel {
            $model       = get_called_class();
            self::$where .= " OR " . $model::$table_name . '.' . $field . " " . $comparison . " " . $value;
            return new static();
        }

        public static function whereJoin( string $field, string $comparison, $value,$table ): HuslaModel {
            if (self::$open_conditions){
                self::$where .=  " ". $table. '.' . $field . " " . $comparison . " " . $value;
            }else{
                self::$where =  " WHERE ". $table . '.' . $field . " " . $comparison . " " . $value;
            }
            return new static();
        }
        public static function andWhereJoin( string $field, string $comparison, $value ,$table): HuslaModel {
            self::$where .= " AND " . $table . '.' . $field . " " . $comparison . " " . $value;
            return new static();
        }

        public static function orWhereJoin( string $field, string $comparison, $value ,$table): HuslaModel {
            self::$where .= " OR " . $table . '.' . $field . " " . $comparison . " " . $value;
            return new static();
        }

        public static function paginate( int $per_page = 1, int $current_page = 1 ,int $limit_per_page=0 ): HuslaModel {
            self::$per_page     = $per_page;
            self::$current_page = $current_page;
            self::$limit_per_page = $limit_per_page;

            return new static();
        }

        public static function orderBy( string $field, string $order ): HuslaModel {
            array_push( self::$orderBys, [ $field, $order ] );

            return new static();
        }

        public static function innerJoin( string $table_name ): HuslaModel {
            self::$join       .= ' INNER JOIN ' . HUSLA_TABLE_PREFIX . $table_name . ' ';
            self::$join_table = HUSLA_TABLE_PREFIX . $table_name;

            return new static();
        }

        public static function leftJoin( string $table_name ): HuslaModel {
            global $wpdb;
            $table= HUSLA_TABLE_PREFIX . $table_name ;
            $prefix = $wpdb->prefix ;
            if(strpos($table_name, $prefix) !== false) {
                $table = $table_name;
            }
            self::$join       .= ' LEFT JOIN ' . $table. ' ';
            self::$join_table = $table;
            return new static();
        }

        public static function rightJoin( string $table_name ): HuslaModel {
            self::$join       .= ' RIGHT JOIN ' . HUSLA_TABLE_PREFIX . $table_name . ' ';
            self::$join_table = HUSLA_TABLE_PREFIX . $table_name;

            return new static();
        }

        public static function on( string $field1, string $field2 ): HuslaModel {
            $model      = get_called_class();
            self::$join .= ( 'ON ' . $model::$table_name . '.' . $field1 . ' = ' . self::$join_table . '.' . $field2 );

            return new static();
        }

        /**
         * This functions joins two unrelated tables using joins
         * @param string $field1 the column you want to compare from a certain table
         * @param string $field2 the column you want to compare from a certain table
         * @param string $field1_table_name
         * @param $field2_table_name
         * @return HuslaModel
         */
        public static function onJoin(string $field1, string $field2 , string $field1_table_name, $field2_table_name): HuslaModel {
            self::$join .= ( 'ON ' . $field1_table_name. '.' . $field1 . ' = ' . $field2_table_name . '.' . $field2 );

            return new static();
        }

        private static function getResults( $query ) {
            global $wpdb;
            $model   = get_called_class();
            $results = $wpdb->get_results( $query );
            $data    = [];
            if ( $results ) {
                if ( trim( self::$join ) == '' ) {
                    foreach ( $results as $result ) {
                        $object = new $model();
                        foreach ( $result as $key => $value ) {
                            $object->$key = $value;
                        }
                        array_push( $data, $object );
                    }
                } else {  // if we have joins, we do not need to return the model since the structure will be different
                    $data = $results;
                }
            }

            return $data;
        }
        /**
         * @param array $fields  the fields to get. if empty, query will get everything
         * example
         * [Job::tableName().'.*',Currency::tableName().'.code',JobType::tableName().'.name AS job_type_name '],
         */
        public static function get( array $fields = [] ,bool $distinct = false) {

            global $wpdb;
            $model   = get_called_class();
            $db_name = $model::$table_name;
            $select  = $distinct ?  "SELECT DISTINCT * " : "SELECT * "; // set select all as the default
            if ( sizeof( $fields ) > 0 ) { // we want to get specific fields, not everything, eg only id, name
                $select = $distinct ?  'SELECT DISTINCT ' : 'SELECT ';
                foreach ( $fields as $field ) {
                    /* in case of joins, use 2.field_name to make reference to the secondary table in the join
                     example Package::rightJoin( 'package_limits' )->on( 'id', 'package_id' )->get( ['name','2.package_id as id'] ) will translate to
                     SELECT packages.name, package_limits.package_id as id FROM packages RIGHT JOIN package_limits ON packages.id=package_limits.package_id WHERE packages.deleted = 0
                    */

                    /**
                     * Since we can get the database table from the model
                     * every field passed to the get method should contain the table
                     * this will solve the problem of adding a number to the column name
                     */
                    $select .= $field. ', ';
                    /**
                     * below code will be deleted when the whole team accepts the new solution
                     */
//                    $select .= $field['table'] . '.' . $field['field'] . ', ';
//                    $is_secondary_table_in_field = strpos( $field, '2' );
//                    if ( $is_secondary_table_in_field === false ) {
//                        $select .= $db_name . '.' . $field . ', '; // reference primary table packages.name
//                    } else {
//                        $field  = str_replace( '2', self::$join_table, $field );
//                        $select .= $field . ', '; // reference secondary table package_limits.package_id
//                    }
                }
            }
            $select    = rtrim( $select, ', ' ); // removes the last comma (,) from the select statement
            $data      = [];
            $query     = $select . " FROM " . $db_name; // build the first section of the query eg Select * from table_name or select id,name from table_name
            $additions = self::$join; // if we have joins, we first add it to the next part of the query eg select * from table_name INNER JOIN ......
            $this_migration      = HuslaMigration::getMigration( $db_name, true ); // check if there's a deleted constraint in the query. if deleted is part of the where variable, we will skip the if statement below
            $is_deleted_in_where = strpos( self::$where, 'deleted' );

            if ( $this_migration && $this_migration->hasColumn( 'deleted' ) && $is_deleted_in_where === false ) {
                if ( trim( self::$where ) == '' ) {
                    self::where( 'deleted', '=', 0 );
                } else {
                    self::andWhere( 'deleted', '=', 0 ); // if the table has the deleted column, we should get the fields that have not been soft deleted by default
                }
            }
            $additions .= self::$where;

            if (sizeof(self::$orderBys)>0) {
                foreach (self::$orderBys as $key=> $order_by) { // ordering will be the last section of the query
                    if ($key == 0){
                        $additions .= " ORDER BY " . $db_name.'.'. $order_by[0] . " " . $order_by[1];
                    }else{
                        $additions .= " , " . $db_name.'.'. $order_by[0] . " " . $order_by[1];
                    }

                }
            }

            if ( self::$per_page > 0 || self::$per_page == - 1 ) { // check if the query requires pagination
                $total_query = 'SELECT COUNT(*) as total FROM ' . $db_name . $additions;
                if ($distinct){
                    $total_query = ' SELECT COUNT(DISTINCT ' .$db_name.'.'. 'id' . ') as total FROM ' . $db_name . $additions;
                }
                $total       = intval( $wpdb->get_var( $total_query ) );
                $query       .= $additions;

                // prevent calculating offset for negative one
                // negative one was used to show all results in get all requests
                // we could have not used negative one since the else will return everything, but the structure of $data will not be the same

                self::$per_page = self::$per_page == - 1 ? $total : self::$per_page;
                $offset         = ( self::$current_page * self::$per_page ) - self::$per_page;
                $limit = self::$limit_per_page ?: self::$per_page;
                $query          .= " LIMIT " . $offset . ' , ' . $limit;


                $data           = self::getResults( $query );
                $total_pages    = $total ? $total / self::$per_page : 0;
                $total_pages    = $total_pages > round( $total_pages ) ? round( $total_pages ) + 1 : round( $total_pages );
                $data           = [
                    'data'       => $data,
                    'page'       => self::$current_page,
                    'totalPages' => $total_pages,
                    'perPage'    => self::$per_page,
                    'totalItems' => $total
                ];

            } else { // query does not require pagination
                $query .= $additions;
                $data  = self::getResults( $query );
            }


            self::$where       = '';
            self::$orderBys     = [];
            self::$pagination   = '';
            self::$join         = '';
            self::$join_table   = '';
            self::$per_page     = 0;
            self::$limit_per_page = 0;
            self::$current_page = 1;
            $wpdb->flush();
            return $data;
        }

        public static function first() {
            $data = self::get();
            if (sizeof($data)){
                return $data[0];
            }
            return  null;

        }

        public static function lastItem(): HuslaModel {
            return self::orderBy( 'id', 'desc' )->first();
        }
    }
