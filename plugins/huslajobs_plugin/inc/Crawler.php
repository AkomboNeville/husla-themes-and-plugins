<?php

namespace huslajobs;

use WP_Error;
use WP_REST_Server;

class Crawler
{

    protected string $namespace = 'huslajobs/v2';
    protected string $url = 'crawler/jobs';
    protected string $useremail = 'huslacrawler@bolocenter.com';
    protected string $apiKey = '237b0lOCE]]NT@4Ever';

    public function __construct()
    {
        $this->addActions();
    }

    /**
     * @since v1.0
     * Adds actions
     */
    public function addActions(): void
    {
        add_action('rest_api_init', [$this, 'restEndpoint'], 40);
    }

    public function restEndpoint()
    {
        register_rest_route($this->namespace, '/' . $this->url, [
            [
                'methods' => WP_REST_Server::CREATABLE,
                'callback' => [$this, 'saveJobs'],
                'permission_callback' => [$this, 'getPermissionCallback']
            ],
        ]);

    }

    public function saveJobs($request)
    {

        if (!$_SERVER['PHP_AUTH_USER'] && !$_SERVER['PHP_AUTH_PW'] ){
            return new WP_Error( 'rest_forbidden', __( 'Sorry, you are not allowed to make this requests.' ), ['status' => rest_authorization_required_code()]);
        }
        if (!($_SERVER['PHP_AUTH_USER'] == $this->useremail && $_SERVER['PHP_AUTH_PW'] == $this->apiKey) ){
            return new WP_Error( 'rest_forbidden', __( 'Sorry, you are not allowed to make this requests.' ), ['status' => rest_authorization_required_code()]);
        }

        $crawler_user = get_user_by('email',$this->useremail);

        if ($crawler_user){
            $account = Account::where('wp_user_id','=',$crawler_user->ID)->get();
            $account = isset($account) && sizeof($account) ? $account[0]: null;
            if ($account){
                $jobs = Job::all();
                $request_body = $request->get_json_params();
                if (sizeof($request_body)) {
                    foreach ($request_body as $job_data) {
                        $current_job = null;
                        if (sizeof($jobs)) {
                            $current_job = Job::where('crawled_job_url', '=', "'" . $job_data['crawled_job_url'] . "'")->get();
                            $current_job = isset($current_job) && sizeof($current_job) ? $current_job[0] : null;
                        }
                        $url = $job_data['crawled_job_url'];
                        $slug = explode('/', $url);
                        if (!$current_job) {
                            $job = new Job();
                            $job->description = stripslashes(sanitize_text_field(esc_html($job_data['description'])));
                            $job->name = stripslashes(sanitize_text_field(esc_html($job_data['name'])));
                            $job->work = $job_data['work'];
                            $job->crawled_job_url = $job_data['crawled_job_url'];
                            $job->crawled_categories = $job_data['crawled_categories'];
                            $job->crawled_job_types = $job_data['crawled_job_types'];
                            $job->crawled_job_location = $job_data['crawled_job_location'];
                            $job->account_id = $account->id;
                            $job->crawled_job_summary = $job_data['crawled_job_summary'];
                            $job->slug = array_pop($slug);
                            $job->save();
                        }
                    }
                    return ['success'=>true,'Message'=> 'Jobs data inserted successfully'];
                }else{
                    return new WP_Error( 'empty_array', __( 'Empty data array' ), array( 'status' => rest_authorization_required_code() ) );
                }


            }else{
                return new WP_Error( 'unknown_account', __( 'Account not found,please contact site admin' ), array( 'status' => rest_authorization_required_code() ) );

            }

        }else{
            return new WP_Error( 'unknown_user', __( 'User not found' ), array( 'status' => rest_authorization_required_code() ) );

        }

    }


    /**
     * Check if method is post
     * check if authorization is set
     *
     * @since 1.0
     */
    function getPermissionCallback($request) {
        if (WP_REST_Server::CREATABLE !== $request->get_method()) {
            return false;
        }
        wp_populate_basic_auth_from_authorization_header();
        return true;
    }
}