const VLoader = {
    props: {
        size: {
            type: String,
            default: 'large',
        },
        card: {
            type: String,
            default: 'smooth',
        },
        active: {
            type: Boolean,
            default: false,
        },
        grey: {
            type: Boolean,
            default: false,
        },
        translucent: {
            type: Boolean,
            default: false,
        },
    },
    data() {
        return {
            imageUrl :THEME_URL
        }
    },
    template:
        `
            <div class="has-loader" :class="[active && 'has-loader-active']">
<!--    <div-->
<!--        v-if="active"-->
<!--        class="v-loader-wrapper is-active"-->
<!--    :class="[-->
<!--    grey && 'is-grey',-->
<!--    translucent && 'is-translucent',-->
<!--    card === 'regular' && 's-card',-->
<!--    card === 'smooth' && 'r-card',-->
<!--    card === 'rounded' && 'l-card',-->
<!--    ]"-->
<!--    >-->
<!--    <div class="loader is-loading is-large"></div>-->
<!--</div>-->
    <div v-if="active" class="v-loader-wrapper" >
        <div class="loading-content">
            <img :src="imageUrl+'/logo.png'" style="animation-name: km-img-loading;
          animation-duration: 2s;
          animation-iteration-count: infinite;"/>
        </div>
    </div>

<slot></slot>
</div>
    `
}


export {VLoader}