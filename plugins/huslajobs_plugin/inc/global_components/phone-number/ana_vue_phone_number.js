const AnaVuePhoneNumber = {
    props: {
        modelValue: {
            required: true,
        },
        countryCode:{
            required: true
        },
    },
    emits: ["update:modelValue",'phoneError'],
    data() {
        return {
            phoneSetUp: undefined
        }
    },
    watch: {
        countryCode: function (currentVal, oldVal) {
            if (currentVal){
                this.setCountry(currentVal);
            }else{
                this.setCountry("cm");
            }
        }
    },
    refs: ['phoneNumber'],
    methods: {
        init() {
            this.phoneSetUp = intlTelInput(this.$refs.phoneNumber, {
                initialCountry: this.countryCode,
                preferredCountries: ["cm"],
                utilsScript: THEME_JS_URL + "/intl-tel-utils.js",
                nationalMode: true
            });

            //set default country
            if (this.modelValue){
                const country_code = this.phoneSetUp.selectedCountryData.iso2
                this.setCountry(country_code);
            }
        },
        onInput() {
            const formattedNumber = this.phoneSetUp.getNumber();
            this.$emit("update:modelValue", formattedNumber);
            if (formattedNumber){
                this.$emit("phoneError",this.phoneSetUp.isValidNumber());
            }


        },
        setCountry(countryCode){
            this.phoneSetUp.setCountry(countryCode);
        }
        // getIp(callback) {
        //     axios({
        //         method: "get",
        //         "url": "https://ip2c.org/s",
        //     }).then((res) => {
        //         let country_code = 'cm'
        //         if (res.status === 200) {
        //             country_code = res.data.split(";")[1].toLowerCase();
        //         }
        //         return country_code
        //     }).catch(() => {
        //         return 'cm'
        //     }).then((res) => callback(res))
        // }
    },
    mounted() {
        this.init()

    },
    template:`
        <div>
        <input
            ref="phoneNumber"
            type="tel"
            name="phone_number"
        :modelValue="modelValue"
        @change="onInput"
        class="form-control"
        :value="modelValue"
        >
    </div>
    
    `
}
export {AnaVuePhoneNumber}


