/**
 * A simple Vuejs tooltip component
 * Author: Akombo Neville Akwo
 *
 */

const AnaToolTip = {
    props: {
        message: {
            required: true,
        },
    },
    template: `
        <div class="hs-tip-holder">
            <div class="hs-tip-arrow-inner hs-tip-arrow"></div>
            <div class="hs-tip-content">{{message}}</div>
        </div>      
`
}
export {AnaToolTip}