// VUE 3 initialization
import {AnaVueLanguageSelect} from "./ana-vue-language-select.js";
const LanguageSwitcher ={
    data(){
        return{
            imgUrl:THEME_URL
        }
    },
    methods:{
        switchLanguage(locale){
            console.log('locale');
            console.log(locale)
            const data = new FormData();
            data.append('action','hs_switch_language')
            data.append('locale',locale)

            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: { "Content-Type": "multipart/form-data" },
            })
                .then(function (response) {
                    console.log(response)

                })
                .catch(function (error) {
                    console.log(error)
                    // if (error?.response) {
                    //     toastr.error(error?.response?.data?.data);
                    // } else {
                    //     toastr.error("An error occurred");
                    // }
                    // that.submitting = false;
                });
        }
    },
    template:`<a href="" className="mr-2" @click.prevent="switchLanguage('en_GB')">
    <img :src="imgUrl+'/en_GB.png'" className="mr-1"
         alt="country flag">
</a>
<a href="" @click.prevent="switchLanguage('fr_FR')">
    <img :src="imgUrl+'/fr_FR.png'" className="mr-1"
         alt="country flag">

</a>
    `
}


const app = Vue.createApp({
    render() {
        return Vue.h(LanguageSwitcher, {});
    },
});
// app.use(router);
// app.mount("#hs-language-select");