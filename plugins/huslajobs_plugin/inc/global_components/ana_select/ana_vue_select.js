/**
 * A simple Vuejs select/multiselect component
 * Author: Akombo Neville Akwo
 *
 */

const AnaVueSelect = {
    emits: ['search-change', 'update:modelValue', 'open'],
    props: {
        value: {
            required: false,
        },
        modelValue: {
            required: true,
        },
        /**
         * options: an array of objects [{label:'Akombo Neville',value:'Akombo Neville'},]
         * label will be the option text and value the option value
         */
        options: {
            type: [Array, Object, Function],
            required: false,
            default: () => ([])
        },
        trackBy: {
            type: String,
            required: false,
            default: 'label',
        },
        placeholder: {
            type: String,
            required: false,
            default: null,
        },
        noOptionsText: {
            type: String,
            required: false,
            default: 'The list is empty',
        },
        noResultsText: {
            type: String,
            required: false,
            default: 'No results found',
        },
        fieldName: {
            type: String,
            required: false,
            default: '',
        },
        disabled: {
            type: Boolean,
            required: false,
            default: false,
        },
        searchable: {
            type: Boolean,
            required: false,
            default: true,
        },
        ajaxSearch: {
            type: Boolean,
            required: false,
            default: false,
        },
        mode: {
            type: String,
            required: false,
            default: 'single', // single|multiple|tags
        },
    },
    data() {
        return {
            search: '',
            active: false,
            selectedOption: undefined,
            searchText: '',
            clearSelect:false,
            selectOptions: this.options,
            inputPlaceholder: this.placeholder ?? undefined

        };
    },
    refs: ['searchInput'],
    methods: {
        showInput() {
            this.$refs.searchInput.focus();
            if (!this.options.length) {
                this.inputPlaceholder = "Type and select " + this.fieldName
            }
        },
        updateModel(option) {
            if (this.mode === 'single') {
                this.active = false;
                this.selectedOption = option.label ?? option;
                this.$emit('update:modelValue', option.value ?? option);
                this.$refs.searchInput.blur()
            } else {
                if (this.selectedOption?.length) {
                    if (this.selectedOption.indexOf(option.label) !== -1) {
                        this.selectedOption = this.selectedOption.filter((opt) => opt.toLowerCase() !== option.label.toLowerCase())
                    } else {
                        this.selectedOption.push(option.label);
                    }
                    //
                    let modelVal = this.modelValue;

                    if (modelVal.indexOf(option.value) !== -1) {
                        modelVal = modelVal.filter((opt) => opt !== option.value)
                    } else {
                        modelVal.push(option.value);
                    }
                    this.$emit('update:modelValue', modelVal);
                } else {
                    const modelVal = [];
                    this.selectedOption = []
                    this.selectedOption.push(option.label);
                    modelVal.push(option.value);
                    this.$emit('update:modelValue', modelVal);
                }
                this.$refs.searchInput.focus()
            }
            this.searchText = '';
        },
        deactivate(event) {
            let el = this.$refs.anaVueSelect;
            let target = event.target;

            if (el && (el !== target) && !el.contains(target) && !target.classList.contains('clear-option')) {
                this.active = false
                setTimeout(() => {
                    if (!this.active) {
                        // close()
                        this.searchText = '';
                        if (!this.selectedOption && this.selectedOption !== undefined) {
                            this.$emit('update:modelValue', undefined);
                        }
                    }
                }, 1)
            }


        },
        clear(clearItem = '') {
            if (this.mode === 'single'){
                this.$emit('update:modelValue', '');
                this.selectedOption = '';
                this.searchText = '';
                this.clearSelect = true;
                this.active = false;
                this.$refs.searchInput.blur();
            }else if(this.mode === 'multiple' && clearItem !=''){
                const options = this.options;
                this.selectedOption = this.selectedOption.filter((opt) => opt.toLowerCase() !== clearItem.toLowerCase());
                for (const option of options) {
                    if (option.label.toLowerCase() === clearItem.toLowerCase()) {
                        let modelVal = this.modelValue;
                        if (modelVal.indexOf(option.value) !== -1) {
                            modelVal = modelVal.filter((opt) => opt !== option.value)
                        }
                        this.$emit('update:modelValue', modelVal);
                        break;
                    }
                }

                if (this.selectedOption?.length === 0){
                    this.clearSelect = true;
                    this.active = false;
                    this.$refs.searchInput.blur();
                }else{
                }

            }

        },
        activate() {
            if (!this.disabled && !this.clearSelect) {
                this.active = true;
                this.showInput();
                this.$emit('open')
            }
            this.clearSelect = false;
        },
        decodeHtml(html){
            var txt = document.createElement("textarea");
            txt.innerHTML =html;
            return txt.value;
        }
    },
    watch: {
        searchText: function (currentVal, oldVal) {

            if (this.options.length && this.ajaxSearch) {
                this.$emit('search-change', currentVal);
            } else if (this.options.length && !this.ajaxSearch) {
                const options = this.options;
                if (currentVal) {
                    this.selectOptions = options.filter((option) => option.label.toLowerCase().includes(currentVal.toLowerCase()));
                } else {
                    this.selectOptions = this.options;
                }
            }

        },
        modelValue: function (currentVal, oldVal) {
            if (currentVal === undefined) {
                this.selectedOption = undefined;
            } else if (currentVal && !this.selectedOption && this.mode === 'single') {
                const options = this.options;
                this.selectedOption = options.filter((option) => option.value == this.modelValue)[0].label;
            }else if(Array.isArray(currentVal) && !this.selectedOption && this.mode === 'multiple'){
                this.selectedOption = [];
                const options = this.options;
                for (const option of options) {
                    if (currentVal.indexOf(option.value) !== -1) {
                        this.selectedOption.push(option.label)
                    }
                }
            }
        },
        options: function (currentVal, oldVal) {
            this.selectOptions = currentVal;
        }
    },

    created() {
        document.addEventListener('click', this.deactivate)
    },
    destroyed() {
        // important to clean up!!
        document.removeEventListener('click', this.deactivate)
    },
    mounted() {
        if (this.modelValue && !this.selectedOption) {
            const options = this.options;
            if (this.mode === 'single') {
                this.selectedOption = options.filter((option) => option.value == this.modelValue)[0].label;
            } else if (Array.isArray(this.modelValue) && this.mode === 'multiple') {
                this.selectedOption = [];
                for (const option of options) {
                    if (this.modelValue.indexOf(option.value) !== -1) {
                        this.selectedOption.push(option.label)
                    }
                }
            }
        }
    },
    template: `
        <div
         
        ref="anaVueSelect"
        class="ana-custom-select custom-select-container p-0 position-relative"
        :class="{'not-allowed':disabled}"  
        :disabled="disabled"
         @click="activate"
    >
  
                <!-- single select   -->
                <div v-if="mode==='single'" class="selected ana-single-select position-relative w-100" :disabled="disabled">
                    <input type="text" v-model="searchText" :placeholder="inputPlaceholder" :disabled="disabled" class="form-control pr-6 position-absolute" :class="{'is-hidden':selectedOption}" ref="searchInput">
                    <p class="form-control selected-text pr-6 mb-0" :class="{'opacity-0': active &&( searchText || !selectedOption)}" >
                         <span v-if="selectedOption" class="d-flex align-items-center">
                       
                            <span class="d-inline-block w-100 overflow-hidden">{{decodeHtml(selectedOption)}}</span>
                         <i class="fas fa-times ml-3 cursor-pointer clear-option"  @click="clear"></i>
                         </span>
                        <span v-else class="opacity-0">{{inputPlaceholder}}{{selectedOption}}</span>
                    </p>
                    
                    <div class="selected-icons position-absolute top-50 end-0 translate-middle-y">
                        <i v-if="active" class="fas fa-chevron-up pointer-event-none"></i>
                        <i v-else class="fas fa-chevron-down pointer-event-none"></i>
                        
                     </div>
                </div>
                
                 <!--       multiple select         -->
                 <div v-if="mode==='multiple'" class="selected position-relative ana-multi-select p-0"  :disabled="disabled">
                    <div class="d-flex pr-6 flex-wrap align-items-center">
                  
                    <p v-if="selectedOption" class="selected-text mb-0 mx-2 ">
                        <span v-for="(option,index) in selectedOption" :key="index" class="badge badge-info mr-2">{{decodeHtml(option)}} <i class="fas fa-times ml-4 cursor-pointer clear-option"  @click="clear(option)"></i></span> 
                    </p>
                    <input type="text" v-model="searchText" :placeholder="inputPlaceholder" class="border-0" :disabled="disabled"  ref="searchInput" >
                    </div>
                    
                    <div class="selected-icons position-absolute top-50 end-0 translate-middle-y">
                        <i v-if="active" class="fas fa-chevron-up pointer-event-none"></i>
                        <i v-else class="fas fa-chevron-down pointer-event-none"></i>
                     </div>
                </div>
                
            <!--      options          -->
<!--            <div class="ana-content-wrapper"> -->
<!--                <div class="h-100"></div>   -->
                <div  class="options-container" :class="active?'':'is-hidden'" tabindex="-1" >
                <ul class="options m-0 p-0" v-if="selectOptions.length">
                    <li v-for="(option,key) in selectOptions" class="option m-0 p-2" :key="key" :class="{'active':selectedOption==option.label || modelValue===option.value || (selectedOption?.length && selectedOption.indexOf(option.label) !== -1)}" value="option.value" @click.stop.prevent="updateModel(option)">{{decodeHtml(option.label)}}</li>
                </ul>
                <div v-else>
                    
                    <p v-if="!searchText">{{noOptionsText}}</p>
                    
                    <p v-else>
                        <span v-if="options.length">{{noResultsText}}</span>
                        <ul class="options m-0 p-0" v-else>
                            <li class="option m-0 p-1 active"  value="option.value" @click.stop.prevent="updateModel(searchText)">{{decodeHtml(searchText)}}</li>
                        </ul>
                    </p>
                </div>
            </div>
<!--            </div>-->
        </div>
        
`
}
export {AnaVueSelect}