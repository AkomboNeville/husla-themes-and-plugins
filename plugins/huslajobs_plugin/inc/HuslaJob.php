<?php

namespace huslajobs;

use huslajobs_admin\HuslaAdmin;
use huslajobs_client\HuslaClient;

class HuslaJob {
	private static float $version = 1.0;

	public function __construct() {
		// do something here
	}

	/**
	 * @since v1.0
	 * Returns the version number of the plugin
	 */
	public static function getVersion(): float {
		return self::$version;
	}

	/**
	 * @since  v1.0
	 * Initializes the admin and client side section
	 */
	public function init(): void {
		// new Package();
		new HuslaAdmin();
		new HuslaClient();
        new WooPayment();
        new Crawler();
	}

	/**
	 * @since v1.0
	 * Starts the plugin
	 */
	public function run(): void {
		// runs the plugin
		$this->init();
	}
}