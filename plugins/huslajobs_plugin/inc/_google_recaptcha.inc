<?php

namespace huslajobs;
/**
 * Verifies whether or not the recaptcha token is valid.
 *
 * @param $captcha The captcha to be verified. Gets from post data if not submitted as a parameter
 * @return bool True if valid and false otherwise
 * @throws Exception If captcha is not passed or submitted as a post parameter
 */
function verify_captcha($captcha): bool
{


    if(!$captcha){
        throw new Exception(__('Recaptcha verification failed. Are you human?', 'husla'));
    }
    $secret_key = HUSLASJOBS_RECAPTCHA_SECRETE_KEY ?? '';

    // post request to server
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data = ['secret' => $secret_key, 'response' => $captcha];

    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );


    $context  = stream_context_create($options);
    $response = file_get_contents($url, false, $context);
    $response_keys = json_decode($response,true);
    if($response_keys["success"]) {
        return true;
    } else {
        return false;
    }
}
