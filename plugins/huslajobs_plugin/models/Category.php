<?php

namespace huslajobs;

class Category extends HuslaModel
{

	protected static string $table_name = HUSLA_TABLE_PREFIX . 'categories';

	 public function __construct() {
	 	parent::__construct();
	 }

    /**
     * @return array <JobCategory>
     */
     public function jobs():array{
         return    JobCategory::where('category_id', '=' , $this->id)->get();
     }

    /**
     * @return array <Profile>
     */
     public function profiles():array{
         return Profile::where('category_id','=',$this->id)->get();
     }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }
}
