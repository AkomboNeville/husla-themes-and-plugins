<?php

namespace huslajobs;

class ProfileCategory extends HuslaModel {
	protected static string $table_name = HUSLA_TABLE_PREFIX . 'profile_categories';

	public function __construct() {
		parent::__construct();
	}
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }
}