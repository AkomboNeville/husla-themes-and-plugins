<?php


namespace huslajobs;

class Currency extends HuslaModel
{
    protected static string $table_name = HUSLA_TABLE_PREFIX . 'currencies';

    public function __construct() {
        parent::__construct();
    }
    /**
     * @return array<Package>
     */
    public function packages(): array
    {
        return Package::where('currency_id', '=', $this->id)->get();
    }

    /**
     * @return array<Job>
     */
    public function jobs(): array
    {
           return Job::where('currency_id', '=', $this->id)->get();
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }

}