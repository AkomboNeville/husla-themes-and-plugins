<?php

namespace huslajobs;

class WpUser extends HuslaModel {

	protected static string $table_name = 'wp_users';

	public function __construct() {
		parent::__construct();
	}


    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }
}