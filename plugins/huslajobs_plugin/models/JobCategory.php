<?php

namespace huslajobs;

class JobCategory extends HuslaModel
{
    protected static string $table_name = HUSLA_TABLE_PREFIX . 'job_categories';

    public function __construct() {
        parent::__construct();
    }
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }
}