<?php

namespace huslajobs;

class JobApplication extends HuslaModel {

	protected static string $table_name = HUSLA_TABLE_PREFIX . 'job_applications';

	public function __construct() {
		parent::__construct();
	}

	public function account(): Account {
		$account = Account::where( 'id', '=', $this->account_id )->first();

		return $account;
	}

	public function job(): Job {
		$job = Job::where( 'id', '=', $this->job_id )->first();

		return $job;
	}
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }
}