<?php

namespace huslajobs;

class Package extends HuslaModel {
	protected static string $table_name = HUSLA_TABLE_PREFIX . 'packages';

    public function __construct() {
        parent::__construct();
    }
	// overriding parent delete method
	public function delete(): bool {
		foreach ( $this->subscriptions() as $subscription ) {
			if ( ! $subscription->delete() ) {
				return false;
			}
		}
		foreach ( $this->limits() as $limit ) {
			if ( ! $limit->delete() ) {
				return false;
			}
		}

		return parent::delete();
	}

	public function currency(): HuslaModel {
		return Currency::where( 'id', '=', $this->currency_id )->first();
	}

	/**
	 * @return array<PackageLimit>
	 */
	public function limits(): array {
		$limits = PackageLimit::where( 'package_id', '=', $this->id )->get();

		return $limits;
	}

	/**
	 * @return array<Subscription>
	 */
	public function subscriptions(): array {
		$subscriptions = Subscription::where( 'package_id', '=', $this->id )->where( 'end_date', '<', 'now()' )->get();

		return $subscriptions;
	}

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }
}
