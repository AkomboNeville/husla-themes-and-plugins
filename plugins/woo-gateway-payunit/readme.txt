=== Woocommerce PayUnit Gateway ===
Contributors: nevilleakwo
Tags: payunit,credit card, paypal, momo, orange money, woocommerce
Requires at least: 5.6
Tested up to: 6.0
Requires PHP: ^5.6
Stable tag: 1.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Take mtn momo ,orange money, visa and paypal payments on your store using PayUnit.

== Description ==

Accept mtn momo ,orange money, visa and paypal on your by redirecting customers to PayUnit website for them to complete payments

= Why choose Woocommerce PayUnit? =

Woocommerce PayUnit Gateway has no setup fees, no monthly fees,it is completely free.

Woocommerce PayUnit Gateway is very easy to setup.

Woocommerce PayUnit Gateway has a dashboard where you can view all it's transactions.

== Installation ==

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don’t need to leave your web browser. To do an automatic install of the Woocommerce PayUnit Gateway plugin, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type “Woocommerce PayUnit Gateway” and click Search Plugins. Once you’ve found our plugin you can view details about it such as the point release, rating, and description. Most importantly, of course, you can install it by simply clicking "Install Now", then "Activate".

= Manual installation =

The manual installation method involves downloading our plugin and uploading it to your web server via your favorite FTP application. The WordPress codex contains [instructions on how to do this here](http://codex.wordpress.org/Managing_Plugins#Manual_Plugin_Installation).

== Screenshots ==

