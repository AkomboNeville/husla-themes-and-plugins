<?php

class RestEndpointPayUnit
{
    private $url = 'notify_url';

    public function __construct()
    {
        $this->addActions();
    }

    /**
     * @since v1.0
     * Adds actions
     */
    public function addActions(): void
    {
        add_action('rest_api_init', [$this, 'restEndpoint']);
    }

    public function restEndpoint()
    {
        register_rest_route('woopayunit', '/' . $this->url, [
            [
                'methods' => 'POST',
                'callback' => [$this, 'notifyCallback']
            ],
        ]);

    }

    public function notifyCallback($request)
    {
//        $parameters = $request->get_params();
        $parameters = $request->get_json_params();

        $status = $parameters['transaction_status'];

        switch ($status) {
            case 'SUCCESS':
             $transaction_id = $parameters['transaction_id'];
                $order_id = explode('-',$transaction_id)[0];

                global $woocommerce;
                $order = new WC_Order($order_id);

                //Complete payment
                $order->payment_complete();
                // Remove cart
                $woocommerce->cart->empty_cart();

                // Return thank-you redirect
                wp_send_json_success([
                    'message' => 'Success'
                ]);

                break;
            default:
                wp_send_json_error([
                    'message' => __('There was an error notifying the server','woo-gateway-payunit'),
                ]);
                break;
        }

    }

}