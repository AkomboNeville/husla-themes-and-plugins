<?php

function wg_payunit_submenu_options(){
    return[
        [
            'parent_slug'=> WOO_PAYUNIT_MENU_SLUG,
            'page_title' => 'Woocommerce Gateway PayUnit Transactions',
            'menu_title' => 'Transactions',
            'capability' => 'manage_options',
            'menu_slug'  =>  WOO_PAYUNIT_MENU_SLUG,
            'position'   => 1,
            'function'   => 'wg_payunit_transactions'
        ]
    ];
}

function wg_payunit_transactions(){
    require_once WOO_PAYUNIT_DIR.'inc/templates/transactions.php';
}