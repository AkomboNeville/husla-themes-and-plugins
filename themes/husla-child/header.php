<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

?><!DOCTYPE html>
<?php astra_html_before(); ?>

<html <?php language_attributes(); ?>>
<head>
    <?php astra_head_top(); ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
    <?php astra_head_bottom(); ?>
    <style>
        body.loading{
            overflow: hidden;
        }
        #loading-screen {
            position: fixed;
            background: white;
            z-index: 9999999999999999999999999;
            overflow: auto;
            height: 100vh;
            width: 100%;
            top: 0;
            left: 0;
            margin:0;
        }
        #loading-screen.loaded{
            display: none;
        }
        #loading-screen .loading-content{
            margin: auto;
            position: absolute;
            left: 50%;
            top:50%;
            overflow: hidden;
            transform: translate(-50%,-50%);
        }
        @keyframes km-img-loading {
            0% {
                width: 120px;
                opacity: 0.5;
            }

            50% {
                width: 150px;
                opacity: 1;
            }

            100% {
                width: 120px;
                opacity: 0.5;
            }
        }
    </style>
    <script>
        (function ($) {
            'use strict';

        $(document).ready(function() {
            document.querySelector('body').classList.add('loading');
            function hidePreloader() {
                if ($("body").find("div.vue-component").length === 0){
                    document.querySelector('body').classList.remove('loading');
                    $('#loading-screen').addClass('d-none');
                }
            }
            hidePreloader();
        });

        }(jQuery))
    </script>
</head>
<body <?php astra_schema_body(); ?> <?php body_class(); ?>>
<div id="loading-screen">
    <div class="loading-content">
        <img src="<?php echo HUSLA_JOBS_IMAGE_URL ?>/logo.png" style="animation-name: km-img-loading;
          animation-duration: 2s;
          animation-iteration-count: infinite;
          width: 110px;
"/>
    </div>
</div>
<?php astra_body_top(); ?>
<?php wp_body_open(); ?>

<a
    class="skip-link screen-reader-text"
    href="#content"
    role="link"
    title="<?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?>">
    <?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?>
</a>

<div
    <?php
        echo astra_attr(
            'site',
            array(
                'id'    => 'page',
                'class' => 'hfeed site',
            )
        );
    ?>
>
    <?php
        astra_header_before();

        astra_header();

        astra_header_after();

        astra_content_before();

    ?>

    <div id="content" class="site-content">
        <div class="ast-container">
            <?php astra_content_top(); ?>
