<?php
/* Custom functions code goes here. */

$dev_recaptcha_site_key = '6LdXYgkfAAAAAEc2A_y6wMLS4IvKF2stbXpCDEWJ'; //site key for localhost
$dev_recaptcha_secret_key = '6LdXYgkfAAAAAIv7OS1Wr5EVOXP1ASbGpvA-KEIG'; //secret key for localhost

$prod_recaptcha_site_key = '6LcrGgkfAAAAANGjmO_6Ub59Kgh0iBZomeOMddcL'; //site key for huslajobs.com
$prod_recaptcha_secret_key = '6LcrGgkfAAAAADclcd8uR0e8OHrETPcsJxTxWeYN';//secret key for huslajobs.com

$demo_recaptcha_site_key = '6Ldu8p0fAAAAAA3hJ4Q-QKfuo1lsqvFJu7Uez4BM'; // site key for zingersystems
$demo_recaptcha_secret_key = '6Ldu8p0fAAAAAD4sVDGahE54_6kB1MS6JeITULwg' ;//secret key for zingersystems

//TODO SWITCH TO PRODUCTION KEY ON LAUNCH
$recaptcha_key  = get_option('recaptcha_key') ;//?? $dev_recaptcha_site_key;
$recaptcha_secret = get_option('recaptcha_secret') ;//?? $dev_recaptcha_secret_key;
//$plugin_mode = get_option('husla_jobs_plugin_mode');
//if ($plugin_mode ==='production'){
//    $recaptcha_key  = $prod_recaptcha_site_key;
//    $recaptcha_secret = $prod_recaptcha_secret_key;
//
//}elseif ($plugin_mode === 'testing'){
//    $recaptcha_key  = $demo_recaptcha_site_key;
//    $recaptcha_secret = $demo_recaptcha_secret_key;
//}
define( 'HUSLASJOBS_RECAPTCHA_SITE_KEY', $recaptcha_key );
define( 'HUSLASJOBS_RECAPTCHA_SECRETE_KEY', $recaptcha_secret );

@ini_set( 'upload_max_size' , '256M' );
@ini_set( 'post_max_size', '256M');
@ini_set( 'max_execution_time', '500' );


function husla_scripts()
{

    wp_enqueue_script("recaptcha", 'https://www.google.com/recaptcha/api.js?render='.HUSLASJOBS_RECAPTCHA_SITE_KEY);

    wp_enqueue_style('ibid-parent-style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style('twentysixteen-style', get_stylesheet_uri());
    wp_enqueue_script("husla-script", get_stylesheet_directory_uri() . '/assets/js/husla-script.js', array('jquery'), NULL, true);
}

add_action('wp_enqueue_scripts', 'husla_scripts');

// Astra_Builder_Helper::$is_header_footer_builder_active = false;


// remove actions
if (!function_exists('husla_header_markup')) {

    /**
     * Site Header - <header>
     *
     * @since 1.0.0
     */
    function husla_header_markup()
    {

        if (has_action('astra_header')) {
            remove_action('astra_header', 'astra_header_markup');
        }
        do_action('astra_header_markup_before');
        ?>
        <header
            <?php
            echo astra_attr(
                'header',
                array(
                    'id' => 'masthead',
                    'class' => join(' ', astra_get_header_classes()),
                )
            );
            ?>
        >
            <?php
            astra_masthead_top();

            astra_masthead();

            astra_masthead_bottom();

            do_action('astra_sticky_header_markup');
            do_action('astra_bottom_header_after_markup');
            ?>
        </header><!-- #masthead -->
        <?php

        do_action('astra_header_markup_after');

    }
}
add_action('astra_header', 'husla_header_markup', 0);

/**
 * Primary Header
 */
if (!function_exists('husla_masthead_primary_template')) {

    /**
     * Primary Header
     *
     * => Used in files:
     *
     * /header.php
     *
     * @since 1.0.0
     */
    function husla_masthead_primary_template()
    {
        if (has_action('astra_masthead')) {
            remove_action('astra_masthead', 'astra_masthead_primary_template');
        }
        get_template_part('template-parts/header/header-main-layout');
    }
}

add_action('astra_masthead', 'husla_masthead_primary_template');


/**
 * Function to get Primary navigation menu
 */
if (!function_exists('husla_primary_navigation_markup')) {

    /**
     * Site Title // Logo
     *
     * @since 1.0.0
     */
    function husla_primary_navigation_markup()
    {

        if (has_action('astra_masthead_content')) {
            remove_action('astra_masthead_content', 'astra_primary_navigation_markup', 10);
        }

        $disable_primary_navigation = astra_get_option('disable-primary-nav');
        $custom_header_section = astra_get_option('header-main-rt-section');

        if ($disable_primary_navigation) {

            $display_outside = astra_get_option('header-display-outside-menu');

            if ('none' != $custom_header_section && !$display_outside) {

                echo '<div class="main-header-bar-navigation ast-flex-1 ast-header-custom-item ast-flex ast-justify-content-flex-end">';
                /**
                 * Fires before the Primary Header Menu navigation.
                 * Disable Primary Menu is checked
                 * Last Item in Menu is not 'none'.
                 * Take Last Item in Menu outside is unchecked.
                 *
                 * @since 1.4.0
                 */
                do_action('astra_main_header_custom_menu_item_before');

                echo astra_masthead_get_menu_items(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

                /**
                 * Fires after the Primary Header Menu navigation.
                 * Disable Primary Menu is checked
                 * Last Item in Menu is not 'none'.
                 * Take Last Item in Menu outside is unchecked.
                 *
                 * @since 1.4.0
                 */
                do_action('astra_main_header_custom_menu_item_after');

                echo '</div>';

            }
        } else {

            $submenu_class = apply_filters('astra_primary_submenu_border_class', ' submenu-with-border');

            // Menu Animation.
            $menu_animation = astra_get_option('header-main-submenu-container-animation');
            if (!empty($menu_animation)) {
                $submenu_class .= ' astra-menu-animation-' . esc_attr($menu_animation) . ' ';
            }

            /**
             * Filter the classes(array) for Primary Menu (<ul>).
             *
             * @since  1.5.0
             * @var Array
             */
            $primary_menu_classes = apply_filters('astra_primary_menu_classes', array('main-header-menu', 'ast-menu-shadow', 'ast-nav-menu', 'ast-flex', 'ast-justify-content-flex-end', $submenu_class));

            // Fallback Menu if primary menu not set.
            $fallback_menu_args = array(
                'theme_location' => 'primary',
                'menu_id' => 'primary-menu',
                'menu_class' => 'main-navigation',
                'container' => 'div',
                'before' => '<ul class="' . esc_attr(implode(' ', $primary_menu_classes)) . '">',
                'after' => '</ul>'.apply_filters('husla_user_menu', $html = ''),
                'walker' => new Astra_Walker_Page(),
            );

            $items_wrap = '<nav ';
            $items_wrap .= astra_attr(
                'site-navigation',
                array(
                    'id' => 'primary-site-navigation',
                    'class' => 'site-navigation ast-flex-grow-1 navigation-accessibility',
                    'aria-label' => esc_attr__('Site Navigation', 'astra'),
                )
            );
            $menu_html = apply_filters('husla_user_menu', $html = '');
            $items_wrap .= '>';
            $items_wrap .= '<div class="main-navigation">';
            $items_wrap .= '<ul id="%1$s" class="%2$s">%3$s</ul>';
            $items_wrap .= str_replace("%","%%",$menu_html);
            $items_wrap .= '</div>';
            $items_wrap .= '</nav>';

            // Primary Menu.
            $primary_menu_args = array(
                'theme_location' => 'primary',
                'menu_id' => 'primary-menu',
                'menu_class' => esc_attr(implode(' ', $primary_menu_classes)),
                'container' => 'div',
                'container_class' => 'main-header-bar-navigation',
                'items_wrap' => $items_wrap,
            );

            if (has_nav_menu('primary')) {
                // To add default alignment for navigation which can be added through any third party plugin.
                // Do not add any CSS from theme except header alignment.
                echo '<div ' . astra_attr('ast-main-header-bar-alignment') . '>';
                wp_nav_menu($primary_menu_args);
                echo '</div>';


            } else {

                echo '<div ' . astra_attr('ast-main-header-bar-alignment') . '>';
                echo '<div class="main-header-bar-navigation ast-flex-1">';
                echo '<nav ';
                echo astra_attr(
                    'site-navigation',
                    array(
                        'id' => 'primary-site-navigation',
                    )
                );
                echo ' class="site-navigation ast-flex-grow-1 navigation-accessibility" aria-label="' . esc_attr__('Site Navigation', 'astra') . '">';
                wp_page_menu($fallback_menu_args);;
                echo '</nav>';
                echo '</div>';
                echo '</div>';
            }
        }

    }
}

add_action('astra_masthead_content', 'husla_primary_navigation_markup', 20);
add_action('husla_mobile_header_custom_menu', 'husla_primary_navigation_markup', 2);

function husla_user_menu_markup($html)
{
    global $user_ID;
    ob_start();

    if ($user_ID) {


        $user = get_user_by('id',$user_ID);
        $fname = $user->first_name ?: $user->display_name;
        $avatar_id = trim($user->profile_image )!="" ? $user->profile_image :  get_stylesheet_directory_uri().'/assets/images/no_img.jpg';
        ?>
        <!-- avatar -->
        <div class="hs-user-login position-relative main-header-menu d-flex flex-wrap align-items-center">
            <div class="menu-item mr-2">
                <?php echo do_shortcode('[gtranslate]'); ?>
            </div>
            <div id="upload-resume-btn" class="menu-item mr-2" >
                <div class="menu-link d-inline-block pr-0">
                    <a href="<?php echo home_url('/my-account/#/add-profile') ?>" class="hs-btn hs-btn-primary-outline">Upload Resume</a>
                </div>
            </div>
            <script>
                jQuery(function ($) {
                    $(document).ready(function () {
                        let active_account = localStorage.getItem('active_account') ? JSON.parse(localStorage.getItem('active_account')) :undefined
                        if (active_account && (active_account.account_type === 'individual_recruiter' || active_account.account_type === 'company_recruiter')) {
                            $("#upload-resume-btn").addClass("d-none")
                        }else{
                            $("#upload-resume-btn").removeClass("d-none")
                        }
                    });
                })
            </script>
            <div class="menu-item">
                <a href="#" id="user-dropdown-btn" class="round menu-link" role="button">
                    <img  src="<?php echo $avatar_id ?>" class="hs-rounded v-avatar" alt="user avatar"/>
                </a>
            </div>

            <div id="hs-user-dropdown-menu" class="dropdown-menu p-4" role="menu">
                <div class="dropdown-content">
                    <div class="dropdown-head d-flex mb-2 align-items-center">
                        <div class="is-small mr-2">
                            <img src="<?php echo $avatar_id ?>" class="hs-rounded v-avatar" alt="user avatar"/>
                        </div>
                        <div class="meta">
                            <span class="text-uppercase"><?php echo $fname; ?></span>
                        </div>
                    </div>
                    <a href="<?php echo home_url('/my-account') ?>" role="menuitem" class="d-flex dropdown-item">
                        <div class="mr-2">
                            <i aria-hidden="true" class="fa fa-user"></i>
                        </div>
                        <div class="meta">
                            <span><?php echo __('My account','huslajobs') ?></span>

                        </div>
                    </a>
                    <hr class="dropdown-divider">
                    <a href="<?php echo home_url('/post-job') ?>" role="menuitem" class="d-flex dropdown-item">
                        <div class="icon mr-2">
                            <i aria-hidden="true" class="fas fa-cogs"></i>
                        </div>
                        <div class="meta">
                            <span><?php echo __(' Post job','huslajobs') ?></span>
                        </div>
                    </a>


                    <div class="mb-4"></div>
                    <div class="">
                        <a  id="husla-logout" href="<?php echo wp_logout_url() ?>" class="hs-btn hs-btn-primary logout" role="menuitem">

                            <span><?php echo __('Logout','huslajobs') ?></span>
                        </a>
                        <script>
                            jQuery(function ($) {
                                $(document).ready(function () {
                                    $("#husla-logout").click(e => {
                                        localStorage.clear();
                                        // localStorage.removeItem('user')
                                        // localStorage.removeItem('active_account')
                                    })
                                });
                            })
                        </script>
                    </div>

                </div>
            </div>
        </div>
        <?php

    } else {
        ?>

        <div class="hs-user-no-login main-header-menu d-flex flex-wrap align-items-center">

            <div class="menu-item">
                <a href="<?php echo home_url('/login') ?>" class="hs-primary-text-color menu-link">login</a>
            </div>
            <div class="menu-item">
                <div class="menu-link d-inline-block">
                    <a href="<?php echo home_url('/signup') ?>" class="hs-btn hs-btn-primary-outline">Sign up</a>
                </div>
            </div>
            <div class="menu-item">

                <?php echo do_shortcode('[gtranslate]'); ?>

            </div>
        </div>
    <?php } ?>
    <script>
        jQuery(function ($) {
            $(document).ready(function () {
                $(".hs-switch-language").click(e => {

                    localStorage.removeItem('user')
                    localStorage.removeItem('active_account')
                })
            });
        })
    </script>
    <?php
    $html = ob_get_clean();
    return $html;

}

add_filter('husla_user_menu', 'husla_user_menu_markup');

add_action('wp', 'astra_remove_new_header');

function astra_remove_new_header()
{
    remove_action('astra_primary_header', array(Astra_Builder_Header::get_instance(), 'primary_header'));
    remove_action('astra_mobile_primary_header', array(Astra_Builder_Header::get_instance(), 'mobile_primary_header'));
}

function husla_set_home_page(){
    $frontpage_id = get_option('page_on_front');
    $homepage =  get_page_by_title( 'Homepage' );
    if (  !$frontpage_id && $homepage)
    {
        update_option( 'page_on_front',  $homepage->ID );
        update_option( 'show_on_front', 'page' );
    }
}
add_action('init','husla_set_home_page');


@ini_set( 'upload_max_filesize' , '256M' );
@ini_set( 'post_max_size', '256M');
@ini_set( 'memory_limit', '256M' );
@ini_set( 'max_execution_time', '300' );
@ini_set( 'max_input_time', '300' );