<?php
/*
This page is used to display the static frontpage.
*/

// Fetch theme header template
get_header(); ?>

        <div id="primary" class="content-area primary">

                <div class="custom-homepage-container">
                    <!-- frontpage-component MARKUP WILL COME FROM VUE-FRONT-END COMPONENT INSIDE HUSLAS_PLUGIN/CLIENT/FRONT-PAGE -->
                    <div id="frontpage-component" class="vue-component">
<!--                        <div id="loading-screen">-->
<!--                            <div class="loading-content">-->
<!--                                <img src="--><?php //echo get_stylesheet_directory_uri() ?><!--/assets/images/logo.png" style="animation-name: km-img-loading;-->
<!--          animation-duration: 2s;-->
<!--          animation-iteration-count: infinite;"/>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                </div>


        </div><!-- #primary -->

<!--    <style>-->
<!--        .card {-->
<!--            max-width: 100% !important;-->
<!--        }-->
<!---->
<!--        #loading-screen {-->
<!--            position: absolute;-->
<!--            top: 30%;-->
<!--            left: 45%;-->
<!--            margin: auto;-->
<!--        }-->
<!---->
<!--        @keyframes km-img-loading {-->
<!--            0% {-->
<!--                width: 120px;-->
<!--                opacity: 0.5;-->
<!--            }-->
<!---->
<!--            50% {-->
<!--                width: 150px;-->
<!--                opacity: 1;-->
<!--            }-->
<!---->
<!--            100% {-->
<!--                width: 120px;-->
<!--                opacity: 0.5;-->
<!--            }-->
<!--        }-->
<!--    </style>-->
<?php get_footer(); ?>